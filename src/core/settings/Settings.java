package core.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Class representation of the settings file, in the form of a 
 * Properties object. Settings values can be obtained by calling
 * into the properties methods by key. 
 * 
 */
public class Settings{

	private static Properties properties = null;
	private static final String DEFAULT_FILE_NAME = "GlobalSettings";
	private static String setFileName = null;

	
	/**
	 * Gets the instance of this settings file (creates on first access).<br>
	 * For specific settings access, use {@link #getSettings(String)}, 
	 * providing the name of the file.<br><br>
	 * 
	 * Uses the set default, or class default: {@link #DEFAULT_FILE_NAME} -- {@value #DEFAULT_FILE_NAME}. 
	 */
	public static Properties getInstance(){
		if(properties == null){
			properties = getProperties(setFileName != null ? setFileName : DEFAULT_FILE_NAME);
		}
		return properties;
	}
	
	/**
	 * Gets the instance of the given properties file.<br>
	 * Given name should be file name only (presumed to be a property file 
	 * and '.properties' will be appended).<br><br>
	 * For more specific file access, use {@link #getSettings(String)}, 
	 * providing the name of the file.<br><br>
	 * @param fileName name of file 
	 */
	public static Properties getInstance(String fileName){
		if(properties == null){
			properties = getProperties(fileName);
		}
		return properties;
	}
	
	
	/**
	 * Returns a new instance of the specified settings file.<br>
	 * Specified by full name & extension.
	 * @param fileName name of file (+extension)
	 * @return settings file values in Properties instance
	 */
	public static Properties getInstanceByName(String fileName){
		return getProperties(fileName);
	}


	private static Properties getProperties(String fileName){
		try{
			ResourceBundle resourceBundle = ResourceBundle.getBundle(fileName);
			Properties props = convertResourceBundleToProperties(resourceBundle);
			return props;
		}catch(MissingResourceException e){
			return getPropertiesByFullFileName(fileName);
		}
	}
	
	private static Properties getPropertiesByFullFileName(String fullFileName){
		File file = new File(fullFileName);
		String defaultName = setFileName != null ? setFileName : DEFAULT_FILE_NAME;
		// check to see if the given is legit...
		if(!file.exists() || file.canRead()){
			// worst case, try default
			file = new File(defaultName + ".properties");
		}
		if(file.exists() && file.canRead()){
			try{
				InputStream is = new FileInputStream(file);
				Properties p = new Properties();
				p.load(is);
				return p;
			}catch (IOException ioException){
				ioException.printStackTrace();
			}
		}else{
			System.err.println("Settings: unable to find matching resource for \"" + fullFileName + "\" or " + defaultName + ".");
			System.exit(1);  // no settings, no run
		}
		return null;
	}

	private static Properties convertResourceBundleToProperties(ResourceBundle resource) {
		Properties props = new Properties();
		Enumeration<String> keys = resource.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			props.put(key, resource.getString(key));
		}
		return props;
	}
	
	/**
	 * Sets the default name that this class will use to access the file,
	 * if using the simplest version of getInstance [{@link #getInstance()}], as 
	 * opposed to providing a file name.
	 * @param fileName
	 */
	public static void setDefaultFileName(String fileName){
		setFileName = fileName;
	}
	
}
