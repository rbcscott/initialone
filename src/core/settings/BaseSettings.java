package core.settings;


import org.openqa.selenium.Platform;

import core.Browser;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import static core.settings.BaseSettings.Field.*;
import core.settings.runtime.RuntimeBrowserArgs;

import core.settings.ExecutionMode;
import core.settings.Settings;

/**
 * An instance of this class will provide access to the fields/properties of the
 * test run. Properties are loaded via the Settings class, which is set to load
 * the properties file with accessibility to the properties and fields necessary
 * for execution against project environments.
 * 
 * @return settings for project
 *
 */
public class BaseSettings{
	// calculated fields
	private Browser browser;
	private boolean sauceFlag = false; 
	private String machineName = "";
	private String machineIP = "";
	private ExecutionMode executionMode = null;  // calc
	private String runPlanName = "";
	private String runID = null;
	private Platform platform = null;
	protected String baseUrl = null;
	protected String currentDateTime = "--current time--";
	protected String currentDate = "--current date--";
	
	// local instance of the underlying properties set
	protected Properties props = null;
	// Default Browser
	private static final Browser DEFAULT_BROWSER = Browser.FIREFOX;
	// local instance of this class, for singleton
	private static BaseSettings baseSettings = null;
	// run name to be assigned
	private static String defaultRunName = null;
	
	/**
	 * Returns the BaseSettings (the settings will be established, if not already),
	 * with accessibility to the properties and fields necessary for execution. 
	 * @return settings for execution
	 */
	public static BaseSettings getSettings(){
		if(baseSettings == null){
			baseSettings = new BaseSettings();
		}
		return baseSettings;
	}
	
	/**
	 * This method will handle the initialization of the properties file
	 * (loading into the properties variable) and applies overrides from the
	 * main set of fields.<br>
	 * <br>
	 * <i>Extending classes should override and call this constructor first.</i><br>
	 * <br>
	 */
	protected BaseSettings(){
		Browser.setDefault(DEFAULT_BROWSER);
		defaultRunName = createRunID();
		runID = defaultRunName;
		machineName = getHostMachineName();
		machineIP = getHostMachineIP();
		props = Settings.getInstance();
		applyOverrides();
		//this.setCalculatedFields();
	}
	
	/**
	 * Representation of all the available fields from a property file.<br>
	 * <br>
	 * Any key-value pair that is added (or removed) from the representative
	 * property file within this project should be handle likewise in this
	 * enumerated set. Optionally, a getter method should be applied to provide
	 * access to the value.<br>
	 * <br>
	 * If the field is used in a calculated manner (e.g. a value dependent on
	 * another value or environment variable), that should be added to the
	 * [CustomSettings].setCalculatedFields() method for calculation.
	 *
	 */
	public enum Field{
		BROWSER_NAME("Browser"),
		BROWSER_VERSION("BrowserVersion"),
		PLATFORM("Platform"),
		BUILD_ID("BuildID"),
		EXECUTION_MODE("ExecutionMode"),
		THREAD_COUNT("ThreadCount"),
		RUNPLAN_NAME("RunPlanName"),
		MACHINE_NAME("MachineName"),
		MACHINE_IP("MachineIP"),
		TEST_REPORTS_DIRECTORY("TestReportsDirectory");

		private String key = "";
		private String value = "";

		private Field(String key){
			this.key = key;
		}
		
		void setValue(String value){
			this.value = value;
		}
		
		void setValue(Properties p){
			this.value = p.getProperty(key, "");
		}
		
		public String getKey(){
			return this.key;
		}

	}

	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br>
	 * <br>
	 * The value will be returned 'as-is', meaning that any derived or
	 * calculated fields will not necessarily be true or as intended (these
	 * fields should have access available from this API, allowing the value to
	 * be retrieved post-calculation).
	 * 
	 * @param key
	 *            {@link Field} value
	 * @return value associated with the given key (<b>null</b> if key is not
	 *         found)
	 */
	public String getProperty(Field key){
		if(key != null){
			return props.getProperty(key.key);
		}
		return null;
	}
	
	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br>
	 * <br>
	 * The value will be returned 'as-is', meaning that any derived or
	 * calculated fields will not necessarily be true or as intended (these
	 * fields should have access available from this API, allowing the value to
	 * be retrieved post-calculation).
	 * 
	 * @param key
	 *            String value of the property field to get
	 * @return value associated with the given key (<b>null</b> if not found)
	 */
	public String getProperty(String key){
		return props.getProperty(key);
	}

	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br>
	 * <br>
	 * This differs from {@link #getProperty(String)} in that it takes a default
	 * value that will be used in the event that the provided key is not found
	 * and the property returns null. Should a null value be provided for
	 * default, this method will return an empty String.<br>
	 * <br>
	 * The value will be returned 'as-is', meaning that any derived or
	 * calculated fields will not necessarily be true or as intended (these
	 * fields should have access available from this API, allowing the value to
	 * be retrieved post-calculation).
	 * 
	 * @param key String value of the property field to get
	 * @return value associated with the given key (<b>null</b> if not found)
	 */
	public String getProperty(String key, String defaultValue){
		String val = props.getProperty(key);
		if(val != null)
			return props.getProperty(key);
		else if(defaultValue != null)
			return defaultValue;
		else
			return "";
	}

	/**
	 * Set the calculated fields based on the assigned property values.<br><br>
	 * This should be called after the property file has been read into the
	 * instance of properties.<br><br>
	 *<b><i>Overriding methods from a subclass must call this method.</i></b>
	 * @param props
	 */
	protected void setCalculatedFields(){
		// calculated fields: Browser, Platform, executionMode, sauceFlag,
		// sauceAcctKey, sauceAcctUrl
		
		defaultRunName = createRunID();
		runID = defaultRunName;
		
		for(Field f : Field.values()){
			String value = props.getProperty(f.key, "");
			f.setValue(value);
		}
		
		browser = Browser.getBrowser(BROWSER_NAME.value);
		String platformStr = PLATFORM.value;
		if(platformStr != null && !platformStr.equals("")){
			platform = Platform.valueOf(platformStr.toUpperCase());
		}else{
			platform = Platform.getCurrent();
		}
		runPlanName = props.getProperty(RUNPLAN_NAME.key);
		String executionModeStr = props.getProperty(EXECUTION_MODE.key);
		executionMode = ExecutionMode.getMode(executionModeStr);
		
		setTimeProperties();
	}
	
	private void setTimeProperties(){
		Calendar currentdate = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a z");
		DateFormat dateOnlyFormatter = new SimpleDateFormat("yyyy-MM-dd");
		TimeZone timeZone = currentdate.getTimeZone();
		formatter.setTimeZone(timeZone);
		currentDateTime = formatter.format(currentdate.getTime());
		currentDateTime = currentDateTime.replaceAll(":", "-");
		currentDate = dateOnlyFormatter.format(currentdate.getTime());
	}
	
	public Browser getBrowser(){
		return this.browser;
	}
	
	public Platform getPlatform(){
		return this.platform;
	}

	/**
	 * Returns the run id for this test execution. This is set in the beginning of
	 * the setProperties internal method. The id will be set as "RUN" followed
	 * by a date-stamp. This may, if the run name is not set, also be the same
	 * as the run plan name (runPlanName).
	 * @return date-stamp to ID this execution
	 */
	public String getRunID(){
		return runID;
	}
	
	public String getCurrentDateTime(){
		return this.currentDateTime;
	}
	
	public String getCurrentDate(){
		return this.currentDate;
	}

	/**
	 * This will set the overriding values (incoming from runtime system
	 * parameters - which take precedence over the static properties).
	 */
	private void applyOverrides(){
		applyPrimaryOverrides();
		applyBrowserOverrides();
	}
	
	private void applyPrimaryOverrides(){
		String value = null;
		for(Field f : Field.values()){
			String argName = f.key;
			value = System.getProperty(argName);
			if(value != null){
				switch(f){
					case BUILD_ID:
					case PLATFORM:
					case EXECUTION_MODE:
					case RUNPLAN_NAME:
					case TEST_REPORTS_DIRECTORY:
					case BROWSER_NAME:
					case BROWSER_VERSION:
					case THREAD_COUNT:
						props.setProperty(argName, value);
						break;
					default:
						break;
				}
			}
		}
		if(props.getProperty(RUNPLAN_NAME.key) == null || props.getProperty(RUNPLAN_NAME.key).equals("")){
			props.setProperty(RUNPLAN_NAME.key, defaultRunName);
		}
		
	}
	
	
	private void applyBrowserOverrides(){
		String value = null;
		for(RuntimeBrowserArgs f : RuntimeBrowserArgs.values()){
			value = System.getProperty(f.getArgName());
			if(value != null){
				switch(f){
					case BROWSER:
						props.setProperty(BROWSER_NAME.key, value);
						break;
					default:
						break;
				}
			}
		}
	}
	
	/**
	 * Applies any mapped set of properties to the settings.<br><br>
	 * This uses a known set of fields that can be overridden, available
	 * from within this base class. If the matching field does not
	 * exist, or is not included within this method, it will not override
	 * the value.<br><br>
	 * <b><i>Overriding methods from a subclass must call this method.</i></b>
	 * @param map of key-value pairs
	 */
	public void applyRunplanOverrides(Map<String, String> map){
		String value = null;
		for(Field f : Field.values()){
			String argName = f.key;
			value = map.get(argName);
			if(value != null){
				switch(f){
					case BUILD_ID:
					case PLATFORM:
					case EXECUTION_MODE:
					case RUNPLAN_NAME:
					case TEST_REPORTS_DIRECTORY:
					case BROWSER_NAME:
					case BROWSER_VERSION:
					case THREAD_COUNT:
						props.setProperty(argName, value);
						break;
					default:
						break;
				}
			}
		}
		if(props.getProperty(RUNPLAN_NAME.key) == null || props.getProperty(RUNPLAN_NAME.key).equals("")){
			props.setProperty(RUNPLAN_NAME.key, defaultRunName);
		}

	}

	/**
	 * If Sauce has been flagged (checked) in the properties or via
	 * Execution Manager, this should return <code><b>true</b></code>.
	 * @return <code><b>true</b></code> if set
	 */
	public boolean isSauceRequested(){
		return sauceFlag;
	}
	
	public String getBaseUrl(){
		return baseUrl;
	}

	
	public ExecutionMode getExecutionMode(){
		return executionMode;
	}

	/**
	 * Output of the Settings in a multi-line, formatted listing of 
	 * <code style='color:blue;'>"key: value"</code>.
	 * 
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("Browser:          " + this.browser.getDisplayValue() + "\n");
		sb.append("RunMode:          " + this.getExecutionMode() + "\n");
		sb.append("Platform:         " + this.platform + "\n");
		sb.append("Runplan Name:     " + runPlanName + "\n");
		sb.append("Run ID:           " + runID + "\n");
		sb.append("Machine Name:     " + machineName + "\n");
		sb.append("Machine IP:       " + machineIP + "\n");
		return sb.toString();
	}

	/**
	 * Returns the run plan name for this test execution. This is set by the
	 * execution, and, if not, will be the same as the run ID. In this case, the
	 * id will be set as "RUN" followed by a date-time stamp.
	 * 
	 * @return assigned run name (if set), or the run ID
	 */
	public String getRunPlanName(){
		return runPlanName;
	}
	
	/**
	 * Gets the set machine name. This will be the execution machine and not
	 * necessarily the executing machine for the browser being tested (unless
	 * the run mode is Local).
	 * 
	 * @return the machine name
	 */
	public String getMachineName(){
		return machineName;
	}

	/**
	 * Gets the set machine IP. This will be the execution machine and not
	 * necessarily the executing machine for the browser being tested (unless
	 * the run mode is Local).
	 * 
	 * @return the machine IP
	 */
	public String getMachineIP(){
		return machineIP;
	}

	public String getAppDirectory(){
		return TEST_REPORTS_DIRECTORY.value;
	}

	/**
	 * Applies the given string to the runplan name on this execution.<br>
	 * <br>
	 * This will override any passed runplan name, or auto set name that may
	 * have been applied.
	 *  
	 * @param runPlanName
	 *            String - new name for the run plan
	 */
	public void setRunPlanName(String runPlanName){
		this.runPlanName = runPlanName;
	}
	/**
	 * Creates a runplan "ID" based on the current time, prefixed by "RUN" to
	 * make the name.<br>
	 * <br>
	 * This value will be used anywhere a unique instance, or tag, for a run is
	 * necessary. One example would be logging result records to a table that
	 * may have seemingly duplicate records, such as a secondary run of the same
	 * runplan, with the same verification point result.
	 * 
	 * @return String - new, built run ID
	 */
	private static String createRunID(){
		Calendar currentdate = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		String currentDateTimeStr = formatter.format(currentdate.getTime());
		return "RUN_" + currentDateTimeStr.replaceAll("\\s", "_");
	}

	private String getHostMachineName(){
		String returnVal = "";
		try{
			returnVal = InetAddress.getLocalHost().getHostName();
		}catch (UnknownHostException e){
			e.printStackTrace();
		}
		return returnVal;
}

	private String getHostMachineIP(){
		String returnVal = "";
		try{
			returnVal = InetAddress.getLocalHost().getHostAddress();
		}catch (UnknownHostException e){
			e.printStackTrace();
		}
		return returnVal;
	}
}
