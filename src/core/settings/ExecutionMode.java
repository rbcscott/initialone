package core.settings;

/**
 * Representation of the available options for execution.<br><br>
 * <ul>
 * <li>LOCAL: local machine execution - browsers are started on execution machine
 * <li>GRID: browsers are run via node-attached VM's
 * </ul>
 */
public enum ExecutionMode{

	LOCAL("Local"),
	GRID("Grid");

	private String name = "";
	
	private ExecutionMode(String mode){
		this.name = mode;
	}
	/**
	 * Interface for properly obtaining an instance of ExecutionMode, provided
	 * a string that should match.
	 * @param executionModeStr - 
	 * 		String matching the mode (<code style='color:blue;'>"Local"</code> 
	 * 		or <code style='color:blue;'>"Grid"</code>). The match is done case insensitively.
	 * @return Execution that matches, or LOCALas default if unable to match. 
	 */
	public static ExecutionMode getMode(String executionModeStr){
		for(ExecutionMode mode : ExecutionMode.values()){
			if(mode.name.equalsIgnoreCase(executionModeStr)){
				return mode;
			}
		}
		return LOCAL;
	}
}
