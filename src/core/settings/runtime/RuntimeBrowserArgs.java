package core.settings.runtime;

import core.settings.BaseSettings;
import core.settings.BaseSettings.Field;

public enum RuntimeBrowserArgs{

	BROWSER("browser", BaseSettings.Field.BROWSER_NAME),
	BROWSER_VERSION("BrowserVersion"),
	RANDOMIZE("RandomizeBrowser"),
	RUN_ALL("RunAllBrowsers");
	

	private String argName = "";
	private Field fieldOverride = null;
	
	private RuntimeBrowserArgs(String argName){
		this.argName = argName;
	}
	private RuntimeBrowserArgs(String argName, BaseSettings.Field fieldOverride){
		this.argName = argName;
		this.fieldOverride = fieldOverride;
	}
	
	public String getArgName(){
		return this.argName;
	}
	
	public Field getFieldOverride(){
		return fieldOverride;
	}
}

