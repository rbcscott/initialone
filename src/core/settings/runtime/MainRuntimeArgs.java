package core.settings.runtime;

import core.settings.BaseSettings;
import core.settings.BaseSettings.Field;

public enum MainRuntimeArgs{

	BUILD_ID("buildId", Field.BUILD_ID),
	PLATFORM("platform", Field.PLATFORM),
	RUN_NAME("runplanName", Field.RUNPLAN_NAME),
	RUN_MODE("runMode", Field.EXECUTION_MODE);

	private String argName = null;
	private Field fieldOverride = null;
	
	private MainRuntimeArgs(String argName){
		this.argName = argName;
	}

	private MainRuntimeArgs(String argName, BaseSettings.Field fieldOverride){
		this.argName = argName;
		this.fieldOverride = fieldOverride;
	}
	
	public String getArgName(){
		return this.argName;
	}

	public Field getFieldOverride(){
		return fieldOverride;
	}
}

