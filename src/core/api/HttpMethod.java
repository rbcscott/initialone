package core.api;

public enum HttpMethod{

	GET("GET"),
	POST("POST"),
	PUT("PUT"),
	PATCH("PATCH"),
	DELETE("DELETE");
	
	private String name;
	private static HttpMethod defaultMethod = GET;

	private HttpMethod(String name){
		this.name = name;
	}
	
	public void applyDefault(HttpMethod defaultMethod){
		HttpMethod.defaultMethod = defaultMethod;
	}
	
	public void setDefault(HttpMethod method){
		HttpMethod.defaultMethod = method;
	}
	
	public static HttpMethod getMethod(String method){
		for(HttpMethod m : HttpMethod.values()){
			if(method.equalsIgnoreCase(m.toString()))
				return m;
		}
		return defaultMethod;
	}
	
	public static String [] getMethodsAsArray(){
		int length = HttpMethod.values().length;
		String [] set = new String[length];
		int index = 0;
		for(HttpMethod m : HttpMethod.values()){
			set[index] = m.name;
			index++;
		}
		return set;
	}
}
