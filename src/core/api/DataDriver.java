package core.api;

public class DataDriver{

	private String label = "";
	private RequestOwner owner = null;
	private String dataTablePath = "";
	private String dataTableColumnReference = "";
	private String originalDataReplacement = "";
	private String dbSource = "";
	private String dbQuery = "";
	
	private boolean projectDataSource = false;
	private boolean externalDataSource = false;
	/**
	 * @return the label
	 */
	public String getLabel(){
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label){
		this.label = label;
	}
	/**
	 * @return the dataTablePath
	 */
	public String getDataTablePath(){
		return dataTablePath;
	}
	/**
	 * @param dataTablePath the dataTablePath to set
	 */
	public void setDataTablePath(String dataTablePath){
		this.dataTablePath = dataTablePath;
	}
	/**
	 * @return the dataTableColumnReference
	 */
	public String getDataTableColumnReference(){
		return dataTableColumnReference;
	}
	/**
	 * @param dataTableColumnReference the dataTableColumnReference to set
	 */
	public void setDataTableColumnReference(String dataTableColumnReference){
		this.dataTableColumnReference = dataTableColumnReference;
	}
	/**
	 * @return the originalDataReplacement
	 */
	public String getOriginalDataReplacement(){
		return originalDataReplacement;
	}
	/**
	 * @param originalDataReplacement the originalDataReplacement to set
	 */
	public void setOriginalDataReplacement(String originalDataReplacement){
		this.originalDataReplacement = originalDataReplacement;
	}
	/**
	 * @return the dbSource
	 */
	public String getDbSource(){
		return dbSource;
	}
	/**
	 * @param dbSource the dbSource to set
	 */
	public void setDbSource(String dbSource){
		this.dbSource = dbSource;
	}
	/**
	 * @return the dbQuery
	 */
	public String getDbQuery(){
		return dbQuery;
	}
	/**
	 * @param dbQuery the dbQuery to set
	 */
	public void setDbQuery(String dbQuery){
		this.dbQuery = dbQuery;
	}
	/**
	 * @return the projectDataSource
	 */
	public boolean isProjectDataSource(){
		return projectDataSource;
	}
	/**
	 * @param projectDataSource the projectDataSource to set
	 */
	public void setIsProjectDataSource(boolean projectDataSource){
		this.projectDataSource = projectDataSource;
	}
	/**
	 * @return the externalDataSource
	 */
	public boolean isExternalDataSource(){
		return externalDataSource;
	}
	/**
	 * @param externalDataSource the externalDataSource to set
	 */
	public void setIsExternalDataSource(boolean externalDataSource){
		this.externalDataSource = externalDataSource;
	}
	/**
	 * @return the owner
	 */
	public RequestOwner getOwner(){
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(RequestOwner owner){
		this.owner = owner;
	}
	@Override
	public boolean equals(Object obj){
		if(obj == this)
			return true;
		if(obj instanceof DataDriver){
			DataDriver driver = (DataDriver) obj;
			if(!driver.label.equals(this.label)){
				return false;
			}
			if(this.owner != driver.owner){
				return false;
			}
			// project vs external
			if(this.projectDataSource){
				if(driver.projectDataSource){
					if(this.dataTablePath.equals(driver.dataTablePath) &&
							this.dataTableColumnReference.equals(driver.dataTableColumnReference)){
						return true;
					}
				}else{
					return false;
				}
			}else if(this.externalDataSource){
				if(driver.externalDataSource){
					if(dbSource.equals(driver.dbSource) && dbQuery.equals(driver.dbQuery)){
						return true;
					}
				}else{
					return false;
				}
			}
		}
		return false;
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		bldr.append("ProjectSource:    " + isProjectDataSource() + "\n");
		bldr.append("DataSourceFile:   " + dataTablePath + "\n");
		bldr.append("DataSourceColumn: " + dataTableColumnReference + "\n");
		bldr.append("DBConnection:     " + dbSource + "\n");
		bldr.append("DBQuery:          " + dbQuery + "\n");
		return bldr.toString();
	}
	
	
	
}
