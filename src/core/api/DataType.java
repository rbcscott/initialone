package core.api;

public enum DataType{

	STRING("String"),
	NUMBER("Number"),
	INTEGER("Number"),
	FLOAT("Number"),
	DOUBLE("Number"),
	BOOLEAN("Boolean");
	
	private String value = "";
	private static DataType defaultType = null;

	private DataType(String value){
		this.value = value;
	}

	/**
	 * Returns a matching DataType based on the given string value. This will attempt to match
	 * with values set to lower case.<br>
	 * Note that spaces are removed (e.g. if given <code style='color:blue;'>"String Value"</code> it
	 * will be compared as if <code style='color:blue;'>"stringvalue"</code>).<br><br>
	 * Default return is <b>null</b> unless a setDefault mechanism is in place. {@link #setDefault(DataType)})
	 * @param value String value matching enumerated type
	 * @return matching DataType
	 */
	public static DataType getDataType(String value){
		for(DataType e : DataType.values()){
			String enumToStr = e.value.replaceAll(" ", "");
			String argToStr = value.replaceAll(" ", "");
			if(enumToStr.equalsIgnoreCase(argToStr))
				return e;
		}
		System.out.println("DataType " + value + " did not match any in DataType enum set.");
		return null; // if default value is in use for this enum, return default in lieu of null
	}

	/**
	 * Set default DataType to be used when a default is needed. Will stand 
	 * unless called upon again.
	 * @param defaultType
	 */
	public static void setDefault(DataType defaultType){
		DataType.defaultType = defaultType;
	}

	/**
	 * Return the default set DataType
	 * @return default DataType
	 */
	public DataType getDefault(){
		return defaultType;
	}

	/**
	 * Returns the string represented for this enumeration. This is the 
	 * value sent to the constructor, and the value in which the 
	 * {@link #getTestType(String)} takes in order to match.
	 */
	public String toString(){
		return this.value;
	}
	
}
