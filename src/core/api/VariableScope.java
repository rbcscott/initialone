package core.api;

public enum VariableScope{

	GLOBAL("Global"),
	TEST_CASE("TestCase"),
	PRIVATE("Private");
	
	private String value = "";
	private static VariableScope defaultType = null;

	private VariableScope(String value){
		this.value = value;
	}

	/**
	 * Returns a matching VariableScope based on the given string value. This will attempt to match
	 * with values set to lower case.<br>
	 * Note that spaces are removed (e.g. if given <code style='color:blue;'>"String Value"</code> it
	 * will be compared as if <code style='color:blue;'>"stringvalue"</code>).<br><br>
	 * Default return is <b>null</b> unless a setDefault mechanism is in place. {@link #setDefault(VariableScope)})
	 * @param value String value matching enumerated type
	 * @return matching VariableScope
	 */
	public static VariableScope getVariableScope(String value){
		for(VariableScope e : VariableScope.values()){
			String enumToStr = e.value.replaceAll(" ", "");
			String argToStr = value.replaceAll(" ", "");
			if(enumToStr.equalsIgnoreCase(argToStr))
				return e;
		}
		System.out.println("VariableScope " + value + " did not match any in VariableScope enum set.");
		return null; // if default value is in use for this enum, return default in lieu of null
	}

	/**
	 * Set default VariableScope to be used when a default is needed. Will stand 
	 * unless called upon again.
	 * @param defaultType
	 */
	public static void setDefault(VariableScope defaultType){
		VariableScope.defaultType = defaultType;
	}

	/**
	 * Return the default set VariableScope
	 * @return default VariableScope
	 */
	public VariableScope getDefault(){
		return defaultType;
	}

	/**
	 * Returns the string represented for this enumeration. This is the 
	 * value sent to the constructor, and the value in which the 
	 * {@link #getTestType(String)} takes in order to match.
	 */
	public String toString(){
		return this.value;
	}
}
