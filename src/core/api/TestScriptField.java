package core.api;

import java.util.ArrayList;
import java.util.List;

public enum TestScriptField implements FieldSet{

	TEST_CASE_ID("TestCaseID", "String"),
	TEST_CASE_NAME("TestCaseName", "String"),
	TEST_CASE_LABEL("TestCaseLabel", "String"),
	TEST_CASE_DESCRIPTION("TestCaseDescription", "String"),
	SERVICE_NAME("ServiceName", "String"),
	METHOD("Method", "Enumeration", "GET;POST;PUT;PATCH;DELETE"),
	SERVICE_ENDPOINT("ServiceEndpoint", "String"),
	AUTHORIZATION("Authorization", "Enumeration", "None;Basic;OAuth1.0;OAuth2.0"),
	ORIGINAL_TEST_URL("OriginalTestURL", "String"),
	TEST_TYPE("TestType", "Enumeration", "Smoke;Regression;Functional;Integration"),
	GROUP("Group", "String"),
	JIRA_ID("JiraID", "String"),
	BODY("Body", "String"),
	HEADERS("Headers", "Header"),
	PARAMETERS("Parameters", "Parameter"),
	VERIFICATIONS("Verifications", "Verification"),
	DATA_DRIVERS("DataDrivers", "DataDriver"),
	VARIABLES("Variables", "Variable");
	
	public String name = "";
	public String type = "";
	public String valuesStr = "";
	public List<String> values = new ArrayList<>();

	private TestScriptField(String name, String type, String ... values){
		this.name = name;
		this.type = type;
		if(values != null && values.length > 0){
			this.valuesStr = values[0];
			String [] split = valuesStr.split(";");
			for(String s : split){
				this.values.add(s);
			}
		}
	}

	/**
	 * Returns a matching TestScriptFields based on the given string value. This will attempt to match
	 * with values set to lower case.<br>
	 * Note that spaces are removed (e.g. if given <code style='color:blue;'>"String Value"</code> it
	 * will be compared as if <code style='color:blue;'>"stringvalue"</code>).<br><br>
	 * Default return is <b>null</b> unless a setDefault mechanism is in place. {@link #setDefault(TestScriptField)})
	 * @param value String value matching enumerated type
	 * @return matching TestScriptFields
	 */
	public static TestScriptField getTestScriptFields(String value){
		for(TestScriptField e : TestScriptField.values()){
			String enumToStr = e.name.replaceAll(" ", "");
			String argToStr = value.replaceAll(" ", "");
			if(enumToStr.equalsIgnoreCase(argToStr))
				return e;
		}
		System.out.println("TestScriptFields " + value + " did not match any in TestScriptFields enum set.");
		return null; // if default value is in use for this enum, return default in lieu of null
	}

	/**
	 * Returns the string represented for this enumeration. This is the 
	 * value sent to the constructor, and the value in which the 
	 * {@link #getTestType(String)} takes in order to match.
	 */
	public String toString(){
		return this.name;
	}

	@Override
	public TestScriptField[] getFields(){
		return TestScriptField.values();
	}

	@Override
	public String getName(){
		return this.name;
	}

	@Override
	public String getType(){
		return this.type;
	}

	@Override
	public String getValues(){
		return this.valuesStr;
	}

	@Override
	public String getReferenceTypeName(){
		return "TestScript";
	}
	
}
