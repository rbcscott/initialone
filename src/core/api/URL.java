package core.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class URL{

	private String http = "";
	private int httpIndex = 0;
	private String server = "";
	private int serverIndex = 0;
	private String port = "";
	private int portIndex = 0;
	private String hostName = "";
	private int hostNameIndex = 0;
	private String resource = "";
	private int resourceIndex = 0;
	private List<String> resourcePaths = new ArrayList<>();
	Map<String, String> parameters = new LinkedHashMap<>();
	private int parameterIndex = 0;
	private String url = "";
	
	
	//public static final String URL_MATCHER = "(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	//public static final String URL_MATCHER = "(https?|ftp|file):\\/\\/([-a-zA-Z0-9+&@#%?=~_|!:,.;]*)([-a-zA-Z0-9+&@#/%=~_|.?]*)";
	public static final String URL_MATCHER = 
			"(https?|ftp|file):\\/\\/([-a-zA-Z0-9+&@#%?=~_|!:,.;]*)([-a-zA-Z0-9+&@#/%=~_|.]*)(\\?[-a-zA-Z0-9+&@#/%=~_|.]*)?";

	static String urlTest = "http://litmdnasv01:8101/item/find?tenant=dotCom&lang=en-US";

	public URL(String url){
		this.url = url;
	}

	public boolean isURL(){
		if(url.matches(URL_MATCHER))
			return true;
		return false;
	}

	public void matchUrl(){
		Pattern p = Pattern.compile(URL_MATCHER);
		Matcher m = p.matcher(url);
		if(m.matches()){
			int matchCount = m.groupCount();
			if(matchCount > 0){
				http = m.group(1);
			}
			if(matchCount > 1){
				String group = m.group(2);
				int index = m.start(2);
				hostName = group;
				hostNameIndex = index;
				if(group.matches("[A-Za-z0-9]+:[0-9]{1,8}")){
					String serverPort = group;
					String [] parts = serverPort.split(":");
					String server = parts[0];
					String port = parts[1];
					this.server = server;
					this.serverIndex = index;
					this.port = port;
					portIndex = index + server.length() + 1;
				}else{
					String server = group;
					this.server = server;
					this.serverIndex = index;
				}
			}
			if(matchCount > 2){
				String resourceAndQuery = m.group(3);
				int index = m.start(3);
				String [] split = resourceAndQuery.split("\\?");
				String resourcePart = split[0];
				resource = resourcePart;
				resourcePaths = getResourceParts(resource);
				// check for query string...
				if(split.length > 1){
					String query = split[1];
					String [] queryParts = query.split("&");
					parameterIndex = index + resourcePart.length() + 1;
					// add query parameters to set...
					for(String queryPart : queryParts){
						String [] nameValue = queryPart.split("=");
						if(nameValue.length > 1){
							String name = nameValue[0];
							String value = nameValue[1];
							parameters.put(name, value);
						}
					}
				}			}
			if(matchCount > 3){
				String query = m.group(4);
				if(query != null){
					query = query.substring(1);
					parameterIndex = m.start(4) + 1;
					String [] queryParts = query.split("&");
					// add query parameters to set...
					for(String queryPart : queryParts){
						String [] nameValue = queryPart.split("=");
						if(nameValue.length > 0){
							String name = nameValue[0];
							String value = "";
							if(nameValue.length > 1){
								value = nameValue[1];
							}
							parameters.put(name, value);
						}
					}
				}
			}
		}
	}
	
	private List<String> getResourceParts(String resource){
		List<String> parts = new ArrayList<String>();
		String[] resourceSplit = resource.split("/");
		for(String s : resourceSplit){
			if(!s.trim().equals(""))
				parts.add(s);
		}
		return parts;
	}
	
	@Deprecated
	public void parseUrl(){
		Scanner scanner = new Scanner(url);
		Pattern currentDelim = scanner.delimiter();
		scanner.useDelimiter("/");
		//String resource = "";
		int index = 0;
		while(scanner.hasNext()){
			String next = scanner.next();
			if(next.matches("(http:|https:)")){
				http = next.substring(0, next.length() - 1);
				httpIndex = index;
			}else if(next.matches("[A-Za-z0-9]+:[0-9]{1,8}")){
				String serverPort = next;
				String [] parts = serverPort.split(":");
				String server = parts[0];
				String port = parts[1];
				this.server = server;
				serverIndex = index;
				this.port = port;
				portIndex = index + server.length() + 1;
			}else if(next.matches("[A-Za-z0-9]+:[0-9]{1,8}")){
				
			}else if(next.contains("?")){
				String resourceAndQuery = next;
				String [] split = resourceAndQuery.split("\\?");
				String resourcePart = split[0];
				String query = split[1];
				resource += "/" + resourcePart;
				resourcePaths.add(resourcePart);
				parameterIndex = index + resourcePart.length() + 1;
				String [] queryParts = query.split("&");
				for(String queryPart : queryParts){
					String [] nameValue = queryPart.split("=");
					if(nameValue.length > 1){
						String name = nameValue[0];
						String value = nameValue[1];
						parameters.put(name, value);
					}
				}
			}else if(next.matches("[A-Za-z0-9_]+")){
				resource += "/" + next;
				resourcePaths.add(next);
			}
			if(next.equals("")){
				index += scanner.delimiter().toString().length() + 1;
			}else{
				index += next.length();
			}
		}
		scanner.close();
	}

	public static void main(String[] args){
		URL url = new URL(urlTest); 
		url.parseUrl();
		System.out.println(url);
	}

	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		bldr.append(http);
		bldr.append("\n");
		bldr.append(server + ":" + port);
		bldr.append("\n");
		bldr.append(resource);
		bldr.append("\n");
		for(String s : parameters.keySet()){
			bldr.append(s);
			bldr.append("=");
			bldr.append(parameters.get(s));
			bldr.append("\n");			
		}
		
		return bldr.toString();
	}

	public String getHttp(){
		return http;
	}

	public String getServer(){
		return server;
	}

	public String getPort(){
		return port;
	}

	public String getResource(){
		return resource;
	}

	public List<String> getResourcePaths(){
		return resourcePaths;
	}

	public Map<String, String> getParameters(){
		return parameters;
	}

	public String getUrl(){
		return url;
	}

	public int getHttpIndex(){
		return httpIndex;
	}

	public int getServerIndex(){
		return serverIndex;
	}

	public int getPortIndex(){
		return portIndex;
	}

	public int getResourceIndex(){
		return resourceIndex;
	}

	public int getParameterIndex(){
		return parameterIndex;
	}
}
