package core.api;

public interface FieldSet{

	public FieldSet [] getFields();
	public String getName();
	public String getType();
	public String getValues();
	public String getReferenceTypeName();
	
}
