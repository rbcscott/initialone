package core.api;

public enum TestType{

	SMOKE("Smoke"),
	REGRESSION("Regression"),
	FUNCTIONAL("Functional"),
	INTEGRATION("Integration");
	
	private String value = "";
	private static TestType defaultType = null;

	private TestType(String value){
		this.value = value;
	}

	/**
	 * Returns a matching TestType based on the given string value. This will attempt to match
	 * with values set to lower case.<br>
	 * Note that spaces are removed (e.g. if given <code style='color:blue;'>"String Value"</code> it
	 * will be compared as if <code style='color:blue;'>"stringvalue"</code>).<br><br>
	 * Default return is <b>null</b> unless a setDefault mechanism is in place. {@link #setDefault(TestType)})
	 * @param value String value matching enumerated type
	 * @return matching TestType
	 */
	public static TestType getTestType(String value){
		for(TestType e : TestType.values()){
			String enumToStr = e.value.replaceAll(" ", "");
			String argToStr = value.replaceAll(" ", "");
			if(enumToStr.equalsIgnoreCase(argToStr))
				return e;
		}
		System.out.println("TestType " + value + " did not match any in TestType enum set.");
		return null; // if default value is in use for this enum, return default in lieu of null
	}

	/**
	 * Set default TestType to be used when a default is needed. Will stand 
	 * unless called upon again.
	 * @param defaultType
	 */
	public static void setDefault(TestType defaultType){
		TestType.defaultType = defaultType;
	}

	/**
	 * Return the default set TestType
	 * @return default TestType
	 */
	public TestType getDefault(){
		return defaultType;
	}

	/**
	 * Returns the string represented for this enumeration. This is the 
	 * value sent to the constructor, and the value in which the 
	 * {@link #getTestType(String)} takes in order to match.
	 */
	public String toString(){
		return this.value;
	}
	
	
	
}
