package core.api;

public enum RequestOwner{

	BODY("BODY"),
	PARAMETERS("PARAMETERS"),
	VERIFICATION("VERIFICATION");
	
	
	private String value = "";
	private static RequestOwner defaultType = null;

	private RequestOwner(String value){
		this.value = value;
	}

	/**
	 * Returns a matching RequestOwner based on the given string value. This will attempt to match
	 * with values set to lower case.<br>
	 * Note that spaces are removed (e.g. if given <code style='color:blue;'>"String Value"</code> it
	 * will be compared as if <code style='color:blue;'>"stringvalue"</code>).<br><br>
	 * Default return is <b>null</b> unless a setDefault mechanism is in place. {@link #setDefault(RequestOwner)})
	 * @param value String value matching enumerated type
	 * @return matching RequestOwner
	 */
	public static RequestOwner getRequestOwner(String value){
		for(RequestOwner e : RequestOwner.values()){
			String enumToStr = e.value.replaceAll(" ", "");
			String argToStr = value.replaceAll(" ", "");
			if(enumToStr.equalsIgnoreCase(argToStr))
				return e;
		}
		System.out.println("RequestOwner " + value + " did not match any in RequestOwner enum set.");
		return null; // if default value is in use for this enum, return default in lieu of null
	}

	/**
	 * Set default RequestOwner to be used when a default is needed. Will stand 
	 * unless called upon again.
	 * @param defaultType
	 */
	public static void setDefault(RequestOwner defaultType){
		RequestOwner.defaultType = defaultType;
	}

	/**
	 * Return the default set RequestOwner
	 * @return default RequestOwner
	 */
	public RequestOwner getDefault(){
		return defaultType;
	}

	/**
	 * Returns the string represented for this enumeration. This is the 
	 * value sent to the constructor, and the value in which the 
	 * {@link #getTestType(String)} takes in order to match.
	 */
	public String toString(){
		return this.value;
	}
}
