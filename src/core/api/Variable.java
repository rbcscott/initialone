package core.api;

public class Variable{

	private String name = "";
	private DataType type = DataType.STRING;
	private VariableScope scope = null;
	private String testCaseOwner = null;
	private String value = "";
	private Object objVal = "";
	private RequestOwner owner = null;
	
	
	public Variable(String name, DataType type, VariableScope scope){
		this.name = name;
		this.type = type;
		this.scope = scope;
	}
	
	
	public String getName(){
		return name;
	}

	public DataType getType(){
		return type;
	}

	public VariableScope getScope(){
		return scope;
	}
	
	public void setScope(VariableScope scope){
		this.scope = scope;
	}

	public String getValue(){
		return value;
	}

	public void setValue(String value){
		this.value = value;
	}
	
	public void setValue(Object value){
		this.objVal = value;
	}
	
	/**
	 * @return the testCaseOwner
	 */
	public String getTestCaseOwner(){
		return testCaseOwner;
	}

	/**
	 * @param testCaseOwner the testCaseOwner to set
	 */
	public void setTestCaseOwner(String testCaseOwner){
		this.testCaseOwner = testCaseOwner;
	}

	
	public boolean equals(Object o){
		if(this == o)
			return true;
		if(o instanceof Variable){
			Variable var = (Variable) o;
			if(this.name.equals(var.name) &&
					this.scope.equals(var.scope) &&
					this.type.equals(var.type) && 
					this.testCaseOwner.equals(var.testCaseOwner)){
				return true;
			}
		}
		return false;
	}


	/**
	 * @return the owner, or requester, of this variable.
	 */
	public RequestOwner getOwner(){
		return owner;
	}


	/**
	 * @param owner the owner to set
	 */
	public void setOwner(RequestOwner owner){
		this.owner = owner;
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		bldr.append("Name: " + this.name + "\n");
		bldr.append("Type: " + this.type.toString() + "\n");
		bldr.append("Scope: " + this.scope.toString() + "\n");
		bldr.append("Test Case Owner: " + this.testCaseOwner + "\n");
		return bldr.toString();
	}
}
