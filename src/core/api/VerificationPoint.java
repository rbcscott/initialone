package core.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representation of a verification point, the underlying
 * data behind the visual representation. It is constructed
 * with the segments identifying the operations and verifiers
 * necessary for a verification.
 * @version 1.0
 * @author Scott C.
 */
public class VerificationPoint{

	private List<String> segments = new ArrayList<>();
	private String path = "";
	private String operator = "";
	private String verifier = "";
	
	private boolean valid = true;
	
	private String variableName = "";
	
	public VerificationPoint(){
		
	}

	
	
	public void setSegments(String vpTextContent){
		if(vpTextContent != null && !vpTextContent.trim().equals("")){
			String [] segmentSplit = vpTextContent.split(" ");
			segments.clear();
			int index = 0;
			for(String segment : segmentSplit){
				segments.add(segment);
				switch(index){
				case 0:
					path = segment;
					break;
				case 1:
					operator = segment;
					break;
				case 2:
					verifier = segment;
					break;
				}
				index++;
			}
		}
	}
	
	public void setValid(boolean valid){
		this.valid = valid;
	}

	
	public List<String> getSegments(){
		return this.segments;
	}

	/**
	 * @return the operator
	 */
	public String getOperator(){
		return operator;
	}
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator){
		this.operator = operator;
	}
	/**
	 * @return the path
	 */
	public String getPath(){
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path){
		this.path = path;
	}
	/**
	 * @return the verifier
	 */
	public String getVerifierSegment(){
		return verifier;
	}
	
	/**
	 * Returns the verifier appropriate for verification.<br><br>
	 * This means that for tokens presented as Strings in the verification point
	 * UI, the exterior quotes need to be removed.
	 * @return verifier
	 */
	public String getVerifier(){
		String _verifier = verifier;
		final String pattern = "\"(.+)\"";
		if(verifier.matches(pattern)){
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(verifier);
			if(m.matches()){
				if(m.groupCount() > 0){
					_verifier = m.group(1);
				}
			}
		}
		return _verifier;
	}
	
	/**
	 * Returns whether the value being used for verification is a string.<br><br>
	 * Utilized mainly for presentation (results).
	 * @return <b>true</b> if the verifier matches a String format
	 */
	public boolean verifierIsString(){
		String _verifier = verifier;
		final String pattern = "\"(.+)\"";
		if(verifier.matches(pattern)){
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(verifier);
			if(m.matches()){
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @return
	 */
	public Integer getVerifierAsInteger(){
		Integer _verifier = null;
		try{
			_verifier = Integer.parseInt(verifier);
		}catch(NumberFormatException e){
			System.err.println();
		}
		return _verifier;
	}
	
	public Double getVerifierAsDouble(){
		Double _verifier = null;
		try{
			_verifier = Double.parseDouble(verifier);
		}catch(NumberFormatException e){
			System.err.println();
		}
		return _verifier;
	}
	public Float getVerifierAsFloat(){
		Float _verifier = null;
		try{
			_verifier = Float.parseFloat(verifier);
		}catch(NumberFormatException e){
			System.err.println();
		}
		return _verifier;
	}
	public Boolean getVerifierAsBoolean(){
		Boolean _verifier = null;
		try{
			_verifier = Boolean.parseBoolean(verifier);
		}catch(NumberFormatException e){
			System.err.println();
		}
		return _verifier;
	}
	
	public <T> T getVerifierAs(T t){
		T _verifier = null;
		if(t instanceof Double){
			Double d = (Double) t;
			try{
				Double value = Double.parseDouble(verifier);
				return (T) value;
			}catch(NumberFormatException e){
				System.err.println();
			}
		}else if(t instanceof Integer){
			Integer i = (Integer)t;
			try{
				Integer value = Integer.parseInt(verifier);
				return (T) value;
			}catch(NumberFormatException e){
				System.err.println();
			}
		}
		return _verifier;
	}



	/**
	 * @param verifier the verifier to set
	 */
	public void setVerifier(String verifier){
		this.verifier = verifier;
	}

	public boolean isValid(){
		return this.valid;
	}
	
	public boolean setVar(){
		if(this.path.equals("set")){
			if(this.operator.matches("[A-Za-z_0-9]+")){
				return true;
			}
		}
		return false;
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		boolean first = true;
		for(String segment : segments){
			if(!first)
				bldr.append(" ");
			bldr.append(segment);
			first = false;
		}
		return bldr.toString();
	}



	/**
	 * @return the variableName
	 */
	public String getVariableName(){
		return variableName;
	}



	/**
	 * @param variableName the variableName to set
	 */
	public void setVariableName(String variableName){
		this.variableName = variableName;
	}
	
}
