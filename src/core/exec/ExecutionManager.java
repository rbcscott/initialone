package core.exec;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import core.test.RunnableTest;
import customproject.report.Mailout;


public class ExecutionManager{

	public static void main(String[] args){
		loadAndExecute();
	}
	
	private static void loadAndExecute(){
		//load();
		execute();
	}
	
	private static void execute(){
		int currentInstance = 0;
		boolean reachedThreadPoolCount = false;
		int threadCount = 50;
		int threadPoolSize = 10;
		ExecutorService threadPool;
		int testCount = 0;
		
		threadPool = Executors.newFixedThreadPool(threadPoolSize);
		for(int currentTestInstance = 0; currentTestInstance < threadCount; currentTestInstance++){
			currentInstance = currentTestInstance;
			RunnableTest test = new RunnableTest(currentTestInstance + 1);
			threadPool.execute(test);
			
		}

		System.out.println("Test count: " + testCount);
		threadPool.shutdown();
		while(!threadPool.isTerminated()){
			try{
				System.out.println("ThreadPool is running. Waiting...");
				Thread.sleep(3000);
			} catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
		Mailout mo = new Mailout();
		mo.sendMail(null);
	}
}
