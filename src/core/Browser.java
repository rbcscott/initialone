package core;

/**
 * Representation of the browsers used for execution.
 */
public enum Browser{
	// Android("android"),
	CHROME("chrome", "Chrome"),
	FIREFOX("firefox", "Firefox"),
	HTML_UNIT("htmlunit", "HtmlUnit"),
	INTERNET_EXPLORER("internet explorer", "Internet Explorer"),
	MICROSOFT_EDGE("microsoftedge", "Microsoft Edge"),
	EDGE("edge", "Microsoft Edge"),
	IPAD("ipad", "iPad"),
	ANDROID("android", "Android"),
	SAFARI("safari", "Safari");

	private String value;
	private String displayValue = "";
	private static Browser defaultBrowser = CHROME;

	private Browser(String value, String displayValue){
		this.value = value;
		this.displayValue = displayValue;
	}

	/**
	 * The value is the base value representing the browser - passed into methods 
	 * or APIs that might expect one as String. For most internal (project) use,
	 * the methods should accept the enumerated type.<br><br>
	 * <ul>
	 * <li>CHROME: "chrome"
	 * <li>FIREFOX: "firefox"
	 * <li>HTML_UNIT: "htmlunit"
	 * <li>INTERNET_EXPLORER: "internet explorer"
	 * <li>MICROSOFT_EDGE: "microsoftedge"
	 * <li>EDGE: "edge"
	 * <li>IPAD: "ipad"
	 * <li>ANDROID: "android"
	 * <li>SAFARI: "safari"
	 * </ul>
	 * @return String value of the Browser type
	 */
	public String getValue(){
		return value;
	}

	/**
	 * Returns a displayable value of the Browser type, something suitable
	 * for general presentation of known browsers.
	 * @return
	 */
	public String getDisplayValue(){
		return this.displayValue;
	}

	/**
	 * Returns a Browser type based on the given browser string. This will attempt to match
	 * with values set to lower case. (current enum string values are set as lower case) 
	 * Note, also, that spaces are removed (e.g. if given <code style='color:blue;'>"Internet Explorer"</code> it
	 * will be compared as <code style='color:blue;'>"internetexplorer"</code>).<br><br>
	 * Default return is the default set type (<code>InternetExplorer</code>, if not otherwise 
	 * set by {@link #setDefault(Browser)})
	 * @param browserName
	 * @return matching Browser type
	 */
	public static Browser getBrowser(String browserName){
		for(Browser b : Browser.values()){
			String thisbrowserStr = b.value.replaceAll(" ", "");
			String trimBrowserName = browserName.replaceAll(" ", "");
			if(thisbrowserStr.equalsIgnoreCase(trimBrowserName))
				return b;
		}
		System.out.println("Browser " + browserName + " did not match any in Browser enum set.");
		return defaultBrowser;
	}

	/**
	 * Set default browser to be used when a default is needed. Will stand for
	 * the life of execution, or until called upon again.
	 * @param browser
	 */
	public static void setDefault(Browser browser){
		defaultBrowser = browser;
	}

	/**
	 * Returns the default browser set. The initialized set default is
	 * <code>InternetExplorer</code>, and can be changed by {@link #setDefault(Browser)}
	 * @return
	 */
	public static Browser getDefault(){
		return defaultBrowser;
	}

}
