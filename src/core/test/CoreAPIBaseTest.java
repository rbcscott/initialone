/**
 *@BaseComponentName: BaseTest
 *@Description:  
 *@author: 
 *@CreatedDate:
 *@ModifiedBy:
 *@ModifiedDate:
 *@param: 
 *@return:
 */

package core.test;
import java.util.EnumSet;
import java.util.HashMap;
import org.apache.log4j.Logger;
//import org.openqa.selenium.WebDriver;
import core.report.Reporting.Status;
import core.settings.BaseSettings;
import core.test.TestParameters;
import db.Record;
import db.RecordSet;

/**
 * Contains the most common essential features and attributes for test case execution.<br><br>
 * This class should be extended for customized project execution.<br>
 * Some methods are implemented (those considered without customization), while others remain
 * abstract. Required implementations for extensions:
 * <ul>
 * <li>initializeWebDriver() - this may have a basic implementation at some point
 * <li>runScript() - to be overridden in the test class itself
 * </ul>
 */
public abstract class CoreAPIBaseTest{

	//public WebDriver driver;
	
	/** PASS - No issues, with a clean verification or execution.<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status PASS = Status.PASS;
	/** FAIL - Issue(s) or problem in verification or execution.<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status FAIL = Status.FAIL;
	/** SKIP - Skips the test case based on the conditions and DB data<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status SKIP = Status.SKIP;
	/** DONE - Execution of step completed.<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status DONE = Status.DONE;
	/** WARNING - low priority issue in verification. Not a show-stopper, but deserves attention.<br><br>
 		[Forwarded from Reporting.Status for convenience] */
	protected Status WARNING = Status.WARNING;
	/** BLOCKED - Problem (direct or indirect) preventing further proper verification or execution.<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status BLOCKED = Status.BLOCKED;
	/** INCOMPLETE - Testing did not complete.<br><br>
		[Forwarded from Reporting.Status for convenience] */
	protected Status INCOMPLETE = Status.INCOMPLETE;
	
	private boolean testingStarted = false;
	private boolean testingComplete = false;
	protected Status testStatus = null;
	protected static boolean tpOverride = false;
	

	public HashMap<String, String> mapDataSheet;
	private RecordSet testData = null;
	public String testBrowser;
	
	private BaseSettings settings = null;
	protected Logger logger = null;
	
	/**
	 * Local instance of TestParameters. Initialized in the constructor, and values
	 * assigned after settings have been established.
	 */
	protected TestParameters tp = null;

	/**
	 * Manages the TestParameters used in each test instance.<br><br>
	 * A TestParameters object will be created for each test that gets run,
	 * initialized in the test. Stored here with access (directly) via 
	 * <code>testParameters.get()</code>, or by using {@link #getTestParameters()}.
	 */
	protected static ThreadLocal<? extends TestParameters> testParameters;


	/**
	 * Initializer for the CoreBaseTest.<br>
	 * Sets TestParameter initialization if not already set by a sub-class.
	 */
	public CoreAPIBaseTest()  {
		if(!tpOverride){
			setTestParameters(); 
		}
		//tp = testParameters.get();
	}
	
	private void setTestParameters(){
		testParameters = new ThreadLocal<TestParameters>(){
			protected TestParameters initialValue(){
				tpOverride = true;
				return new TestParameters();
			}
		};
	}

	/**
	 * @param browser
	 * @param mapDataSheet
	 */
	public CoreAPIBaseTest(String testName, String browser,
			HashMap<String, String> mapDataSheet) {
		getTestParameters().setTestName(testName);
		this.testBrowser = browser;
		this.mapDataSheet = mapDataSheet;

	}

	/**
	 * Main driver for test case execution. <br><br>
	 * The <code>runScript()</code> call (script step-wise execution) is wrapped into 
	 * this method, and prior to running, parameters, data and the driver 
	 * are all setup.
	 */
	protected void executeTest(){
		setDefaultTestParameters();
		//initializeData(getTestParameters()); use in the extension
		initializeWebDriver();
		// report and set values for start of test
		markTestStarted();
		// execution of test script content...
		runScript();
		// report and set values for end of test
		markTestEnded();
		//updateResults();
	}
	
	/**
	 * Sets the parameters for this test case based on the 
	 * known settings from the global settings instance. 
	 */
	protected void setDefaultTestParameters(){
		// initialize test parameters
		TestParameters tp = getTestParameters();
		
		String className = this.getClass().getSimpleName();
		tp.setTestName(className);
		// sc - the below is project specific - should be handled in the override
		//tp.setEnvironment(settings.getApplicationEnvironment());  
		tp.setPlatform(settings.getPlatform());
		tp.setBrowser(settings.getBrowser());
		
	}
	
	
	
	/**
	 * Convenience method for obtaining the TestParameters instance
	 * associated with the current test (managed by {@link #testParameters})
	 * @return instance of {@link TestParameters}, specific to this test. If the
	 * instance has not been created, it will be, however values are not
	 * assigned until {@link #setDefaultTestParameters()} has been called.
	 */
	public static TestParameters getTestParameters(){
		return testParameters.get();
	}
	
	/**
	 * This method should be responsible for checking run parameters
	 * (such as local/grid, browser, etc.) and appropriately instantiating
	 * the WebDriver instance used by the test.
	 */
	protected abstract void initializeWebDriver();
	
	
	/**
	 * Main executor for the script. Typically will contain the medium/high level
	 * steps for the test case, interacting with both logical and page modules.
	 */
	public abstract void runScript();
	

	

	/**
	 * Based on a cascading series of variable and verification point checks,
	 * returns the overall test Status. <code>BLOCKED</code> and <code>NOT_RUN</code> 
	 * status are checked first, followed by a check of VPs. If test has completed with no major
	 * posted status flags [<code>FAIL</code>, <code>WARNING</code>, <code>BLOCKED</code>], 
	 * a Status of <code>PASS</code> will be returned.
	 * @return overall test Status, derived from verification results and/or
	 * current test variables.
	 */
	public final Status getTestStatus(){
		// should the test be set with a blocked status, this will 
		if(this.testStatus == Status.BLOCKED){
			return BLOCKED;
		}
		// if this is called against the test prior to starting...
		if(!testingStarted){
			return Status.NOT_RUN;
		}
		// check for issues within verification points...
		if(tp != null){
			RecordSet records = tp.getVPData();
			EnumSet<Status> statusSet = EnumSet.noneOf(Status.class);
			for(Record<String> record : records){
				String vpStatus = record.get("STATUS");
				Status s = Status.getStatus(vpStatus);
				if(s != null)
					statusSet.add(s);
			}
			if(statusSet.contains(FAIL)){
				return FAIL;
			}else if(statusSet.contains(BLOCKED)){
				return BLOCKED;
			}else if(statusSet.contains(WARNING)){
				return WARNING;
			}
		}
		// if no issues, but has not completed yet ...
		if(!testingComplete){
			return INCOMPLETE;
		}
		// if test has completed with no issues...
		return PASS;
	}
	
	/**
	 * Sets the overall test status to {@link Status#BLOCKED} for situations
	 * where outside factors are preventing any possible valid run of this case.
	 */
	protected void setTestStatusToBlocked(){
		this.testStatus = BLOCKED;
	}
	
	
	/**
	 * Returns the RecordSet of data assigned to this test case.
	 * @return  RecordSet of data for this test case
	 */
	public RecordSet getTestData(){
		return this.testData;
	}

	/**
	 * Gets the value contained in the local data instance.<br><br>
	 * If the data has not been set or populated, this will return <b>null</b>.<br>
	 * Implementers should override if necessary.
	 * @param field name of field/column to retrieve
	 * @return String value (if field and data are found), otherwise <b>null</b>
	 */
	public String getValue(String field){
		if(testData != null){
			return testData.get(field);
		}
		return null;
	}

	
	
	/**
	 * Method will set the value of the testingStarted variable to true
	 * to mark that the test has begun.<br><br>
	 * Overrides should call this super implementation first.
	 */
	protected void markTestStarted(){
		testingStarted = true;
	}
	
	/**
	 * Method will set the value of the testingComplete variable to true
	 * to mark that the test has ended.<br><br>
	 * Overrides should call this super implementation first.
	 */
	protected void markTestEnded(){
		testingComplete = true;
	}
	
	
	
	
	
	
	
	
	
}
