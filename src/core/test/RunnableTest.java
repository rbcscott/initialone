package core.test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;



public class RunnableTest implements Runnable{
	private TestParameters testParameters;
	//private Date startTime, endTime;
	private String testStatus;
	private int count = 0;

	/**
	 * Constructor to initialize the details of the test case to be executed
	 * @param testParameters The {@link SeleniumTestParameters} object (passed from the {@link Allocator})
	 * @param report The {@link SeleniumReport} object (passed from the {@link Allocator})
	 */
	public RunnableTest(TestParameters testParameters){
		super();
		this.testParameters = testParameters;
	}
	public RunnableTest(int count){
		super();
		this.count = count;
		//this.testParameters = testParameters;
	}
	
	
	@Override
	/**
	 * Invokes the test script and updates the report.
	 */
	public void run(){
		invokeTestScript2();
	}

	private void invokeTestScript2(){
		String strBrowserType = null;
		Object startTime = System.currentTimeMillis();
		System.out.println("Executing test [count:" + count + "]");
		try{
			Thread.sleep(2000);
		}catch (InterruptedException e){
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
	}
	
	private String invokeTestScript(){
		String functionalArea = testParameters.getFunctionalArea().toLowerCase();
		String location = "functional." + functionalArea + ".scripts.";
		String testName = testParameters.getTestName();
		try{
			Class<?> testScriptClass = Class.forName(location + testName);
			Object testScript = testScriptClass.newInstance();

			Field testParameters = testScriptClass.getSuperclass().getDeclaredField("testParameters");
			testParameters.setAccessible(true);
			testParameters.set(testScript, this.testParameters);

			if(testScriptClass.getSuperclass().getSuperclass().toString().contains("DriverScript")){
				testParameters = testScriptClass.getSuperclass().getSuperclass().getDeclaredField("testParameters");
				testParameters.setAccessible(true);
				testParameters.set(testScript, this.testParameters);
			}

			Method driveTestExecution = testScriptClass.getMethod("driveTestExecution", (Class<?>[]) null);
			driveTestExecution.invoke(testScript, (Object[]) null);

			Field testReport = testScriptClass.getSuperclass().getDeclaredField("report");
			testReport.setAccessible(true);
		} catch(ClassNotFoundException e){
			testStatus = "Reflection Error - ClassNotFoundException";
			e.printStackTrace();
		} catch(IllegalArgumentException e){
			testStatus = "Reflection Error - IllegalArgumentException";
			e.printStackTrace();
		} catch(InstantiationException e){
			testStatus = "Reflection Error - InstantiationException";
			e.printStackTrace();
		} catch(IllegalAccessException e){
			testStatus = "Reflection Error - IllegalAccessException";
			e.printStackTrace();
		} catch(SecurityException e){
			testStatus = "Reflection Error - SecurityException";
			e.printStackTrace();
		} catch(NoSuchFieldException e){
			testStatus = "Reflection Error - NoSuchFieldException";
			e.printStackTrace();
		} catch(NoSuchMethodException e){
			testStatus = "Reflection Error - NoSuchMethodException";
			e.printStackTrace();
		} catch(InvocationTargetException e){
			testStatus = "Failed";
			e.printStackTrace();
		}

		return testStatus;
	}
}

