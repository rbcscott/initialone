package core.test;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;

import core.Browser;
import db.RecordSet;

/**
 * Container class for the various test parameters set up by properties
 * and/or run managers or controllers.<br><br>
 * Many standard and common settings will be contained in this base class.
 */
public class TestParameters {

	protected Browser browser = null;
	protected String browserVersion = "";
	protected String environment = "";
	protected Platform platform = null;
	protected String runmode = "";
	protected int iteration = 1;
	protected int internalIteration = 1;
	protected String vmName = "";
	protected String testName = "";
	protected String testShortName = "";
	protected String functionalCategory = "";
	protected String functionalArea = "";
	protected WebDriver driver = null;
	protected Set<Cookie> cookieStore = new HashSet<>();
	protected RecordSet testData = null;
	protected RecordSet vpData = new RecordSet();

	/**
	 * @return the testName
	 */
	public String getTestName() {
		return testName;
	}
	/**
	 * @param testName the testName to set
	 */
	public void setTestName(String testName) {
		this.testName = testName;
		this.testShortName = getTestShortName(testName);
		/*String fcAbbrev = getFunctionalAcronymFromShortName(testShortName);
		String fcCat = BaseTestSql.getFunctionalName(fcAbbrev);
		String fcArea = BaseTestSql.getFunctionalArea(fcAbbrev);
		this.functionalCategory = fcCat;
		this.functionalArea = fcArea;*/
		
	}
	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}
	/**
	 * @param environment the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getbuildId(){
		return null;
	}

	public String getCurrentTestcase(){
		return null;
	}

	public String getMacSpecificPlatform(){
		return null;
	}
	public String getRunmode(){
		return runmode;
	}
	public void setRunmode(String runmode){
		this.runmode = runmode;
	}
	public int getIteration(){
		return iteration;
	}
	public void setIteration(int iteration){
		this.iteration = iteration;
	}
	public int getInternalIteration(){
		return internalIteration;
	}
	public void setInternalIteration(int internalIteration){
		this.internalIteration = internalIteration;
	}
	public String getVMName(){
		return vmName;
	}
	public void setVMName(String strVMName){
		this.vmName = strVMName;
	}
	public Browser getBrowser(){
		return browser;
	}
	public void setBrowser(Browser browser){
		this.browser = browser;
	}
	
	public Platform getPlatform(){
		return platform;
	}
	public void setPlatform(Platform platform){
		this.platform = platform;
	}
	public String getBrowserVersion(){
		return browserVersion;
	}
	public void setBrowserVersion(String browserVersion){
		this.browserVersion = browserVersion;
	}
	
	public String getTestShortName(){
		return testShortName;
	}
	public void setTestShortName(String testShortName){
		this.testShortName = testShortName;
	}
	
	/**
	 * Output of the contained test parameters in a multi-line listing of "key: value".
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("TestCase:          " + testName + "\n");
		sb.append("TestCase (short):  " + testShortName + "\n");
		sb.append("Category:          " + functionalCategory + "\n");
		sb.append("Area:              " + functionalArea + "\n");
		sb.append("Platform:          " + platform + "\n");
		sb.append("Browser:           " + ((browser != null) ? browser.getDisplayValue() : "null") + "\n");
		sb.append("Browser Version:   " + browserVersion + "\n");
		sb.append("Environment:       " + environment + "\n");
		sb.append("WebDriver:         " + driver + "\n");
		sb.append("VM Name:           " + vmName + "\n");
		sb.append("Test Data:         \n" + testData + "\n");
		return sb.toString();
	}
	
	/**
	 * Based on the presence of the underscore '_' to indicate
	 * a separation in the encoded name and the descriptive name, this will
	 * extract the first part (encoded name) and return. Should conditions
	 * not be met, the original given name is returned.
	 * @param testName
	 * @return
	 */
	private static String getTestShortName(String testName){
		if(testName.contains("_")){
			String [] split = testName.split("_");
			if(split.length > 1){
				return split[0];
			}
		}
		return testName;
	}
	
	/**
	 * Based on a set functional category, gets the functional name.<br><br>
	 * @param abbrev
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String getFunctionalCategoryFromAbbrev(String abbrev){
		String	name = BaseTestSql.getFunctionalName(abbrev);
		return name;
	}
	/**
	 * Gets the first 2-3 letter acronym from the given short name
	 * @param testShortName
	 */
	@SuppressWarnings("unused")
	private static String getFunctionalAcronymFromShortName(String testShortName){
		final String SHORTNAME_MATCH = "([A-Za-z]{1,3})[0-9]{1,3}";
		Pattern p = Pattern.compile(SHORTNAME_MATCH);
		Matcher m = p.matcher(testShortName);
		String fc = "";
		if(m.find()){
			fc = m.group(1);
		}else{
			System.err.println("unable to validate test case name");
		}
		return fc;
	}
	
	public String getFunctionalCategory(){
		return functionalCategory;
	}
	public void setFunctionalCategory(String functionalCategory){
		this.functionalCategory = functionalCategory;
	}
	
	public String getFunctionalArea(){
		return functionalArea;
	}
	/**
	 * Returns the current set of cookies stored
	 * for this test.<br><br>
	 * @return current set of cookies (empty set if none have been added)
	 */
	public Set<Cookie> getCookieStore(){
		return cookieStore;
	}
	/**
	 * Adds the cookies to this tests storage.<br><br>
	 * The given set is added to the existing set. If any 
	 * of the cookies already exist, it will be overwritten 
	 * with new cookie data.
	 */
	public void addCookiesToStore(Set<Cookie> cookies){
		for(Cookie c : cookies){
			addCookieToStore(c);
		}
	}
	/**
	 * Adds a cookie to this tests storage.<br><br>
	 * The given cookie is added to the set. If the cookie
	 * already exists, it will be overwritten with new cookie 
	 * data.
	 */
	public void addCookieToStore(Cookie cookie){
		if(cookie != null){
			cookieStore.add(cookie);
		}
	}
	
	public RecordSet getTestData(){
		return testData;
	}
	public void setTestData(RecordSet testData){
		this.testData = testData;
	}
	
	public RecordSet getVPData(){
		return vpData;
	}
	
	public void setVPData(RecordSet vpData){
		this.vpData = vpData;
	}
	
}

