package core.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import core.api.DataDriver;
import core.api.HttpMethod;
import core.api.TestScriptField;
import core.api.TestType;
import core.api.URL;
import core.api.Variable;
import core.api.VerificationPoint;
import nu.xom.Element;
import db.Record;
import db.RecordSet;
import static core.api.TestScriptField.*;

public class APITestScript{
	
	private String name;
	private String description = "";
	private String id;
	private String label;
	private String service = "";
	private HttpMethod method;
	private URL url;
	private String endpointResource = "";
	private String authorization = "";
	private String environment = "";
	private String origTestURL = "";
	private TestType testType = null;
	private String group = "";
	private String jiraId = "";
	private Map<String, String> parameters = new LinkedHashMap<>();
	private Map<String, String> headers = new LinkedHashMap<>();
	private List<VerificationPoint> vps = new ArrayList<VerificationPoint>();
	private List<Variable> variables = new ArrayList<Variable>();
	private List<DataDriver> dataDrivers = new ArrayList<>();
	private String body = "";
	
	private boolean valid = false;

	public APITestScript(String name, String label) {
		this.name = name;
		this.setLabel(label);
	}

	public String getName() {
		return name;
	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	/**
	 * Id - reserved for a unique identifier setting
	 * @return
	 */
	public String getId(){
		return id;
	}

	/**
	 * Sets the id for this script instance. This is reserved for a 
	 * unique identifier application.
	 * @param id
	 */
	public void setId(String id){
		this.id = id;
	}

	public String getService(){
		return service;
	}

	public void setService(String service){
		this.service = service;
	}

	public URL getUrl(){
		return url;
	}
	
	/**
	 * Builds new URL based on the server and service settings.<br><br>
	 * 
	 * @return
	 */
	public URL getBuildUrl(){
		StringBuilder bldr = new StringBuilder();
		//bldr.append(b)
		
		//return new URL(bldr.toString());
		return new URL(this.origTestURL);
	}

	public void setUrl(URL url){
		this.url = url;
	}

	public String getEndpointResource(){
		return endpointResource;
	}

	public void setEndpointResource(String endpointResource){
		this.endpointResource = endpointResource;
	}

	public String getEnvironment(){
		return environment;
	}

	public void setEnvironment(String environment){
		this.environment = environment;
	}

	public Map<String, String> getParameters(){
		return parameters;
	}

	public void setParameters(Map<String, String> parameters){
		this.parameters = parameters;
	}

	public Map<String, String> getHeaders(){
		return headers;
	}

	public void setHeaders(Map<String, String> headers){
		this.headers = headers;
	}

	public List<VerificationPoint> getVps(){
		return vps;
	}

	public void setVps(List<VerificationPoint> vps){
		if(vps != null)
			this.vps = vps;
	}
	
	public void setDataDrivers(List<DataDriver> dds){
		if(dds != null)
			this.dataDrivers = dds;
	}

	public String getBody(){
		return body;
	}

	public void setBody(String body){
		this.body = body;
	}

	public void setName(String name){
		this.name = name;
	}

	public HttpMethod getMethod(){
		return method;
	}

	public void setMethod(HttpMethod method){
		this.method = method;
	}
	
	private void validate(){
		
	}

	/**
	 * @return the authorization
	 */
	public String getAuthorization(){
		return authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(String authorization){
		this.authorization = authorization;
	}

	/**
	 * @return the label
	 */
	public String getLabel(){
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label){
		this.label = label;
	}

	/**
	 * @return the variables
	 */
	public List<Variable> getVariables(){
		return variables;
	}
	/**
	 * @return the dataDrivers
	 */
	public List<DataDriver> getDataDrivers(){
		return dataDrivers;
	}
	/**
	 * Adds the given data driver to this collection (provided the driver does not already
	 * exist).
	 * @param driver
	 * @return <b>true</b> if a driver was added with success (did not already exist), 
	 * 		<b>false</b> otherwise.
	 */
	public boolean addDataDriver(DataDriver driver){
		boolean exists = false;
		for(DataDriver _driver : dataDrivers){
			if(_driver.equals(driver)){
				exists = true;
				break;
			}
		}
		if(!exists){
			this.dataDrivers.add(driver);
			return true;
		}
		return false;
	}
	/**
	 * Removes the given data driver from this collection.
	 * @param driver
	 * @return <b>true</b> if a driver was removed (existed in the set), 
	 * 		<b>false</b> otherwise.
	 */
	public boolean removeDataDriver(DataDriver driver){
		boolean exists = false;
		for(DataDriver _driver : dataDrivers){
			if(_driver.equals(driver)){
				exists = true;
				break;
			}
		}
		if(exists){
			this.dataDrivers.remove(driver);
			return true;
		}
		return false;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<Variable> variables){
		this.variables = variables;
	}

	/**
	 * @return the origTestURL
	 */
	public String getOrigTestURL(){
		return origTestURL;
	}

	/**
	 * @param origTestURL the origTestURL to set
	 */
	public void setOrigTestURL(String origTestURL){
		this.origTestURL = origTestURL;
	}
	
	public TestType getTestType(){
		return testType;
	}
	
	public String getTestTypeToString(){
		return testType == null ? "" : testType.toString();
	}

	public void setTestType(TestType testType){
		this.testType = testType;
	}

	public String getGroup(){
		return group;
	}

	public void setGroup(String group){
		this.group = group;
	}

	public String getJiraId(){
		return jiraId;
	}

	public void setJiraId(String jiraId){
		this.jiraId = jiraId;
	}

	public void setValid(boolean valid){
		this.valid = valid;
	}

	public Map<String, String> getAttributesAsMap(){
		Map<String, String> attributes = new LinkedHashMap<String, String>();
		attributes.put("Name", getName());
		attributes.put("Label", getLabel());
		attributes.put("Description", getDescription());
		attributes.put("Service", getService());
		attributes.put("Endpoint Resource", getEndpointResource());
		attributes.put("Method", getMethod() == null ? "" : getMethod().name());
		attributes.put("Body", getBody());
		attributes.put("Data Driven Elements", dataDrivers.size() > 0 ? "true" : "false");
		
		//attributes.put("URL", (getUrl() == null ? "" : getUrl().getUrl()));
		attributes.put("Original Test URL", getOrigTestURL());
		attributes.put("TestType", getTestTypeToString());
		attributes.put("Group", getGroup());
		attributes.put("Jira ID", getJiraId());
		attributes.put("Authorization", getAuthorization());
		
		return attributes;
	}
	
	
	
	public boolean checkValidity(){
		// required fields...
		if(getName() == null || getName().equals("")){
			return false;
		}
		if(getLabel() == null || getLabel().equals("")){
			return false;
		}
		if(getService() == null || getService().equals("")){
			return false;
		}
		if(getEndpointResource() == null || getEndpointResource().equals("")){
			return false;
		}
		if(getMethod() == null){
			return false;
		}
		// combinations...
		return true;
	}

	public void applyToRecord(Record<String> r){
		r.set(TEST_CASE_ID.toString(), "");
		r.set(TEST_CASE_NAME.toString(), name);
		r.set(TEST_CASE_LABEL.toString(), label);
		r.set(TEST_CASE_DESCRIPTION.toString(), description);
		r.set(SERVICE_NAME.toString(), service);
		r.set(METHOD.toString(), method == null ? "" : method.name());
		r.set(SERVICE_ENDPOINT.toString(), endpointResource);
		r.set(AUTHORIZATION.toString(), authorization);
		r.set(ORIGINAL_TEST_URL.toString(), origTestURL);
		r.set(TEST_TYPE.toString(), testType == null ? "" : testType.name());
		r.set(GROUP.toString(), group);
		r.set(JIRA_ID.toString(), jiraId);
		r.set(BODY.toString(), body);
		LinkedHashMap<String, RecordSet> rsMap = new LinkedHashMap<String, RecordSet>();

		RecordSet hdrSet = new RecordSet();
		if(headers != null){
			int i = 0;
			for(String s : headers.keySet()){
				hdrSet.addElement(i, "Name", s);
				hdrSet.addElement(i, "Value", headers.get(s));
				i++;
			}
			rsMap.put("Headers", hdrSet);
		}
		r.addElement("Headers", "customType");

		RecordSet prmSet = new RecordSet();
		if(parameters != null){
			int i = 0;
			for(String s : parameters.keySet()){
				prmSet.addElement(i, "Name", s);
				prmSet.addElement(i, "Value", parameters.get(s));
				i++;
			}
			rsMap.put("Parameters", prmSet);
		}
		r.addElement("Parameters", "customType");

		RecordSet verSet = new RecordSet();
		if(vps != null){
			int i = 0;
			for(VerificationPoint v : vps){
				int k = 1;
				for(String segment : v.getSegments()){
					verSet.addElement(i, "Segment" + k, segment);
					k++;
				}
				i++;
			}
			rsMap.put("Verifications", verSet);
		}
		r.addElement("Verifications", "customType");

		RecordSet varSet = new RecordSet();
		if(variables != null){
			int i = 0;
			for(Variable s : variables){
				varSet.addElement(i, "Name", s.getName());
				varSet.addElement(i, "Type", s.getType().toString());
				varSet.addElement(i, "Scope", s.getScope().toString());
				varSet.addElement(i, "Value", s.getValue());
				i++;
			}
			rsMap.put("Variables", varSet);
		}
		r.addElement("Variables", "customType");
		r.setObject(rsMap);
	}
	
	public Record<String> toRecord(){
		Record<String> r = new Record<>();
		r.addElement("TestCaseID", "");
		r.addElement("TestCaseName", name);
		r.addElement("TestCaseLabel", label);
		r.addElement("TestCaseDescription", description);
		r.addElement("ServiceName", service);
		r.addElement("Method", method == null ? "" : method.name());
		r.addElement("ServiceEndpoint", endpointResource);
		r.addElement("Authorization", authorization);
		r.addElement("OriginalTestURL", origTestURL);
		r.addElement("TestType", testType == null ? "" : testType.name());
		r.addElement("Group", group);
		r.addElement("JiraID", jiraId);
		r.addElement("Body", body);
		LinkedHashMap<String, RecordSet> rsMap = new LinkedHashMap<String, RecordSet>();
		
		RecordSet hdrSet = new RecordSet();
		if(headers != null){
			int i = 0;
			for(String s : headers.keySet()){
				hdrSet.addElement(i, "Name", s);
				hdrSet.addElement(i, "Value", headers.get(s));
				i++;
			}
			rsMap.put("Headers", hdrSet);
		}
		r.addElement("Headers", "customType");
		
		RecordSet prmSet = new RecordSet();
		if(parameters != null){
			int i = 0;
			for(String s : parameters.keySet()){
				prmSet.addElement(i, "Name", s);
				prmSet.addElement(i, "Value", parameters.get(s));
				i++;
			}
			rsMap.put("Parameters", prmSet);
		}
		r.addElement("Parameters", "customType");

		RecordSet verSet = new RecordSet();
		if(vps != null){
			int i = 0;
			for(VerificationPoint v : vps){
				verSet.addElement(i, "Path", v.getPath());
				verSet.addElement(i, "Path", v.getOperator());
				verSet.addElement(i, "Path", v.getVerifierSegment());
				i++;
			}
			rsMap.put("Verifications", verSet);
		}
		r.addElement("Verifications", "customType");
		
		RecordSet varSet = new RecordSet();
		if(variables != null){
			int i = 0;
			for(Variable s : variables){
				varSet.addElement(i, "Name", s.getName());
				varSet.addElement(i, "Type", s.getType().toString());
				varSet.addElement(i, "Scope", s.getScope().toString());
				varSet.addElement(i, "Value", s.getValue());
				i++;
			}
			rsMap.put("Variables", varSet);
		}
		r.addElement("Variables", "customType");
		
		r.setObject(rsMap);
		r.setAPITestScript(this);
		return r;
	}
	
	/**
	 * Builds the XML Element for this record, which can 
	 * be appended to any XML element (namely the RecordSet root
	 * that will get created for the main file).
	 * @return XML Element for this script record
	 */
	public Element toXML(){
		Element root = new Element("Record");  

		appendElement(TEST_CASE_ID, id, root);
		appendElement(TEST_CASE_NAME, name, root);
		appendElement(TEST_CASE_LABEL, label, root);
		appendElement(TEST_CASE_DESCRIPTION, description, root);
		appendElement(SERVICE_NAME, service, root);
		appendElement(METHOD, method == null ? "" : method.name(), root);
		appendElement(SERVICE_ENDPOINT, endpointResource, root);
		appendElement(AUTHORIZATION, authorization, root);
		appendElement(ORIGINAL_TEST_URL, origTestURL, root);
		appendElement(TEST_TYPE, testType == null ? "" : testType.toString(), root);
		appendElement(GROUP, group, root);
		appendElement(JIRA_ID, jiraId, root);
		root.appendChild(headersToXML());
		root.appendChild(parametersToXML());
		appendElement(BODY, body, root);
		root.appendChild(verificationsToXML());
		root.appendChild(dataDriversToXML());
		root.appendChild(variablesToXML());
		return root;
	}
	
	private void appendElement(TestScriptField field, String value, Element root){
		Element elem = new Element(field.toString());
		elem.appendChild(value);
		root.appendChild(elem);
	}
	
	private Element headersToXML(){
		Element headers = new Element(HEADERS.toString());  
		for(String key : this.headers.keySet()){
			Element header = new Element("Header");
			
			Element name = new Element("Name");
			name.appendChild(key);
			header.appendChild(name);
			
			Element value = new Element("Value");
			value.appendChild(this.headers.get(key));
			header.appendChild(value);
			
			headers.appendChild(header);
		}
		return headers;
	}
	
	private Element parametersToXML(){
		Element parameters = new Element(PARAMETERS.toString());  
		for(String key : this.parameters.keySet()){
			Element parameter = new Element("Parameter");
			
			Element name = new Element("Name");
			name.appendChild(key);
			parameter.appendChild(name);
			
			Element value = new Element("Value");
			value.appendChild((this.parameters.get(key)));
			parameter.appendChild(value);
			
			parameters.appendChild(parameter);
		}
		return parameters;
	}
	
	private Element verificationsToXML(){
		Element verifications = new Element(VERIFICATIONS.toString()); 
		if(vps != null){
			for(VerificationPoint vp : this.vps){
				Element verification = new Element("Verification");
				
				Element path = new Element("Path");
				path.appendChild(vp.getPath());
				verification.appendChild(path);
				
				Element operator = new Element("Operator");
				operator.appendChild(vp.getOperator());
				verification.appendChild(operator);
				
				Element verifier = new Element("Verifier");
				verifier.appendChild(vp.getVerifierSegment());
				verification.appendChild(verifier);
				
				verifications.appendChild(verification);
			}
		}
		return verifications;
	}
	private Element dataDriversToXML(){
		Element elemDataDrivers = new Element(DATA_DRIVERS.toString()); 
		if(dataDrivers != null){
			for(DataDriver dd : this.dataDrivers){
				if(dd != null){
					Element dataDriver = new Element("DataDriver");

					Element label = new Element("Label");
					label.appendChild(dd.getLabel());
					dataDriver.appendChild(label);

					Element owner = new Element("Owner");
					String ownerValue = dd.getOwner() == null ? "" : dd.getOwner().toString();
					owner.appendChild(ownerValue);
					dataDriver.appendChild(owner);

					Element orig = new Element("Original");
					orig.appendChild(dd.getOriginalDataReplacement());
					dataDriver.appendChild(orig);

					Element projSource = new Element("ProjectSource");
					projSource.appendChild(dd.isProjectDataSource() ? "true" : "false");
					dataDriver.appendChild(projSource);

					Element datatablePath = new Element("DataTablePath");
					datatablePath.appendChild(dd.getDataTablePath());
					dataDriver.appendChild(datatablePath);

					Element datatableCol = new Element("DataTableColumn");
					datatableCol.appendChild(dd.getDataTableColumnReference());
					dataDriver.appendChild(datatableCol);

					Element dbSource = new Element("DBSource");
					dbSource.appendChild(dd.getDbSource());
					dataDriver.appendChild(dbSource);

					Element dbQuery = new Element("DBQuery");
					dbQuery.appendChild(dd.getDbQuery());
					dataDriver.appendChild(dbQuery);

					elemDataDrivers.appendChild(dataDriver);
				}
			}
		}
		return elemDataDrivers;
	}
	
	private Element variablesToXML(){
		Element variables = new Element(VARIABLES.toString());
		if(variables != null){
			for(Variable vp : this.variables){
				Element variable = new Element("Variable");

				Element name = new Element("Name");
				name.appendChild(vp.getName());
				variable.appendChild(name);

				Element type = new Element("Type");
				type.appendChild(vp.getType().toString());
				variable.appendChild(type);

				Element scope = new Element("Scope");
				scope.appendChild(vp.getScope().toString());
				variable.appendChild(scope);

				Element value = new Element("Value");
				value.appendChild(vp.getValue());
				variable.appendChild(value);

				variables.appendChild(variable);
			}
		}
		return variables;
	}
	
	
	
	public boolean headersEqual(Map<String, String> hdrs){
		for(String key : this.headers.keySet()){
			boolean foundMatch = false;
			for(String extKey : hdrs.keySet()){
				if(extKey.equals(key)){
					foundMatch = true;
					String thisVal = this.headers.get(key);
					String extVal = hdrs.get(extKey);
					if(extVal == null ? thisVal == null : extVal.equals(thisVal)){
						continue;
					}else{
						return false;
					}
				}
			}
			if(!foundMatch){
				return false;
			}
		}
		return true;
	}
	
	public boolean parametersEqual(Map<String, String> params){
		for(String key : this.parameters.keySet()){
			boolean foundMatch = false;
			for(String extKey : params.keySet()){
				if(extKey.equals(key)){
					foundMatch = true;
					String thisVal = this.parameters.get(key);
					String extVal = params.get(extKey);
					if(extVal == null ? thisVal == null : extVal.equals(thisVal)){
						continue;
					}else{
						return false;
					}
				}
			}
			if(!foundMatch){
				return false;
			}
		}
		return true;
	}
	
	public APITestScript clone(){
		return null;
	}
	
	public String toString(){
		StringBuilder bldr = new StringBuilder();
		bldr.append("Name: " + this.name + "\n");
		bldr.append("Label: " + this.label + "\n");
		bldr.append("Service: " + this.service + "\n");
		bldr.append("Test URL: " + this.origTestURL + "\n");
		return bldr.toString();
	}
	
	public boolean equals(Object obj){
		if(obj == this)
			return true;
		if(!(obj instanceof APITestScript)){
			return false;
		}
		APITestScript script = (APITestScript) obj;
		if(!script.name.equals(this.name))
			return false;
		if(!script.label.equals(this.label))
			return false;
		if(!script.description.equals(this.description))
			return false;
		if(!script.service.equals(this.service))
			return false;
		if(!script.endpointResource.equals(this.endpointResource))
			return false;
		if(!(script.body == null ? this.body == null : script.body.equals(this.body)))
			return false;
		//if(!(script.url == null ? this.url == null : script.url.getUrl().equals(this.url.getUrl())))
		//	return false;
		if(!script.headersEqual(this.headers))
			return false;
		if(!script.parametersEqual(this.parameters))
			return false;
		
		// create extra columns in exec table for chained items and db indicator
			
		return true;
	}

}
