package core.test;

import db.*;

public class BaseTestSql{

	private static RecordSet categoryMap = null;
	
	/**
	 * Gets the mapped functional category from the given abbreviation/acronym.<br>
	 * This method will use a cached capture of the necessary table data after initial retrieval.
	 * @param fcAbbrev
	 * @return worded functional category
	 */
	public static String getFunctionalName(String fcAbbrev){
		if(categoryMap == null){
			String sql = 
					"SELECT * FROM CATEGORY_MAP ";
			JdbcOracle jdbc = new JdbcOracle();
			categoryMap = jdbc.executeSql(AutoDB.parameters(), sql);
		}
		Record<String> rec = categoryMap.getRecordWhere("FC_ABBREV", fcAbbrev);
		if(rec.isEmpty()){
			rec = categoryMap.getRecordWhere("FC_ABBREV2", fcAbbrev);
		}
		return rec.get("CATEGORY_DB_NAME");
	}
	
	/**
	 * Gets the mapped functional area from the given abbreviation/acronym.<br>
	 * This method will use a cached capture of the necessary table data after initial retrieval.
	 * @param fcAbbrev
	 * @return worded functional category
	 */
	public static String getFunctionalArea(String fcAbbrev){
		if(categoryMap == null){
			String sql = 
					"SELECT * FROM CATEGORY_MAP ";
			JdbcOracle jdbc = new JdbcOracle();
			categoryMap = jdbc.executeSql(AutoDB.parameters(), sql);
		}
		Record<String> rec = categoryMap.getRecordWhere("FC_ABBREV", fcAbbrev);
		if(rec.isEmpty()){
			rec = categoryMap.getRecordWhere("FC_ABBREV2", fcAbbrev);
		}
		return rec.get("AREA");
	}
	
}
