package core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import core.report.Reporting;
import core.report.Reporting.Status;

/**
 * Provides support (reporting, et. al.) for functional and page-related module
 * implementation.<br><br>
 * As the main (superclass) library, the common and standard
 * implementations reside here. Extension classes can add
 * customized or project-specific functionality via new members
 * and methods (or overridden where necessary).
 */
public abstract class CoreLibrary{

	/** PASS - No issues, with a clean verification or execution.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status PASS = Status.PASS;
	/** FAIL - Issue(s) or problem in verification or execution.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status FAIL = Status.FAIL;
	/** SKIP - Skip the test case based on the Execution conditions<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status SKIP = Status.SKIP;
	/** DONE - Execution of step completed.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status DONE = Status.DONE;
	/** WARNING - low priority issue in verification. Not a show-stopper, but deserves attention.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status WARNING = Status.WARNING;
	/** BLOCKED - Problem (direct or indirect) preventing further proper verification or execution.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status BLOCKED = Status.BLOCKED;
	/** INCOMPLETE - Testing did not complete.<br><br>
	[Forwarded from Reporting.Status for convenience] */
	protected Status INCOMPLETE = Status.INCOMPLETE;

	protected Reporting reporting; //= Reporting.getReporting();


	
	/**
	 * Reports out using the given expected and message, with a status of DONE.
	 * @param expected
	 * @param msg
	 */
	protected void report(String expected,  String msg){
		reporting.reportVP(expected, Status.DONE, msg);
	}
	
	/**
	 * Reports out using the status and message.
	 * @param 
	 * @param expected
	 * @param msg
	 */
	protected void report(Reporting.Status status, String msg){
		reporting.report(status, msg);
	}
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String expected, String status, String msg){
		Status _status = Status.getStatus(status);
		if(_status != null){
			reporting.reportVP(expected, _status, msg);
		}
	}
	
	/**
	 * Reports out based on the given expected and actual. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param actual
	 */
	protected void reportVP(String expected, String actual){
		Status status = FAIL;
		if(expected.equalsIgnoreCase(actual)){
			status = Status.PASS;
		}
		reporting.reportVP(expected, status, "expected [" + expected + "] does " + (status == Status.PASS ? "" : "not") + " match actual [" + actual + "]");
	}
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String expected, Status status, String msg){
		reporting.reportVP(expected, status, msg);
	}
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param input
	 * @param output
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String input, String output, String expected, Status status, String msg){
		reporting.reportVP(input, output, expected, status, msg);
	}
	/**
	 * Returns a formatted version of the date/time that is safe 
	 * for file use (no special characters, and no spaces).<br><br>
	 * e.g. 2016-08-21_12-45-15_PM_EST
	 */
	public static String getCurrentDateTime(){
		Calendar currentdate = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a z");
		TimeZone timeZone = currentdate.getTimeZone();
		formatter.setTimeZone(timeZone);
		String currentDateTime = formatter.format(currentdate.getTime());
		currentDateTime = currentDateTime.replaceAll(":", "-");
		currentDateTime = currentDateTime.replaceAll(" ", "_");
		return currentDateTime;
	}
	
}
