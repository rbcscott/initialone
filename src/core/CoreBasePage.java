package core;

import java.io.File;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.remote.UnreachableBrowserException;

import se.Se;
import core.report.Reporting.Status;
import core.settings.Settings;

/**
 * Contains methods for core page object support.<br><br>
 * This class should be extended for customized page object generation and Reporting/Logging.<br>
 * Most methods are implemented to provide support, with the required methods listed below:<br>
 * <i>Required implementations for extensions:</i><br>
 * [Reporting/Logging]
 * <ul>
 * <li>initializeLogger()
 * <li>initializeReporting()
 * </ul>
 */
public abstract class CoreBasePage extends CoreLibrary{

	protected WebDriver driver;
	protected static final Settings settings = null;
	protected static Logger logger = null;

	///** PASS - No issues, with a clean verification or execution.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status PASS = Status.PASS;
	///** FAIL - Issue(s) or problem in verification or execution.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status FAIL = Status.FAIL;
	///** DONE - Execution of step completed.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status DONE = Status.DONE;
	///** WARNING - low priority issue in verification. Not a show-stopper, but deserves attention.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status WARNING = Status.WARNING;
	///** BLOCKED - Problem (direct or indirect) preventing further proper verification or execution.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status BLOCKED = Status.BLOCKED;
	///** INCOMPLETE - Testing did not complete.<br><br>
	//	[Forwarded from Reporting.Status for convenience] */
	//protected Status INCOMPLETE = Status.INCOMPLETE;


	protected abstract void initializeLogger();
	protected abstract void initializeReporting();

	/**
	 * Return the currently assigned WebDriver instance. This may return null, but should not
	 * as long as the implementing page is restricted to constructing with a provided driver instance,
	 * and that instance itself is not null.
	 * @return driver instance
	 */
	public WebDriver getDriver(){
		return driver;
	}

	/**
	 * Makes a check on the load state of the page. Returns <b>true</b> if loaded
	 * and complete, <b>false</b> otherwise.<br><br>
	 * In order to wait on this state, utilize {@link #waitForPageLoad()}
	 * @return Returns <b>true</b> if loaded and complete, <b>false</b> otherwise.
	 */
	public boolean hasPageLoaded(){
		if(pageLoadComplete()){
			return true;
		}
		return false;
	}

	/**
	 * Default implementation returns the current URL obtained from 
	 * this page. (Acts the same as <code>driver.getCurrentUrl()</code>)
	 * @return current page URL
	 */
	public String getPageUrl(){
		return driver.getCurrentUrl();
	}

	/**
	 * Constructs the CorebasePage (receives and assigns the WebDriver instance).
	 * @param driver
	 */
	public CoreBasePage(WebDriver driver){
		this.driver = driver;
	}

	
	/**
	 * Returns the current URL in the driver instance.
	 */
	public String getCurrentURL(){
		return driver.getCurrentUrl();
	}

	/**
	 * Returns the current title in the document from the driver instance.
	 */
	public String getPageTitle(){
		return driver.getTitle();
	}


	/**
	 * Navigates the browser back.
	 */
	public void navigateBack(){
		try{
			driver.navigate().back();
			logger.info("The page is navigated backwards");
		}catch (WebDriverException e){
			logger.error("The page cannot be navigated backward", e.fillInStackTrace());
			throw new WebDriverException("The page cannot be navigated backward");
		}
	}

	/**
	 * Navigates the browser forward.
	 */
	public void navigateForward(){
		try{
			driver.navigate().forward();
			logger.info("The page is navigated forward");
		}catch (UnreachableBrowserException e){
			logger.error("The page cannot be navigated forward", e.fillInStackTrace());
			throw new UnreachableBrowserException("The page cannot be navigated forward");
		}
	}

	/**
	 * Refresh page.
	 */
	public void refreshPage(){
		try{
			driver.navigate().refresh();
			logger.info("The page is refreshed");
		}catch (UnreachableBrowserException e){
			logger.error("The page cannot be refreshed", e.fillInStackTrace());
			throw new UnreachableBrowserException("The page cannot be refreshed");
		}
	}

	/**
	 * 
	 */
	public void maximizeWindow(){
		try{
			driver.manage().window().maximize();
		}catch (UnreachableBrowserException e){
			throw new NoSuchElementException("Unable to maximize the window");
		}
	}

	/**
	 * 
	 */
	public String getWindowHandle(){
		String windowHandle = driver.getWindowHandle();
		logger.info("The current window handle " + windowHandle);
		try{
			windowHandle = driver.getWindowHandle();
		}catch (WebDriverException e){
			throw new WebDriverException("Unable to returm the window handle");
		}
		return windowHandle;
	}

	/**
	 * Switches to most recent opened window. Assumes last in list of obtained
	 * handles will be most recent.
	 */
	public void switchToWindow(){
		try{
			for(String windowHandle : driver.getWindowHandles()){
				driver.switchTo().window(windowHandle);
			}
			logger.info("The window is switched");
		}catch (NoSuchWindowException e){
			logger.error("Unable to switch the window: ", e.fillInStackTrace());
			throw new NoSuchWindowException("Unable to switch the window");
		}
	}

	/**
	 * Switches to window with given title. If successful (found window with title
	 * and called switch) returns <b>true</b>. Otherwise, <b>false</b> is returned if there was
	 * no match to title (attempt to switch will not have been made).
	 * @param title String indicating title of target window
	 */
	public boolean switchToWindowWithTitle(String title){
		String handle = getWindowHandleWithTitle(title);
		if(handle != null){
			driver.switchTo().window(handle);
			logger.info("Driver switched to window with title = '" + title + "'. " + handle);
			return true;
		}
		return false;
	}

	public boolean newWindowExistsBefore(int secondsToWait, int initialWindowCount){
		//String currentHandle = driver.getWindowHandle();
		int initHandleCount = initialWindowCount; //driver.getWindowHandles().size();
		System.out.println("Checking for new window [Initial count: " + initHandleCount + "]");
		int currentHandleCount = initHandleCount;
		long runningTime = 0;
		long sleepTime = 0;
		final long INTERVAL = 100;
		long start = 0;
		long end = 0;
		long compTime = 0;
		long wait = secondsToWait * 1000;
		while(runningTime < wait){
			start = System.currentTimeMillis();
			currentHandleCount = driver.getWindowHandles().size();
			if(currentHandleCount > initHandleCount){
				System.out.println("New window found [Window count: " + currentHandleCount + "]");
				return true;
			}else{
				end = System.currentTimeMillis();
				compTime = (end - start);
				sleepTime = INTERVAL - compTime;
				if(sleepTime > 5){
					runningTime += INTERVAL;
					Se.sleep(sleepTime);
				}else if(sleepTime < -10){
					runningTime += compTime;
				}
			}
		}
		return false;
	}
	

	



	/**
	 * Gets and returns the named cookie.
	 * @return cookie
	 */
	public Cookie getCookie(String cookieName){
		logger.info("The cookie:" + cookieName + " is obtained");
		return driver.manage().getCookieNamed(cookieName);
	}

	/**
	 * Attempts to delete the cookie with the given name.
	 * @param cookieName
	 */
	public void deleteCookie(String cookieName){
		
		if(cookieName != null){
			try{
				driver.manage().deleteCookieNamed(cookieName);
				report("Delete cookie", "The cookie " + cookieName + " is deleted");
				logger.info("The cookie:" + cookieName + " is deleted");
			}catch (InvalidCookieDomainException e){
				reportVP("Delete cookie", FAIL, "The cookie " + cookieName + " could not be deleted");
				logger.error("Unable to delete the cookie: ", e.fillInStackTrace());
				throw new InvalidCookieDomainException("The cookie with name " + cookieName + " cannot be deleted");
			}
		}else{
			reportVP("Delete cookie", FAIL, "The cookie name is null & could not be deleted");
			logger.info("Cookie Name is null; Unable to delete");
		}
	}

	/**
	 * Attempts to delete all cookies.
	 */
	public void deleteAllCookies(){
		
		try{
			driver.manage().deleteAllCookies();
			report("delete all cookies", "All cookies are deleted");
			logger.info("All cookies are deleted");
		}catch (InvalidCookieDomainException e){
			report("delete all cookies", "Unable to delete cookies");
			logger.error("Unable to delete cookies: ", e.fillInStackTrace());
			throw new InvalidCookieDomainException("Unable to delete cookies");
		}
	}

	/**
	 * Checks for and returns <b>true</b> if the alert exists, <b>false</b> if not.
	 * @return true if alert exists
	 */
	public boolean isAlertPresent(){
		try{
			driver.switchTo().alert();
			logger.info("An alert box is present");
			return true;
		}catch (NoAlertPresentException e){
			logger.error("There is no alert present ", e.fillInStackTrace());
			return false;
		}
	}

	/**
	 * Attempts to accept an Alert box (if present).<br><br>
	 * Following a switch to the active alert for the browser,
	 * this will make a call to accept (typically selecting
	 * the 'OK' button on an alert).
	 */
	public void acceptAlert(){
		try{
			Alert alertBox = driver.switchTo().alert();
			alertBox.accept();
			reportVP("Accept Alert", PASS, "The alert is accepted");
			logger.info("The alert is accepted");
		}catch (NoAlertPresentException e){
			reportVP("Accept Alert", FAIL, "Alert not present");
			logger.error("Unable to access the alert: ", e.fillInStackTrace());
			throw new NoAlertPresentException("Alert not present");
		}
	}

	/**
	 * Attempts to dismiss an Alert box (if present).<br><br>
	 * Following a switch to the active alert for the browser,
	 * this will make a call to dismiss (typically selecting
	 * the 'Cancel' or close button on an alert).
	 */
	public void dismissAlert(){
		try{
			Alert alertBox = driver.switchTo().alert();
			alertBox.dismiss();
			reportVP("Dismiss Alert", PASS, "The alert is dismissed");
			logger.info("The alert is dismissed");
		}catch (NoAlertPresentException e){
			reportVP("Dismiss Alert", FAIL, "Unable to dismiss alert - no alert present");
			logger.error("Unable to access the alert: ", e.fillInStackTrace());
			throw new NoAlertPresentException("Alert not present");
		}
	}

	/**
	 * Retrieves the alert message.
	 * @return message in the displayed alert box
	 */
	public String getAlertMessage(){
		String alertMessage = null;
		try{
			Alert alertBox = driver.switchTo().alert();
			alertMessage = alertBox.getText();
			String infoMsg = "The text '" + alertMessage + "' within popup is returned";
			reportVP("Get Alert text", PASS, infoMsg);
			logger.info(infoMsg);
		}catch (NoAlertPresentException e){
			reportVP("Get Alert text", FAIL, "Unable to capture alert text - no alert present");
			logger.error("Unable to access the alert:", e.fillInStackTrace());
			throw new NoAlertPresentException("Alert not present");
		}
		return alertMessage;
	}
	
	/**
	 * Performs a close on current driver instance and reports the same.
	 */
	public void closeCurrentWindow(){
		driver.close();
		report("Close current window", "Driver window is closed");
		logger.info("Driver window is closed");
	}

	/**
	 * Obtains window handle count and reports the same.
	 * @return count of window handles
	 */
	public int getWindowHandleCount(){
		int windowHandleCount = driver.getWindowHandles().size();
		report("Get window handle count", "window handle count: [" + windowHandleCount + "]");
		logger.info("Window handle count " + windowHandleCount);
		return windowHandleCount;
	}

	
	/**
	 * 
	 */
	public String convertRGBToHexValue(String rgbValue){
		String str = "";
		try{
			String color = rgbValue.toString();
			String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");

			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			str = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
		}catch (NoSuchElementException e){
			throw new NoSuchElementException("The element with" + rgbValue + " not found");
		}
		return str;
	}

	/**
	 * Convenience method for enacting a pause in execution.<br><br> 
	 * <em>Time is given in seconds.</em><br><br>
	 * <em>This method should be used judiciously, and carefully considered.</em><br><br>
	 * (utilizes <code>{@link #sleep(long)}</code>).<br><br> 
	 * @param seconds amount of time to sleep (in seconds)
	 */
	public static void sleep(int seconds){
		sleep(seconds * 1000L);
	}

	/**
	 * Convenience method for implementing delays in execution.<br><br>
	 * This method should be used judiciously - for temporary measures, or to
	 * help debug certain timing issues.<br><br>
	 * (if given time is less than 1, this method is effectively skipped)
	 * @param time in milliseconds
	 */
	public static void sleep(long time){
		if(time > 0){
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Verifies page load complete using a wait period and a check against
	 * the document ready state.<br><br>
	 * Uses a set default timeout of 30s
	 * @return <b>true</b> if ready state confirmed, <b>false</b> if timed out and not confirmed
	 */
	public boolean waitForPageLoad(){
		boolean ready = false;
		final int interval = 100;
		final int timeout = 30 * 1000;
		int counter = 0;
			while(!ready && counter <= timeout){
				sleep(100L);
				counter += interval;
				ready = pageLoadComplete();
			}
		return ready;
	}
	
	/**
	 * Makes a call into the page (via JS) to get the page status.
	 * @return <b>true</b> if document.readyState equals "complete"
	 */
	private boolean pageLoadComplete(){
		boolean ready = false;
		if(driver != null){
			String status = (String) ((JavascriptExecutor)driver).executeScript("return document.readyState");
			ready = status.equals("complete");
		}
		return ready;
	}
	
	/**
	 * Attempts to find the window with given title. If found, handle (string) is
	 * returned. Otherwise, null.
	 * @param title String title of target window
	 * @return handle to found window, <code><b>null</b></code> if not found
	 */
	private String getWindowHandleWithTitle(String title){
		Set<String> windows = driver.getWindowHandles();
		for(String window : windows){
			driver.switchTo().window(window);
			if(driver.getTitle().contains(title)){
				return window;
			}
		}
		return null;
	}
	
	/**
	 * Takes screenshot using the current driver instance.<br><br>
	 * The driver will be checked for type prior in order to ensure
	 * screenshot capability (HtmlUnit is not supported, as the UI is not rendered).
	 * @return java.io.File instance of the screenshot
	 * @throws ScreenshotException
	 */
	public File getScreenshot() throws ScreenshotException{
		File screenshotFile = null;
		// check driver type and act accordingly
		if(driver instanceof RemoteWebDriver){
			WebDriver augmentedDriver = new Augmenter().augment(driver);
			screenshotFile = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
		}else{
			screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		}
		return screenshotFile;
	}
	
	/**
	 * Reports out using the given expected and message, with a status of DONE.
	 * @param expected
	 * @param msg
	 */
	protected void report(String expected, String msg){
		reporting.reportVP(expected, Status.DONE, msg);
	}
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String expected, String status, String msg){
		Status _status = Status.getStatus(status);
		if(_status != null){
			reporting.reportVP(expected, _status, msg);
		}
	}
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String expected, Status status, String msg){
		reporting.reportVP(expected, status, msg);
	}
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param input
	 * @param output
	 * @param expected
	 * @param status
	 * @param msg
	 */
	protected void reportVP(String input, String output, String expected, Status status, String msg){
		reporting.reportVP(input, output, expected, status, msg);
	}
	
	
	/**
	 * Attempts to get the calling method for reporting purposes
	 * @return
	 */
	protected final String getMethodCalledFrom(){
		StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		String className = "";
		String methodName = "";
		for(StackTraceElement e : elems){
			className = e.getClassName();
			if(className.equals("java.lang.Thread") || className.endsWith("BasePage")){
				continue;
			}
			methodName = e.getMethodName();
			break;
		}
		return methodName;
	}
	
	
}
