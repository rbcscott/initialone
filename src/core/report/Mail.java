package core.report;

import java.io.*;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

public final class Mail{

	static java.io.PrintWriter outStream;
	static java.io.BufferedReader inStream;
	private static final String VALID_FROM = "systems.testteam@staples.com";
	private static final String MASTER_SEND = "QEAutomationArchitecture@staples.com";

	
	private Mail(){
		
	}
	/**
	 * Creates an email, sent to the given "to", with the given "subject" in the subject
	 * line. The String content is placed as-is for the email body. This email is sent
	 * <em>from</em> the systems.test@cexp.com email address and the socket used is port 25
	 * into MAILGATE.cexp.com.
	 * @param to - email address to send to
	 * @param subject - string for subject line
	 * @param content - text or html body of email
	 */
	public static void sendMail(String to, String subject, String content){
		List<String> toList = new ArrayList<String>();
		toList.add(to);
		sendMail(toList, subject, content);
	}

	/**
	 * Creates an email, sent to the given set of "to"s, with the given "subject" in the subject
	 * line. The String content is placed as-is for the email body. This email is sent
	 * <em>from</em> the systems.test@cexp.com email address and the socket used is port 25
	 * into MAILGATE.cexp.com.
	 * @param toList - email addresses to send to
	 * @param subject - string for subject line
	 * @param content - text or html body of email
	 */
	public static void sendMail(List<String> toList, String subject, String content){
		List<String> invalidAddresses = new ArrayList<String>();
		int validAddressCount = 0;
		if(toList.isEmpty()){
			java.net.Socket s = null;
			try{
				// old: "mail.host" = "USCOBRMFA-SE-70.northamerica.cexp.com"
				System.getProperties().put("mail.host", "MAILGATE.cexp.com");
				s = new java.net.Socket("MAILGATE.cexp.com", 25);
				outStream = new java.io.PrintWriter(s.getOutputStream());
				inStream = new java.io.BufferedReader(new java.io.InputStreamReader(s.getInputStream()));

				send(null);
				send("HELO " + java.net.InetAddress.getLocalHost().getHostName());
				send("MAIL FROM: <" + VALID_FROM + ">");
				for(String to : toList){
					if(to.matches("[\\w\\.\\-]+\\@[\\w\\.\\-]+\\.(com|edu|org|net|gov|mil|info){1}(\\.(\\w{2}))?$")){
						send("RCPT TO: " + "<" + to + ">");
						validAddressCount++;
					} else{
						invalidAddresses.add(to);
					}
				}
				if(validAddressCount > 0){
					send("DATA");
					StringBuilder sendToList = new StringBuilder();
					boolean first = true;
					for(String to : toList){
						sendToList.append((first ? "" : "; ") + to);
						first = false;
					}
					outStream.println("From: " + MASTER_SEND);
					outStream.println("To: " + sendToList);
					outStream.println("Subject: " + subject);
					outStream.println("Content-Type: text/html; charset=\"us-ascii\"");
					outStream.println();
					outStream.println(content);
					send(".");
				}
				if(!invalidAddresses.isEmpty()){
					for(String invalidEmail : invalidAddresses){
						System.out.println("Invalid send-to email address given: " + invalidEmail);
					}
				}
				s.close();

			} catch(Exception e){
				System.err.println(e);
			}
		} else{
			System.out.println("No email addresses present in list for sendMail(List, String, String)");
		}
	}
	
	/**
	 * Creates an email, sent to the given set of recipients, with the given subject for
	 * the subject line. The String content is placed as-is for the email body. 
	 * The file will added as an attachment. 
	 * 
	 * @param toList - email addresses to send to
	 * @param subject - string for subject line
	 * @param content - text or html body of email
	 * @param attachment - File for attachment
	 */
	public static void sendMail(List<String> toList, String subject, String content, File attachment){
		String filePath = "";
		try{
			filePath = attachment.getCanonicalPath();
		} catch(IOException e){
			System.out.println("Error in sendMail() - attachment path extraction. " + e.getMessage());
		} catch(SecurityException se){
			System.out.println("Error in sendMail() - attachment security exception. " + se.getMessage());
		}
		sendMail(toList, subject, content, filePath);
	}
	
	/**
	 * Creates an email, sent to the given set of recipients, with the given subject for
	 * the subject line. The String content is placed as-is for the email body. 
	 * The file will added as an attachment. 
	 * 
	 * @param toList - email addresses to send to
	 * @param subject - string for subject line
	 * @param content - text or html body of email
	 * @param attachment - filename for attachment
	 */
	public static void sendMail(List<String> toList, String subject, String content, String attachment){
		List<InternetAddress> validAddresses = new ArrayList<InternetAddress>();
		List<String> invalidAddresses = new ArrayList<String>();
		
		boolean debug = false;
		
		if(toList.size() > 0){
			String host = "MAILGATE.cexp.com";
			//String host = "qarelay.staples.com";
			Properties props = System.getProperties();
			props.put("mail.smtp.host", host);

			Session session = Session.getInstance(props, null);
			session.setDebug(debug);

			validAddresses = getValidAdresses(toList, invalidAddresses);
			
			if(validAddresses.size() > 0){
				try {
					// message
					MimeMessage msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(MASTER_SEND));
					InternetAddress[] addresses = validAddresses.toArray(new InternetAddress[0]);
					msg.setRecipients(Message.RecipientType.TO, addresses);
					msg.setSubject(subject);

					// content body of email
					MimeBodyPart body = new MimeBodyPart();
					body.setContent(content, "text/html");


					// create the Multipart and add parts
					Multipart mp = new MimeMultipart();
					mp.addBodyPart(body);
					if(attachment != null && !attachment.trim().equals("")){
						// attach the file
						MimeBodyPart fileAttachment = new MimeBodyPart();
						fileAttachment.attachFile(attachment);
						mp.addBodyPart(fileAttachment);
					}

					// add the Multipart to the message
					msg.setContent(mp);
					msg.saveChanges();

					// set the Date: header
					msg.setSentDate(new Date());

					// send the message
					Transport.send(msg);
					
				} catch (MessagingException mex) {
					mex.printStackTrace();
					Exception ex = null;
					if ((ex = mex.getNextException()) != null) {
						ex.printStackTrace();
					}
				} catch (IOException ioex) {
					ioex.printStackTrace();
				}
			} 
			// notify on any bad addresses
			if(!invalidAddresses.isEmpty()){
				StringBuilder strBldr = new StringBuilder();
				strBldr.append("Invalid send-to email address given:\n");
				for(String invalidAddr : invalidAddresses){
					strBldr.append(invalidAddr + "\n");
				}
				System.out.println(strBldr.toString());
			}

		} else{
			System.out.println("No email addresses present in list for sendMail(List, String, String, File)");
		}
	}

	/**
	 * Sends a string of text to the socket. This method makes
	 * a sysout call to output Client(C:) and Server(S:) responses. Pass a
	 * null value to simply wait for a response.
	 * 
	 * @param s  A string to be sent to the socket. null to just wait for a
	 *            response.
	 * @exception java.io.IOException
	 */
	protected static void send(String s) throws java.io.IOException{
		// Send the SMTP command
		if(s != null){
			System.out.println("C:" + s);
			outStream.println(s);
			outStream.flush();
		}
		// Wait for the response
		String line = inStream.readLine();
		if(line != null){
			System.out.println("S:" + line);
		}
	}
	
	private static List<InternetAddress> getValidAdresses(List<String> toList, List<String> invalidAddresses){
		List<InternetAddress> validAddresses = new ArrayList<InternetAddress>();
		for(String to : toList){
			if(to.matches("[\\w\\.\\-]+\\@[\\w\\.\\-]+\\.(com|edu|org|net|gov|mil|info){1}(\\.(\\w{2}))?$")){
				try{
					validAddresses.add(new InternetAddress(to));
				} catch(AddressException e){
					invalidAddresses.add(to);
				}
			} else{
				invalidAddresses.add(to);
			}
		}
		return validAddresses;
	}
}
