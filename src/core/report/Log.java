
package core.report;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import se.Se;


public class Log {
	/**
	 * Gets the logger based on given class name.<br><br>
	 * @param className
	 * @return log4j logger instance
     */
	public static Logger getInstance(String className)	{
	    DOMConfigurator.configure("log4j.xml");
	    Logger logger = Logger.getLogger(className);
	    Se.addLoggingProvider(new LoggerAdapter(logger));
		return logger;
	}

}
