package core.report;



public interface Reporting{
	/**
	 * Range of Status codes for use in verification results and overall
	 * testing status reporting. These represent the core representation
	 * of codes and should be reused wherever possible.
	 */
	public enum Status{
		/** PASS - No issues, with a clean verification or execution. */
		PASS,
		/** FAIL - Issues or problem in verification or execution. */
		FAIL,
		/** SKIP - Skipped test (for reasons specific to project conditions) */
		SKIP,
		/** DONE - Execution of step completed. */
		DONE,
		/** WARNING - low priority issue in verification. Not a show-stopper, but deserves attention. */
		WARNING,
		/** BLOCKED - Problem (direct or indirect) preventing further proper verification or execution. */
		BLOCKED,
		/** INCOMPLETE - Testing did not complete. */
		INCOMPLETE,
		/** NOT_RUN - Test has not run */
		NOT_RUN,
		/** SNAPSHOT - no impact. */
		SNAP;
		
		/**
		 * Attempts to match the given status String with one of the declared
		 * status types in this enumeration. If successful, method returns the
		 * Status. If not, returns <code><b>null</b></code>.
		 * @param statusStr status to match
		 * @return matching Status, or <b>null</b>
		 */
		public static Status getStatus(String statusStr){
			for(Status status : Status.values()){
				if(status.toString().equalsIgnoreCase(statusStr)){
					return status;
				}
			}
			System.out.println("Unable to match given status [" + statusStr + "]");
			return null;
		}
	}
	
	//public static Status PASS = Status.PASS;
	//public static Status FAIL = Status.FAIL;
	
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	public void report(Status status, String msg);
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param status
	 * @param msg
	 */
	public void reportVP(String expected, Status status, String msg);
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param input
	 * @param output
	 * @param expected
	 * @param status
	 * @param msg
	 */
	public void reportVP(String input, String output, String expected, Status status, String msg);
	public boolean isReportingToDB();
	public void setReportingToDB(boolean reportingToDB);

	
	

}
