package core.testlink.entity;


public class TestCase{

	private Integer id = null;
	private String fullExternalId = "";
	private String name = "";
	private Integer projectId = null;
	private Integer planId = null;
	private Integer suiteId = null;
	private String buildName = "";
	
	/**
	 * Used for instantiation of the TestCase entity. 
	 *
	 */
	public static class Builder{
		private Integer testCaseId = null;
		private String fullExternalId = "";
		private String name = "";
		private Integer projectId = null;
		private Integer testPlanId = null;
		private Integer suiteId;
		private String buildName = "";

		/**
		 * Utilize this class to apply the values for the required fields, and 'build' the entity.
		 * @param testCaseId - id of the test case
		 * @param fullExternalId - full external id of the case
		 * @param testPlanId - id of plan that the test resides in
		 * @param buildName - build name for plan
		 */
		public Builder(int testCaseId, String fullExternalId, int testPlanId, String buildName){
			this.testCaseId = testCaseId;
			this.fullExternalId = fullExternalId;
			this.testPlanId = testPlanId;
			this.buildName = buildName;
		}
		
		public Builder suiteId(Integer suiteId){
			this.suiteId = suiteId;
			return this;
		}

		/**
		 * Builds using the given parameters (via Builder)
		 * @return new Run
		 */
		public TestCase build(){
			return new TestCase(this);
		}
	}
	
	private TestCase(Builder builder){
		this.id = builder.testCaseId;
		this.setFullExternalId(builder.fullExternalId);
		this.name = builder.name;
		this.projectId = builder.projectId;
		this.planId = builder.testPlanId;
		this.suiteId = builder.suiteId;
		this.setBuildName(builder.buildName);
	}
	
	/**
	 * @return the id
	 */
	public Integer getId(){
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id){
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName(){
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * @return the projectId
	 */
	public Integer getProjectId(){
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Integer projectId){
		this.projectId = projectId;
	}
	/**
	 * @return the planId
	 */
	public Integer getPlanId(){
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(Integer planId){
		this.planId = planId;
	}
	/**
	 * @return the suiteId
	 */
	public Integer getSuiteId(){
		return suiteId;
	}
	/**
	 * @param suiteId the suiteId to set
	 */
	public void setSuiteId(Integer suiteId){
		this.suiteId = suiteId;
	}

	public String getBuildName(){
		return buildName;
	}

	public void setBuildName(String buildName){
		this.buildName = buildName;
	}

	public String getFullExternalId(){
		return fullExternalId;
	}

	public void setFullExternalId(String fullExternalId){
		this.fullExternalId = fullExternalId;
	}
	
	
	
	

}
