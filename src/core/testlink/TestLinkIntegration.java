package core.testlink;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import core.report.Reporting.Status;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;

public class TestLinkIntegration{

	
	
	private static String testlinkURL = "http://10.128.5.178/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	private static String devKey = "b6d0aea336ca22b6548c63d5c6ae5d20";
	private static String defaultProject = null;
	private static boolean overwrite = false;
	private static boolean guess = false;
	private static Map<String, String> customFields = null;

	public static void main(String[] args){
		//updateTest(12, "SA-2", 2, "JUN02", Status.PASS);
		setProject("SARegression");
		updateTest("SmokeTestAlt", "SmokeTestAlt", "ST002_TestCase", "JUN02", Status.PASS);
		//TestLinkIntegration.updateTest();
	}
	
	/**
	 * Sets the URL for connecting to the preferred TestLink instance
	 * @param url - e.g. 
	 * <code><font color='blue'>"http://</font>&ltmachineName&gt<font color='blue'>/testlink/lib/api/xmlrpc/v1/xmlrpc.php"</font></code>
	 */
	public static void setTestlinkUrl(String url){
		testlinkURL = url;
	}
	
	/**
	 * Apply the dev key for connecting to the preferred TestLink instance
	 * @param devKey 
	 */
	public static void setDevKey(String devKey){
		TestLinkIntegration.devKey = devKey;
	}
	/**
	 * Sets the overwrite mode (whether the update will overwrite an existing status).
	 */
	public static void setOverwriteMode(boolean overwrite){
		TestLinkIntegration.overwrite = overwrite;
	}
	
	/**
	 * Set the project name for this execution.
	 * @param projectName
	 */
	public static void setProject(String projectName){
		defaultProject = projectName;
	}
	
	/**
	 * Update the test case (by testName) in the given plan and suite. These can
	 * be referred to by their presentation names.
	 * @param testPlan test plan within the project
	 * @param testSuite test suite within the plan
	 * @param testName update-able test case within the suite
	 */
	public static void updateTest(String testPlan, String testSuite, String testName, String buildName, Status status){
		if(defaultProject != null){
			TestLinkAPI tlApi = new TestLinkAPI(getUrl(testlinkURL), devKey);
			TestPlan plan = tlApi.getTestPlanByName(testPlan, defaultProject);
			Integer testPlanId = plan.getId();
			Integer suiteId = null;
			Integer testCaseId = null;
			String testCaseExtId = null;
			TestSuite[] suites = tlApi.getTestSuitesForTestPlan(testPlanId);
			for(TestSuite s : suites){
				String suiteName = s.getName();
				if(suiteName.equalsIgnoreCase(testSuite)){
					suiteId = s.getId();
					break;
				}
			}
			if(suiteId != null){
				TestCase[] tcSet = tlApi.getTestCasesForTestSuite(suiteId, true, null);
				for(TestCase tc : tcSet){
					if(tc.getName().equalsIgnoreCase(testName)){
						testCaseId = tc.getId();
						testCaseExtId = tc.getFullExternalId();
						break;
					}
				}
				if(testCaseId != null){
					updateTest(testCaseId, testCaseExtId, testPlanId, buildName, status);
				}
			}else{
				System.out.println("Unable to locate TestSuite [" + testSuite + "] within " + defaultProject);
			}
			
		}else{
			System.out.println("default project for testLinkIntegration is null. " +
					"This must be set by setProject() prior to calling updates.");
		}
	}
	
	public static void updateTest(
			int testCaseId, 
			String fullExternalId, 
			int testPlanId,
			String buildName,
			Status status){
		updateTest(testCaseId, fullExternalId, testPlanId, status, buildName, null, null);
	}
	
	public static void updateTest(core.testlink.entity.TestCase testCase, Status status){
		Integer testCaseId = testCase.getId();
		String fullExternalId = testCase.getFullExternalId();
		Integer testPlanId = testCase.getPlanId();
		String buildName = testCase.getBuildName();
		updateTest(testCaseId, fullExternalId, testPlanId, status, buildName, null, null);
	}
	
	
	private static void updateTest(
			int testCaseId,
			String fullExternalId,
			int testPlanId,
			Status status,
			String buildName, 
			String notes, 
			String platformName){
		ReportTCResultResponse response = null;
		
		// convert the status
		ExecutionStatus execStatus = getExecutionStatusFrom(status);
		
		// if the status converted, proceed with updating the test...
		if(execStatus != null){
			try{
				TestLinkAPI api = new TestLinkAPI(new URL(testlinkURL), devKey);
				//TestPlan newTp = api.createTestPlan("SamplePlan", "SARegression", "Sample Plan Creation", true, true);
				response = api.reportTCResult(
						testCaseId, 
						getExternalID(fullExternalId),
						testPlanId, 
						execStatus, 
						null, 
						buildName, // required
						notes, 
						guess, 
						null, 
						null, 
						platformName, 
						customFields, 
						overwrite);
			} catch(Exception te){
				System.out.println(te.getMessage());
			} finally {
				if(response != null)
					System.out.println(response.getMessage());
				else
					System.err.println("Response from Testlink update is null.");
			}
		}

	
	}
	private static Integer getExternalID(String fullExternalId){
		if(fullExternalId != null){
			String [] extIdSplitArr = fullExternalId.split("-");
			if(extIdSplitArr.length > 1){
				String id = extIdSplitArr[1];
				if(id.matches("[0-9]{1,8}")){
					Integer retVal = Integer.parseInt(id);
					return retVal;
				}
			}
		}
		return null;
	}
	
	private static ExecutionStatus getExecutionStatusFrom(Status status){
		ExecutionStatus execStatus = null;
		switch(status){
			case PASS:
				execStatus = ExecutionStatus.PASSED;
				break;
			case FAIL:
				execStatus = ExecutionStatus.FAILED;
				break;
			case NOT_RUN:
				execStatus = ExecutionStatus.NOT_RUN;
				break;
			case BLOCKED:
				execStatus = ExecutionStatus.BLOCKED;
				break;
			default:
				System.err.println("unable to set TestLink status to " + status + ". Not a valid TestLink status.");
		}
		return execStatus;
	}
	
	private static URL getUrl(String url){
		URL returnUrl = null;
		try{
			returnUrl = new URL(url);
		}catch (MalformedURLException e){
			e.printStackTrace();
		}
		return returnUrl;
	}
	
	
	

}
