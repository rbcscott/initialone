package core.util;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.ParsingException;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Provides a means for setups or changes (e.g. the execution settings built into
 * the XML suite) to get applied prior to TestNG execution.<br><br>
 * This version provides a reusable method for pulling settings
 * from and XML and applying to a map that can be used where needed.
 */
public class CoreSuiteListener implements ISuiteListener{

	protected static final String LOAD_EXT = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
	
	@Override
	public void onStart(ISuite suite){
		

	}

	@Override
	public void onFinish(ISuite suite){


	}


	/**
	 * Returns a mapped set of elements from the given XML string.<br><br>
	 * The XML source should be from a known XML runplan entity, with 
	 * settings placed a a child of the root. The Execution Manager
	 * generates such files, TestNG compatible.
	 * @param doc
	 * @param xmlContent
	 * @return map of settings elements
	 */
	protected Map<String, String> getSettingsMap(Document doc, String xmlContent){
		Map<String, String> settingsMap = new LinkedHashMap<>();
		try{
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			xmlReader.setFeature(LOAD_EXT, false);
			Builder builder = new Builder(xmlReader);
			doc = builder.build(xmlContent, null);
			Element root = doc.getRootElement();
			Elements settings = root.getChildElements("settings");
			
			for(int i = 0; i < settings.size(); i++){
				Elements settingSet = settings.get(i).getChildElements("setting");
				for(int j = 0; j < settingSet.size(); j++){
					String name = settingSet.get(j).getAttributeValue("name");
					String value = settingSet.get(j).getAttributeValue("value");
					settingsMap.put(name, value);
					//System.out.println(name + "=" + value);
				}
			}
		}catch (ParsingException | IOException | SAXException e1){
			e1.printStackTrace();
		}
		return settingsMap;
	}


}
