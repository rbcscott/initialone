/* * Created on Mar 24, 2008 */
/** * @author scampbell  */

package core.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/**
 * This class contains many convenience methods to use for file editing. Since we are primarily concerned with
 * Text files, the methods within this class are geared toward them and because of that, we can wrap 
 * much of the overhead needed for file i/o within these and, in many cases, provide only a file location path
 * and content to write or append.
 */
public class FileEdit{

	/**
	 * Returns the entire content of the specified file as a String. This uses a BufferedReader.readLine(), 
	 * so it will be looking for a character-based stream to read content from.
	 * @param filepath location and filename (e.g.: {@code "C:\\Program Files\\myFile.txt"})
	 * @return entire content of the specified file as a String
	 * (**Note - the buffer size on this has been set to 65535 (buffer for line read) - 
	 * in the unlikely event that a single line in a file is larger than the buffer, 
	 * and I/O error (caught) may occur, resulting in return of an empty string.
	 * @author scampbell
	 */
	public static String getFileContentAsString(String filepath){
		String nextLine = "";
		final int SIZE = 65535;  // default buffer size is 4096
		String content = "";
		File file = new File(filepath);
		if(file.isDirectory()){
			System.err.println("FileEdit.getFileContentAsString(): File " + filepath + " is a directory");
		}else if(file.exists() && file.canRead()) {
			try {
				// create new filestream for reading
				FileReader fileinput = new FileReader(file);
				// create buffer to store (in this case) lines (see *note below)
				BufferedReader bufferedReader = new BufferedReader(fileinput, SIZE);
				// remove
				//char[] cbuf = new char[10000];
				//bufferedReader.read(cbuf, 0, 9999);
				
				// remove
				while( (nextLine = bufferedReader.readLine()) != null) {
					content += nextLine + "\r\n";
				}
				//close the stream
				bufferedReader.close();
				fileinput.close();
			}catch(IOException e) {
				System.out.println("Exception in file: " + e);
			}
		}else {
			System.err.println("File " + filepath + " does not exist or cannot be read");
		}
		return content;
	}

	/**
	 * Returns the entire content of the specified file as a String. This uses a BufferedReader.readLine(), 
	 * so it will be looking for a character-based stream to read content from.
	 * @param filepath location and filename (e.g.: {@code "C:\\Program Files\\myFile.txt"})
	 * @return entire content of the specified file as a String
	 * (**Note - the buffer size on this has been set to 65535 (buffer for line read) - 
	 * in the unlikely event that a single line in a file is larger than the buffer, 
	 * and I/O error (caught) may occur, resulting in return of an empty string.
	 * @author scampbell
	 */
	public static void transfer(String filepath, String newFilePath){
		String nextLine = "";
		final int SIZE = 65535;  // default buffer size is 4096
		//String content = "";
		File file = new File(filepath);
		File newFile = new File(newFilePath);
		if(file.isDirectory()){
			System.err.println("FileEdit.getFileContentAsString(): File " + filepath + " is a directory");
		}else if(file.exists() && file.canRead()) {
			try {
				// create new filestream for reading
				FileReader fileinput = new FileReader(file);
				// create buffer to store (in this case) lines (see *note below)
				BufferedReader bufferedReader = new BufferedReader(fileinput, SIZE);				
				// create new filestream for writing
				FileWriter fileWriter = new FileWriter(newFile);
				// create buffer to write content
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, SIZE);
				
				while( (nextLine = bufferedReader.readLine()) != null) {
					bufferedWriter.append(nextLine + "\r\n");
					bufferedWriter.flush();
					//content += nextLine + "\r\n";
				}
				//close the stream
				bufferedReader.close();
				bufferedWriter.close();
				fileinput.close();
				fileWriter.close();
			}catch(IOException e) {
				System.out.println("Exception in file: " + e);
			}
		}else {
			System.err.println("File " + filepath + " does not exist or cannot be read");
		}
		//return content;
	}
	
	/**
	 * Writes string content to given file location. If file does not exist at the location, it will create
	 * the file. If file already exists, <b>content is replaced with given content</b>. If you need to add to a 
	 * file's content, see {@link #appendToFile(String, String)}.
	 * @param filePath location and filename (e.g.: {@code "C:\\Program Files\\myFile.txt"})
	 * @param content given String content 
	 * @see #appendToFile(String, String)
	 * @author scampbell
	 */
	public static void writeToFile(String filePath, String content){
		final int SIZE = 65535;  // default buffer size is 4096
		File file = new File(filePath);
		if(file.isDirectory()){
			System.err.println("FileEdit.writeToFile(): Given file [" + filePath + "] is a directory");
		}else{
			try {
				if(!file.exists()){
					file.createNewFile();
				}
				if(file.exists()){
					if(file.canWrite()){
						// create new filestream for writing
						FileWriter fileWriter = new FileWriter(file);
						// create buffer to write content
						BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, SIZE);
						if(content.length() > SIZE){
							Vector<String> stringBlocks = new Vector<String>();
							int start = 0;
							int end = start + SIZE;
							while(end <= content.length()){
								stringBlocks.add(content.substring(start, end));
								start = end;
								String checkString = content.substring(start);
								if(checkString.length() >= SIZE){
									end += SIZE;
								}else if(checkString.length() > 0){
									end = start + (checkString.length());
								}else{
									end = start + 1;
								}
							}
							for(String block : stringBlocks)
								bufferedWriter.append(block);
						}else{
							bufferedWriter.append(content);
						}
						bufferedWriter.close();
						fileWriter.close();
					}else{
						System.out.println("FileEdit.writeToFile(): could not write to file [" + file.getAbsolutePath() + "]. Check read-only.");
					}
				}else{
					System.out.println("FileEdit.writeToFile(): could not create file [" + file.getAbsolutePath() + "].");
				}
			}catch(IOException e) {
				System.out.println("IOException in FileEdit.writeToFile(): " + e);
			}
		}
	}

	/**
	 * Writes string content to given file location, appending to the end. 
	 * If file does not exist at the location, it will create the file and append content to the empty
	 * file. If file already exists, <b>content is appended</b>. If you simply need to create and write 
	 * to a new file, see {@link #writeToFile(String, String)}.
	 * @param filePath location and filename (e.g.: {@code "C:\\Program Files\\myFile.txt"})
	 * @param content given String content 
	 * @see #writeToFile(String, String)
	 * @author scampbell
	 */
	public static void appendToFile(String filePath, String stringToAdd){
		final int SIZE = 65535;  // default buffer size is 4096
		File file = new File(filePath);
		try{
			if(!file.exists()){
				file.createNewFile();
			}
			if(file.isDirectory()){
				System.err.println("FileEdit.appendToFile(): Given file [" + filePath + "] is a directory");
			}else if(file.exists() && file.canWrite()) {
				// create new filestream for writing
				FileWriter fileWriter = new FileWriter(file, true); //the true indicates append mode
				// create buffer to write content
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, SIZE);

				if(stringToAdd.length() > SIZE){
					Vector<String> stringBlocks = new Vector<String>();
					int start = 0;
					int end = start + SIZE;
					while(end <= stringToAdd.length()){
						stringBlocks.add(stringToAdd.substring(start, end));
						start = end;
						String checkString = stringToAdd.substring(start);
						if(checkString.length() >= SIZE){
							end += SIZE;
						}else if(checkString.length() > 0){
							end = start + (checkString.length());
						}else{
							end = start + 1;
						}
					}
					for(String block : stringBlocks)
						bufferedWriter.append(block);
				}else{
					bufferedWriter.append(stringToAdd);
				}
				bufferedWriter.close();
				fileWriter.close();

			}else {
				System.err.println("File " + filePath + " does not exist or cannot be written to.");
			}
		}catch(IOException e) {
			System.out.println("Exception in FileEdit.appendToFile(String, String): " + e);
		}

	}
	/**
	 * Gets file content, replaces as indicated and returns updated content. Does not modify file at path
	 * in any way (no stream writes are performed).
	 * @param filepath - full path of file (e.g.: "c:\\Documents and Settings\\textFile.txt"
	 * @param strToReplace - String that will get replaced by...
	 * @param replacement - String that will exist in file in place of above
	 * @return - file content with replacepments made
	 */
	public static String replaceText(String filepath, String strToReplace, String replacement){
		String nextLine = "";
		//int nextChar;
		//char[] charArray = new char[4096];
		final int SIZE = 65535;  // default buffer size is 4096
		String content = "";
		File file = new File(filepath);
		if(file.isDirectory()){
			System.err.println("FileEdit.replaceText(): File " + filepath + " is a directory");
		}else if(file.exists() && file.canRead()) {
			try {
				// create new filestream for reading
				FileReader fileinput = new FileReader(file);
				// create buffer to store (in this case) lines (see *note below)
				BufferedReader bufferedReader = new BufferedReader(fileinput, SIZE);
				while( (nextLine = bufferedReader.readLine()) != null) {
					content += nextLine + "\r\n";
				}
				// *note - the commented method below reads in a character at a time, and shows a 
				// \r\n after each line of xml data. When pasted into a form
				// this yielded an extra line between each meaningful line. Not really a problem, and when
				// done via inputKeys(String), it looked correct,
				// but this method may be needed if unexpected results are seen in the subsequent use
				// of the string (content).

				// read each char and add to the string variable
				//				while( (nextChar = fileinput.read()) != -1) {
				//					content += (char)nextChar;
				//				}

				//close the stream
				bufferedReader.close();
				fileinput.close();
			}catch(IOException e) {
				System.out.println("Exception in file: " + e);
			}
			// using replace all, replace every instance of the word
			if(!strToReplace.equals("") && replacement != null){
				content = content.replaceAll(strToReplace, replacement);
			}
		}else {
			System.err.println("File " + filepath + " does not exist or cannot be read");
		}
		return content;
	}
	/**
	 * Gets file content, replaces as indicated and returns updated content. The arrays should be 
	 * coordinated such that strToReplace[0] will be replaced by replacement[0], etc. <br>
	 * <em>(Does not modify file at path in any way (no stream writes are performed)).</em>
	 * @param filepath - full path of file (e.g.: "c:\\Documents and Settings\\textFile.txt"
	 * @param strToReplace[] - array of Strings that will get replaced by...
	 * @param replacement[] - array of Strings that will exist in file in place of above
	 * @return - file content with replacepments made
	 * @see <code>replaceText(String, String, String)}<code>
	 */
	public static String replaceText(String filepath, String[] strToReplace, String[] replacement){
		String nextLine = "";
		//int nextChar;
		//char[] charArray = new char[4096];
		String content = "";
		File file = new File(filepath);
		if(file.isDirectory()){
			System.err.println("FileEdit.replaceText(): File " + filepath + " is a directory");
		}else if(file.exists() && file.canRead()) {
			try {
				// create new filestream for reading
				FileReader fileinput = new FileReader(file);
				// create buffer to store (in this case) lines (see *note below)
				BufferedReader bufferedReader = new BufferedReader(fileinput);
				while( (nextLine = bufferedReader.readLine()) != null) {
					content += nextLine + "\n";
				}
				// *note - the commented method below reads in a character at a time, and shows a 
				// /r/n after each line of xml data. When pasted into a form
				// this yielded an extra line between each meaningful line. Not really a problem, and when
				// done via inputKeys(String), it looked correct,
				// but this method may be needed if unexpected results are seen in the subsequent use
				// of the string (content).

				// 				read each char and add to the string variable
				//				while( (nextChar = fileinput.read()) != -1) {
				//					content += (char)nextChar;
				//				}

				//close the stream
				bufferedReader.close();
				fileinput.close();
			}catch(IOException e) {
				System.out.println("Exception in file: " + e);
			}
			for(int i = 0; i < strToReplace.length; i++){
				// Using replaceAll, replace every instance of the token
				// first make sure we don't try to use a null element AND make sure we don't try to
				// replace an empty string
				if(strToReplace[i] != null && !strToReplace[i].equals("")){
					if(replacement[i] != null)  // check this to - if there is nothing to replace with, don't use it
						content = content.replaceAll(strToReplace[i], replacement[i]);
				}
			}

		}else {
			System.err.println("File " + filepath + " does not exist or cannot be read");
		}
		return content;
	}

}

