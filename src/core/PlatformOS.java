package core;

/**
 * Handler for the host operating system information and values.<br><br>
 * Provides indicators for the operating system residing on the executing machine.
 */
public class PlatformOS{

	private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
	/**
	 * Representation of the Operating System value. <br>
	 * See {@link PlatformOS#getOS()}.
	 */
	public enum OS {
		WINDOWS, 
		MAC, 
		LINUX,
		UNIX,
		UNSUPPORTED;
	}

	private static OS os = null;

	/**
	 * Matches and returns the OS value for the os.name property of this system.
	 * @return
	 */
	public static OS getOS() {
		if(os == null) {
			if(OS_NAME.indexOf("win") > -1){ 
				os = OS.WINDOWS;
			}else if(OS_NAME.indexOf("mac") > -1){
				os = OS.MAC;
			}else if(OS_NAME.indexOf("nux") > -1){
				os = OS.LINUX;
			}else if(OS_NAME.indexOf("nix") > -1){
				os = OS.UNIX;
			}else{
				os = OS.UNSUPPORTED;
			}
		}
		return os;
	}

	/**
	 * If the underlying system is a windows OS, returns <code><b>true</b></code>.
	 * @return If the underlying system is a windows OS, returns <code><b>true</b></code>
	 * otherwise, <b><code>false</code></b>
	 */
	public static boolean isWindows(){
		return getOS() == OS.WINDOWS;
	}
	/**
	 * If the underlying system is a Mac OS, returns <code><b>true</b></code>.
	 * @return If the underlying system is a Mac OS, returns <code><b>true</b></code>
	 * otherwise, <b><code>false</code></b>
	 */
	public static boolean isMac(){
		return getOS() == OS.MAC;
	}
	/**
	 * If the underlying system is a Linux OS, returns <code><b>true</b></code>.
	 * @return If the underlying system is a Linux OS, returns <code><b>true</b></code>
	 * otherwise, <b><code>false</code></b>
	 */
	public static boolean isLinux(){
		return getOS() == OS.LINUX;
	}
	/**
	 * If the underlying system is a Unix OS, returns <code><b>true</b></code>.
	 * @return If the underlying system is a Unix OS, returns <code><b>true</b></code>
	 * otherwise, <b><code>false</code></b>
	 */
	public static boolean isUnix(){
		return getOS() == OS.UNIX;
	}

}
