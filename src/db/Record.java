package db;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import core.test.APITestScript;
import db.tables.Table;
import db.tables.TableColumn;

/**
 * Holds an individual "record" for the RecordSet class. Also maintains a reference to the 
 * header (columns) from the RecordSet object that creates this.
 * @param <T>
 */
public class Record<T> extends ArrayList<String> implements Serializable{

	private static final long serialVersionUID = 1L;
	private ArrayList<String> header;
	private Object object = null;
	private RecordSet data = null;
	private String dbConnection = null;
	private APITestScript apiTestScript = null;
	public boolean memberHeader = false;
	
	/**
	 * Creates a new, empty, initialized record. This will be unattached from
	 * any RecordSet object.
	 */
	public Record(){
		header = new ArrayList<String>();
		memberHeader = true;
	}
	
	Record(ArrayList<String> header){
		this.header = header;
		this.header.trimToSize();
		//this.addWithAutoBuffer(header.size(), "");
	}
	
	/**
	 * Searches in record for the element at given column name. 
	 * @param	columnName   the Column name of the table
	 * @return	value from field specified by column name	
	 */
	public String getValueAt(String columnName){
		return get(columnName);
	}
	
	/**
	 * Checks the value at the given column and if empty, returns true
	 * @param columnName
	 * @return <b>true</b> if empty (i.e., check yields empty String or column was not found)
	 */
	public boolean isEmpty(String columnName){
		String val = get(columnName);
		if(val == null || val.equals("")){
			return true;
		}
		return false;
	}
	
	/**
	 * Automatically backfills prior elements if the index is greater than size,
	 * so that column elements may be added when prior column fields are not yet 
	 * populated.<br><br>
	 * 0-based
	 * @param index
	 * @param element
	 */
	public void addWithAutoBuffer(int index, String element){
		int size = this.size();
		if(index > size){
			for(int i = size; i < index; i++){
				this.add("");
			}
			this.add(element);
		}else if(index == size){
			this.add(element);
		}else{
			this.set(index, element);
		}
		if(this.size() < this.header.size()){
			for(int i = this.size(); i < this.header.size(); i++){
				this.add("");
			}
		}
	}
	public void addElement(String columnName, String value){
		int columnIndex = header.indexOf(columnName);
		if(columnIndex < 0){
			header.add(columnName);
			columnIndex = header.indexOf(columnName);
			this.add(value);
		}else{
			if(this.size() <= columnIndex)
				this.add(value);
			else
				this.set(columnIndex, value);
		}
		
	}
	
	//public void insertHeader
	
	/**
	 * Searches in record for the element at given column name. 
	 * @param	columnName   the Column name of the table
	 * @return	value from field specified by column name	
	 */
	public String get(String columnName){
		int columnIndex = header.indexOf(columnName);
		String value = "";
		if(columnIndex > -1){
			if(this.size() > 0){
				if(!(columnIndex >= this.size())){
					value = this.get(columnIndex);
				}
				if(value != null){
					return value;
				}
			}else{
				System.out.println(this.getClass() + ".get(" + columnName + "): row data empty");
			}
		}else{
			System.out.println("Record.get(columnName): Column not found [given name = " + columnName + "]");
		}
		return value;
	}
	
	/**
	 * Searches in record for the element at given column. 
	 * @param	column   the Column of the table
	 * @return	value from field specified by column name	
	 */
	public String get(TableColumn column){
		String columnName = column.getName();
		return get(columnName);
	}
	/**
	 * Searches in record for the given column name, and if found, sets the element
	 * with the value given.
	 * @param columnName   the Column name of the table
	 * @param value String value to set
	 * @return	value from field specified by column name	
	 */
	public String set(String columnName, String value){
		int columnIndex = header.indexOf(columnName);
		if(columnIndex > -1){
			if(this.size() > 0){
				this.set(columnIndex, value);
			}else{
				System.out.println(this.getClass() + ".set(" + columnName + "): row data empty");
			}
		}else{
			System.out.println(this.getClass() + ".set: Column not found [given name = " + columnName + "]");
		}
		return value;
	}
	
	/**
	 * Searches in record for the given column name, and if found, sets the element
	 * with the value given.
	 * @param column  the Column of the table
	 * @param value String value to set
	 * @return	value from field specified by column name	
	 */
	public String set(TableColumn column, String value){
		String columnName = column.getName();
		return set(columnName, value);
	}
	/**
	 * Checks in this Record for the given column name to exist.
	 * @param column
	 * @return true if the column name (header) exists
	 */
	public boolean containsColumn(String column){
		boolean exists = false;
		if(this.header.contains(column))
			exists = true;
		return exists;
	}
	/**
	 * Returns the collection of column names related to this record entry as an ArrayList.
	 * @return the collection of column names as an ArrayList
	 */
	public ArrayList<String> getColumns(){
		ArrayList<String> columns = new ArrayList<String>();
		for(String s : header){
			columns.add(new String(s));
		}
		return columns;
	}
	
	/**
	 * Returns the Record elements contained in this vector in a readable
	 * table format (headers along the top and row lined out below).<br>
	 * E.g.:<br>
	 * <pre>
	 * COL1      COL2         COL3       COL4
	 * --------- ------------ ---------- ---------
	 * ROW1DATA1 ROW1D2       ROW1DATA3  ROW1D4
	 * </pre>
	 */
	@Override
	public String toString(){
		// SC-R1002 - updating the output to a more viewable one
		// return records.toString(); // old
		StringBuilder output = new StringBuilder();
		//final int TAB_SPACING = 4;
		int columnSize[] = new int[header.size()];
		// set initial column space
		for(int i = 0; i < header.size(); i++){
			columnSize[i] = header.get(i).length() + 2;
		}
		// check row data and resize column space if necessary
		
			int recordLength = 0;
			String data = "";
			for(int i = 0; i < this.size(); i++){
				data = this.get(i);
				if(data != null)
					recordLength = data.length();
				else
					recordLength = 4;
				if(recordLength > columnSize[i] - 2){
					columnSize[i] = recordLength + 2;
				}
			}
		
		// output the data
		int spaceCount = 0;
		char space = ' ';
		// loop thru header and append
		for(int i = 0; i < header.size(); i++){
			output.append(header.get(i));
			spaceCount = columnSize[i] - header.get(i).length();
			if(spaceCount > 0){
				for(int x = 0; x < spaceCount; x++)
					output.append(space);
			}
		}
		// add the return between header and data
		output.append("\r\n");
		for(int i = 0; i < header.size(); i++){
			for(int j = 0; j < columnSize[i]-2; j++)
				output.append("-");
			output.append("  ");
		}
		output.append("\r\n");
		// loop thru data and append
	
			for(int i = 0; i < this.size(); i++){
				output.append(this.get(i));
				spaceCount = columnSize[i] - (this.get(i) != null ? this.get(i).length() : 4);
				if(spaceCount > 0){
					for(int x = 0; x < spaceCount; x++)
						output.append(space);
				}
			}
			output.append("\r\n");
		
		return output.toString();
	}
	
	/**
	 * Get the data instance that may be attached to this Record.<br>
	 * (may return <b>null</b>).
	 * @return
	 */
	public RecordSet getData(){
		return this.data;
	}
	
	/**
	 * Set an arbitrary RecordSet, attached to this Record instance.
	 * @param data 
	 */
	public void setData(RecordSet data) {
		this.data = data;
	}
	/**
	 * Gets the object instance that may be attached to this Record.<br>
	 * (may return <b>null</b>).
	 * @return
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * Set an arbitrary object, attached to this Record instance.
	 * @param object
	 */
	public void setObject(Object object) {
		this.object = object;
	}
	
	/**
	 * Write records to the table. <br>
	 * Currently, no checks are done for column validity. Client
	 * should ensure columns match and data types are compatible. <br><br>
	 * Currently <b>Oracle only</b>:<br>
	 * <ul>
	 * <li>SQL is oracle defined
	 * <li>All data values are surrounded by single quotes.
	 * </ul>
	 * @param table
	 */
	public void writeToDatabase(Table table){
		String _table = table.getTableName();
		if(dbConnection  != null){
			// insert into db
			StringBuilder sqlBldr = new StringBuilder();
			StringBuilder columnSet = new StringBuilder();
			StringBuilder valueSet = new StringBuilder();

			sqlBldr.append("INSERT ALL \n");
			String data = "";
			boolean first = true;
			for(String column : this.getColumns()){
				if(!first){
					columnSet.append(",");
					valueSet.append(",");
				}
				columnSet.append(column);
				data = setDataForOracleWrite(this.get(column).trim());
				TableColumn tableField = table.getField(column);
				String type = tableField.getType();
				if(type != null){
					switch(type){
						case "NVARCHAR2":
							valueSet.append("N'" + data + "'");
							break;
						case "DATE":
							String format = getOracleDateFormat(data);
							valueSet.append("TO_DATE('" + data + "', '" + format + "')");
							break;
						default:
							valueSet.append("'" + data + "'");
					}
				}else{
					valueSet.append("'" + data + "'");
				}
				first = false;
			}
			sqlBldr.append("INTO " + _table + " (" + columnSet + ")" + " VALUES (" + valueSet + ") \n");
			columnSet.setLength(0);
			valueSet.setLength(0);
			sqlBldr.append("SELECT * FROM DUAL\n");

			JdbcOracle oracleDb = new JdbcOracle();
			oracleDb.executeSql(dbConnection, sqlBldr.toString());
		}else{
			System.out.println("Record.writeToDatabase(): Unable to execute - connection is not set.");
		}
	}
	
	/**
	 * In place to identify the AM/PM in the date string and apply
	 * to the oracle expected format. The passed String is expected to 
	 * be of the format "YYYY-MM-DD HH:MI:SS AM/PM" already.<br>
	 * The TO_DATE implementation for Oracle
	 * expects a matching AM/PM for 12-hour date strings. 
	 * @param origDate
	 * @return
	 */
	private String getOracleDateFormat(String origDate){
		StringBuilder format = new StringBuilder();
		format.append("YYYY-MM-DD HH:MI:SS");
		if(origDate.endsWith("AM")){
			format.append(" AM");
		}else if(origDate.endsWith("PM")){
			format.append(" PM");
		}
		return format.toString();
	}

	/**
	 * @return the dbConnection
	 */
	public String getDbConnection(){
		return dbConnection;
	}

	/**
	 * @param dbConnection the dbConnection to set
	 */
	public void setDbConnection(String dbConnection){
		this.dbConnection = dbConnection;
	}
	
	private String setDataForOracleWrite(String data){
		return data.replaceAll("'", "''");
	}

	/**
	 * Removes the data at columnName.
	 * @param columnName
	 */
	public void removeData(String columnName){
		if(this.header.contains(columnName)){
			int index = this.header.indexOf(columnName);
			this.removeData(index);
		}
		
	}
	
	public void removeData(int index){
		if(index < this.size()){
			this.remove(index);
		}
	}
	/**
	 * Removes the header matching given columnName.
	 * @param columnName
	 */
	public void removeHeader(String columnName){
		if(this.header.contains(columnName)){
			this.header.remove(columnName);
		}
	}
	
	/**
	 * @return the APITestScript
	 */
	public APITestScript getAPITestScript(){
		return apiTestScript;
	}

	/**
	 * @param apiTestScript the APITestScript to set
	 */
	public void setAPITestScript(APITestScript apiTestScript){
		this.apiTestScript = apiTestScript;
	}
}


