package customproject.settings;

import db.AutoDB;
import db.JdbcOracle;
import db.RecordSet;

public class SettingsSql{

	/**
	 * Sample method to pull records (registration, test case parameter,
	 * etc.) from the test case records.<br><br>
	 * <b><i>[template - non-functional]</i></b>
	 * @param value to query with
	 * @return
	 */
	public static RecordSet getSampleRecords(String value){
		RecordSet rs = new RecordSet();
		String sql = 
				"SELECT * FROM <TABLE> " +
				"WHERE <COLUMN> = '" + value + "'";
		JdbcOracle jdbc = new JdbcOracle();
		System.out.println("Executing SQL [" + sql + "]");
		rs = jdbc.executeSql(AutoDB.parameters(), sql);
		return rs;
	}
	
}
