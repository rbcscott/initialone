package customproject.settings.runtime;

import customproject.settings.ProjectSettings;
import customproject.settings.ProjectSettings.Field;

public enum RuntimeArgs{

	ENVIRONMENT("appEnv", Field.ENVIRONMENT);

	private String argName = null;
	private Field fieldOverride = null;
	
	private RuntimeArgs(String argName){
		this.argName = argName;
	}

	private RuntimeArgs(String argName, ProjectSettings.Field fieldOverride){
		this.argName = argName;
		this.fieldOverride = fieldOverride;
	}
	
	public String getArgName(){
		return this.argName;
	}

	public Field getFieldOverride(){
		return fieldOverride;
	}
}

