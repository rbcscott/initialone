package customproject.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Map;

import static customproject.settings.ProjectSettings.Field.*;

import org.testng.Assert;

import core.settings.BaseSettings;
import customproject.app.db.*;
import customproject.app.db.AppDB.Schema;
import customproject.settings.runtime.RuntimeArgs;
import db.AutoDB;
//import db.RecordSet;

/**
 * An instance of this class will provide access to the fields/properties
 * of the test run. Properties are loaded via the Settings class, which
 * is set to load the properties file with accessibility to the properties
 * and fields necessary for execution against Project Application environments.<br><br>
 * Additionally, if external argument values are loaded, they are handled 
 * here as overrides. The order of override is thus:<br>
 * <ul>
 * <li>GlobalSettings are loaded</li>
 * <li>If the run is executed by unit test, nothing more is done
 * <li>else...
 * <ul>
 * <li>Runtime arguments are read
 * <li>Any matching overrides are applied
 * </ul>
 * </ul>
 * @return settings for ProjectApplication
 *
 */
public class ProjectSettings extends BaseSettings{
	// calculated fields
	private int implicitTimeOut = 0;
	private int dbTimeOut = 0;
	//private Browser browser;
	private boolean sauceFlag = false; 
	private String sauceAcct = null;
	private String sauceAcctKey = null; 
	private String sauceAcctUrl = null; 
	//private ExecutionMode executionMode = null;  // calc
	//private String runPlanName = "";
	//private String runID = null;
	//private Platform platform = null;
	//private String baseUrl = null;
	//private String currentDateTime = "--current time--";
	List<String> countryList = new ArrayList<String>();
	
	// local instance of the underlying properties set
	//private Properties props = null;
	// Default Browser
	//private static final Browser DEFAULT_BROWSER = Browser.FIREFOX;
	// local instance of this class, for singleton
	private static ProjectSettings projectSettings = null;
	// run name to be assigned
	private static String defaultRunName = null;
	
	/**
	 * Returns the instance of ProjectSettings (the settings will be established, if not already),
	 * with accessibility to the properties and fields necessary for execution against 
	 * environments.
	 * @return settings for execution
	 */
	public static ProjectSettings getSettings(){
		if(projectSettings == null){
			projectSettings = new ProjectSettings();
		}
		return projectSettings;
	}
	
	private ProjectSettings(){
		super();
		applyOverrides();
		setCalculatedFields();
	}
	
	/**
	 * Representation of all the application-specific and relevant fields from the property file.<br><br>
	 * Any key-value pair that is added (or removed) from the representative property file
	 * within this project should be handle likewise in this enumerated set. 
	 * Optionally, a getter method should be applied to provide access to the value.<br><br> 
	 * If the field is used in a calculated manner (e.g. a value dependent on another value 
	 * or environment variable), that should be added to the 
	 * {@link ProjectSettings#setCalculatedFields()} method for calculation.
	 */
	public enum Field{
		IMPLICIT_TIMEOUT("ImplicitTimeout"),
		SAUCE_FLAG("SauceLabsExecution"),
		SAUCE_ACCT("SauceLabsAccount"),
		//SAUCE_ACCT_KEY("SauceAcctKey"),
		TEST_DATA_SOURCE("DataSource"),
		EXTERNAL_SHEET_PATH("external_sheet_path"),
		IEDRIVER_32("IEDriver32"),
		IEDRIVER_64("IEDriver64"),
		CHROMEDRIVER("ChromeDriver"),
		FIREFOX_PROFILE("FirefoxProfile"),
		DB_INSTANCE("DatabaseInstance"),
		DB_TIME_OUT("time_out"),
		SCHEMA_SET("SchemaSet"),
		SCHEMA_PW("SchemaPw"),
		APP_DB_INSTANCE("DBInstance"),
		APP_DB_SCHEMA("DBSchema"),
		APP_DB_SCHEMA_PW("DBSchemaPW"),
		COSMOS_APPDB_INSTANCE("CosmosDBInstance"),
		COSMOS_APPDB_SCHEMA("CosmosDBUser"),
		COSMOS_APPDB_SCHEMA_PW("CosmosDBPassword"),
		USE_GLOBAL_BROWSER_FOREXCEL("use_global_browser_forExcel"),
		TESTLINK_DEV_KEY("TestLink_DEV_KEY"),
		TESTLINK_SERVER_URL("TestLink_SERVER_URL"),
		TESTLINK_PROJECT_NAME("TestLink_PROJECT_NAME"),
		TESTLINK_PLAN_NAME("TestLink_PLAN_NAME"),
		TESTLINK_BUILD_NAME("TestLink_BUILD_NAME"),
		TESTLINK_TESTCASEID_PREFIX("TestLink_TestcaseID_Prefix"),
		APPLICATIONENVIRONMENT("Environment"),
		APPLICATION_TEST_MODULES("app.application.test.modules"),
		APPLICATION_TEST_GROUPS("app.application.test.groups"),
		TEST_REPORTS_DIRECTORY("TestReportsDirectory"),
		ENABLE_LOGS_IN_REPORT("app.enable.logs.in.report"),
		GRID_HOSTNAME("Hostname"),
		GRID_HOST_PORT("Port"),
		GRID_TIMEOUT("Timeout"),
		ENVIRONMENT("Environment"),
		DEFAULT_URL("ApplicationUrl"),
		APPLICATION_URL("ApplicationUrl"),
		APPLICATION_LOGIN_URL("ApplicationLoginUrl"),
		MAC_PLATFORM("MacPlatform");

		private String key = "";
		private String value = "";

		private Field(String key){
			this.key = key;
		}
		
		void setValue(String value){
			this.value = value;
		}
		
		void setValue(Properties p){
			this.value = p.getProperty(key, "");
		}
		
		public String getKey(){
			return this.key;
		}

	}

	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br><br>The value will be returned 'as-is', meaning that any
	 * derived or calculated fields will not necessarily be true or as intended (these fields
	 * should have access available from this API, allowing the value to be retrieved
	 * post-calculation).
	 * @param key {@link Field} value
	 * @return value associated with the given key (<b>null</b> if key is not found)
	 */
	public String getProperty(Field key){
		if(key != null){
			return props.getProperty(key.key);
		}
		return null;
	}
	
	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br><br>The value will be returned 'as-is', meaning that any
	 * derived or calculated fields will not necessarily be true or as intended (these fields
	 * should have access available from this API, allowing the value to be retrieved
	 * post-calculation).
	 * @param key String value of the property field to get
	 * @return value associated with the given key (<b>null</b> if not found)
	 */
	public String getProperty(String key){
		return props.getProperty(key);
	}
	
	/**
	 * Allows the retrieval of any String data from the underlying properties
	 * instance. <br><br>
	 * This differs from {@link #getProperty(String)} in that it takes a default value
	 * that will be used in the event that the provided key is not found and the property
	 * returns null. Should a null value be provided for default, this method will return an 
	 * empty String.<br><br>  
	 * The value will be returned 'as-is', meaning that any
	 * derived or calculated fields will not necessarily be true or as intended (these fields
	 * should have access available from this API, allowing the value to be retrieved
	 * post-calculation).
	 * @param key String value of the property field to get
	 * @return value associated with the given key (<b>null</b> if not found)
	 */
	public String getProperty(String key, String defaultValue){
		String val = props.getProperty(key);
		if(val != null)
			return props.getProperty(key);
		else if(defaultValue != null)
			return defaultValue;
		else
			return "";
	}

	/**
	 * Set the calculated fields based on the assigned property values.<br><br>
	 * This should be called after the property file has been read into the
	 * instance of properties. (via {@link core.settings.Settings#getInstance()})
	 * <br><br>
	 * This method may be called twice, should there be an execution from the 
	 * runplan file or the directly from the execution manager. The local settings
	 * file is loaded first as default, followed by any runtime overrides as part 
	 * of the aforementioned execution types.<br>
	 * <br>
	 * Calculated fields handled as part of this method:
	 * <ul>
	 * <li>Browser
	 * <li>Platform
	 * <li>executionMode
	 * <li>sauceFlag
	 * <li>sauceAcctKey
	 * <li>sauceAcctUrl
	 * </ul>
	 */
	protected void setCalculatedFields(){
		
		super.setCalculatedFields();
		
		for(Field f : Field.values()){
			String value = props.getProperty(f.key, "");
			f.setValue(value);
		}
		
		sauceFlag = Boolean.parseBoolean(props.getProperty(SAUCE_FLAG.key, "false"));
		sauceAcct = props.getProperty(SAUCE_ACCT.key, "");
		//  http://regression:cda3bf01-5fca-4887-9944-2be8da2dad67@ondemand.saucelabs.com:80/wd/hub
		sauceAcctKey = props.getProperty(sauceAcct + "Acct", null);
		sauceAcctUrl = buildSauceAcctUrl(sauceAcct, sauceAcctKey);
		
		
		String implicitTimeOutStr = props.getProperty(IMPLICIT_TIMEOUT.key);
		if(implicitTimeOutStr != null && implicitTimeOutStr.matches("[0-9]{1,2}")){
			implicitTimeOut = Integer.parseInt(props.getProperty(IMPLICIT_TIMEOUT.key));
		}
		
		String timeOutStr = props.getProperty(DB_TIME_OUT.key);
		if(timeOutStr != null && timeOutStr.matches("[0-9]{1,6}")){
			dbTimeOut = Integer.parseInt(props.getProperty(DB_TIME_OUT.key));
		}
		
		if(TEST_DATA_SOURCE.value.equalsIgnoreCase("excel")){
			EXTERNAL_SHEET_PATH.value = props.getProperty(EXTERNAL_SHEET_PATH.key);
			if(EXTERNAL_SHEET_PATH.value.equals("")){
				Assert.fail();
			}
		}else if(TEST_DATA_SOURCE.value.equalsIgnoreCase("database")){
			// set Oracle DB
			AutoDB.setInstance(props, DB_INSTANCE.key);
			AutoDB.setAllSchemas(SCHEMA_SET.value, SCHEMA_PW.value);
		}else{
			Assert.fail();
		}
		
		// set application DB ...
		AppDB.setInstance(props, APP_DB_INSTANCE.key);
		Schema schema = Schema.getSchema(props.getProperty(APP_DB_SCHEMA.key));
		String schemaPW = props.getProperty(APP_DB_SCHEMA_PW.key);
		AppDB.setSchema(schema, schemaPW);
		
	}
	
	/**
	 * Builds the URL by account and key, creating the full String
	 * with Sauce domain, port and directory
	 * @param accountName name of requested account (see Sauce admin if unknown)
	 * @param accountKey key for account 
	 * (see your local administrator, or obtain from account page: https://saucelabs.com/login)
	 * @return built URL ready for WebDriver
	 */
	private String buildSauceAcctUrl(String accountName, String accountKey){
		StringBuilder bldr = new StringBuilder();
		bldr.append("http://");
		bldr.append(accountName);
		bldr.append(":"); 
		bldr.append(accountKey);
		bldr.append("@ondemand.saucelabs.com:80/wd/hub"); 
		return bldr.toString();
	}


	public String getExternalSheetPath(){
		return EXTERNAL_SHEET_PATH.value;
	}

	public String getTestLinkDevKey(){
		return TESTLINK_DEV_KEY.value;
	}

	public String getTestLinkServerUrl(){
		return TESTLINK_SERVER_URL.value;
	}

	public String getTestLinkProjectName(){
		return TESTLINK_PROJECT_NAME.value;
	}

	public String getTestLinkPlanName(){
		return TESTLINK_PLAN_NAME.value;
	}

	public String getTestLinkBuildName(){
		return TESTLINK_BUILD_NAME.value;
	}

	public String getTestLinkTestCaseIDPrefix(){
		return TESTLINK_TESTCASEID_PREFIX.value;
	}

	public String getApplicationEnableLogInReport(){
		return ENABLE_LOGS_IN_REPORT.value;
	}

	public String getApplicationDirectory(){
		return TEST_REPORTS_DIRECTORY.value;
	}

	public String getApplicationTestGroup(){
		return APPLICATION_TEST_GROUPS.value;
	}

	public String getApplicationTestModule(){
		return APPLICATION_TEST_MODULES.value;
	}

	public String getApplicationEnvironment(){
		return APPLICATIONENVIRONMENT.value;
	}
	public String getApplicationUrl(){
		return APPLICATION_URL.value;
	}
	public String getApplicationLoginUrl(){
		return APPLICATION_LOGIN_URL.value;
	}
	
	public int getImplicitTimeOut(){
		return implicitTimeOut;
	}
	
	public int getDBTimeout(){
		return dbTimeOut;
	}
	
	public String getDefaultRunName(){
		return defaultRunName;
	}

	public String getTestDataSource(){
		return TEST_DATA_SOURCE.value;
	}
	
	public String getMacPlatform(){
		return MAC_PLATFORM.value;
	}


	/**
	 * This will set the overriding values (incoming from runtime system
	 * parameters - which take precedence over the static properties).
	 */
	private void applyOverrides(){
		applyPrimaryOverrides();
		//applyBrowserOverrides();  // handled in base class
	}
	
	private void applyPrimaryOverrides(){
		String value = null;
		for(RuntimeArgs f : RuntimeArgs.values()){
			String argName = f.getArgName();
			value = System.getProperty(argName);
			if(value != null){
				switch(f){
					case ENVIRONMENT:
						// currently the template for the URL is stored in the enumeration, but
						// this could be changed to pull from the settings file template (in the
						// field settings section) - to keep the maintenance in one place.
						setEnvironments(props, value);
						// this is meant to fall through - like the others, we want to
						// go ahead and set the override...
						break;
					default:
						System.out.println("applyPrimaryOverrides: value " + f + " not handled.");
				}
			}
		}
	}
	
	/**
	 * Handles any external arguments passed in that override the existing settings. 
	 * These arguments must have the same 'key' name as the fields contained in 
	 * this class (which will match the property file 'key' name.<br><br>
	 * This will systematically call into the BaseSettings to apply any overrides,
	 * followed by this class, followed by the same for resetting any calculated
	 * fields.
	 */
	public void applyRunplanOverrides(Map<String, String> map){
		super.applyRunplanOverrides(map);
		//super.setCalculatedFields();
		String value = null;
		for(Field f : Field.values()){
			String argName = f.key;
			value = map.get(argName);
			if(value != null){
				switch(f){
					// apply any special cases here (e.g.a calculated field)
					default:
						// for the most straightforward cases, it is a simple override
						props.setProperty(argName, value);
				}
			}
		}
		setCalculatedFields();
	}
	
	private static void setEnvironments(Properties props, String runTimeEnv){
		props.setProperty(DEFAULT_URL.key, NavigationURL.DEFAULT_URL.getUrl(runTimeEnv));
	}

	
	public String getGridHostName(){
		return GRID_HOSTNAME.value;
	}

	public String getGridPort(){
		return GRID_HOST_PORT.value;
	}

	public String getGridTimeout(){
		return GRID_TIMEOUT.value;
	}
	/**
	 * If Sauce has been flagged (checked) in the properties or via
	 * Execution Manager, this should return <code><b>true</b></code>.
	 * @return <code><b>true</b></code> if set
	 */
	public boolean isSauceRequested(){
		return sauceFlag;
	}
	/**
	 * If the chosen account has been set in properties, the proper URL
	 * will be returned - a URL that is able to be sent as the remote 
	 * web driver URL.<br><br>
	 * This value will be built regardless of the flag, but only if a corresponding
	 * account and URL are set:
	 * <pre>
	 * SauceLabsAccount=<code style='color:blue;'>autoarch</code>
	 * autoarchAcct=<code style='color:blue;'>8078a1ac-1084-4fb2-b2f2-b4633b08d9cc</code>
	 * </pre>
	 * @return proper URL, if settings are valid, otherwise <b>null</b>
	 */
	public String getSauceUrl(){
		return this.sauceAcctUrl;
	}

	public String getDefaultUrl(){
		return DEFAULT_URL.value;
	}
	
	public String getIEDriver32bit(){
		return IEDRIVER_32.value;
	}

	public String getIEDriver64bit(){
		return IEDRIVER_64.value;
	}

	public String getChromeDriver(){
		return CHROMEDRIVER.value;
	}

	/**
	 * Output of the Settings in a multi-line, formatted listing of 
	 * <code style='color:blue;'>"key: value"</code>.
	 * 
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\n");
		sb.append("Implicit Timeout: " + this.implicitTimeOut + "\n");
		sb.append("Grid Hostname:    " + GRID_HOSTNAME.value + "\n");
		sb.append("Grid Port:        " + GRID_HOST_PORT.value + "\n");
		sb.append("Grid Timeout:     " + GRID_TIMEOUT.value + "\n");
		if(sauceFlag){
			sb.append("Sauce Account:     " + SAUCE_ACCT.value + "\n");
			sb.append("Sauce URL:         " + sauceAcctUrl + "\n");
		}
		sb.append("Schema set:       " + SCHEMA_SET.value + "\n");
		sb.append("Default URL:      " + DEFAULT_URL.value + "\n");
		return sb.toString();
	}

}
