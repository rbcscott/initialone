package customproject.settings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representation of the differing URL destinations for the 'application' locations.
 */
public enum NavigationURL{

	DEFAULT_URL("http://<env>.staples.com", null, "qa");
	
	private String url = "";
	private String replacement = "";
	private String qaPrefix = "";
	/** Fixed string that marks the replacement text in the URL - for replacing with the environment #. */
	public static final String ENV_REPL = "<env>";
	private static final String ENV_MATCHER = "(q|qa)?([0-9]{1,2})";

	private NavigationURL(String url, String replacement, String qaPrefix){
		this.url = url;
		this.replacement  = replacement;
		this.qaPrefix = qaPrefix;
	}
	
	/**
	 * Returns the URL template String for the type. <br><br>
	 * 
	 * @return URL template String for the type
	 */
	public String getUrl(){
		return this.url;
	}
	
	/**
	 * Returns the URL String for the type with the given environment inserted
	 * into the template, based on the replacement string (and qaPrefix).<br><br>
	 * The given string can be either just the environment number, <code style='color:blue;'>"10"</code>,
	 * or a full representation such as <code style='color:blue;'>"QA10"</code>.<br>
	 * e.g., calling <code>DEFAULT_URL.getUrl(<code style='color:blue;'>"QA9"</code>)</code> will return<br>
	 * <code style='color:blue;'>"http://aka-qa9.staplesadvantage.com/learn?storeId=10101"</code>
	 * <br><br>Still contained will be any supplemental replacement used in the URL 
	 * (e.g., see {@link #MVP_URL}). <br><br> 
	 * @return URL String for the type, with the environment in place
	 * @param environment environment indicator, either a number or 'QA' + number
	 */
	public String getUrl(String environment){
		String returnUrl = this.url;
		String givenEnv = environment.toLowerCase();
		Pattern p = Pattern.compile(ENV_MATCHER);
		Matcher m = p.matcher(givenEnv);
		String envNumber = "";
		@SuppressWarnings("unused")
		int ct = m.groupCount();
		if(m.find() && m.groupCount() > 1){
			envNumber = m.group(2);
			String envReplacement = this.getQaPrefix() + envNumber;
			returnUrl = this.url.replace(NavigationURL.ENV_REPL, envReplacement);
		}else{
			System.out.println("Unable to properly match given environment [" + environment + "]. " +
					"Returning URL as-is (includes '" + ENV_REPL + "' replacement string).");
		}
		return returnUrl;
	}

	/**
	 * Get the string to replace for this URL.<br>
	 * For example, if "[replace]" is set, and present in the 
	 * settings URL, it can be called up and replaced where necessary.
	 * @return URL string
	 */
	public String stringToReplace(){
		return this.replacement;
	}
	
	/**
	 * Get the environment prefix for this URL. To be applied in replacement<br>
	 * Either 'q' or 'qa', depending on the URL.
	 * @return env prefix, either 'q' or 'qa'
	 */
	public String getQaPrefix(){
		return this.qaPrefix;
	}
}
