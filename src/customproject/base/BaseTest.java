/**
 *@BaseComponentName: BaseTest
 *@Description:  
 *@author: 
 *@CreatedDate:
 *@ModifiedBy:
 *@ModifiedDate:
 *@param: 
 *@return:
 */

package customproject.base;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import com.thoughtworks.selenium.SeleniumException;

import se.Se;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;
//import report.Reporting;


import core.Browser;
import core.report.Log;
import core.report.ReporterAdapter;
import core.report.Reporting.Status;
import core.settings.ExecutionMode;
import core.test.CoreBaseTest;
import core.test.TestParameters;
import customproject.report.Reporting;
import customproject.settings.ProjectSettings;
import customproject.settings.ProjectSettings.Field;
import db.RecordSet;

/**
 * Test class base. Contains some implementations and support methods,
 * as well as enforced method overrides. This is an intermediate absatrct class
 * used to support and add customized project-specific implementations, 
 * overriding where necessary.
 */
public abstract class BaseTest extends CoreBaseTest{

	
	public static Reporting reporting = Reporting.getReporting();
	public HashMap<String, String> mapDataSheet;
	private RecordSet testData = null;
	public String testBrowser;
	private static final String BROWSER_EXCEPTION_MSG = "Unable to start a new session. \n\nPossible causes: \n\t1.Selenium Grid is not setup.\n\t2.Driver executables are not properly configured.\n\t3.Port number in grid properties file is not the same as the port in which selenium hub is running\n";
	/**
	 * Local instance of TestParameters. Initialized during construction, and values
	 * assigned after settings (properties) have been established.
	 */
	ProjectTestParameters ctp = null;

	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	protected static ProjectSettings settings;

	static{
		Se.setReportingProvider(new ReporterAdapter());
		Se.setShowPassOnActionComplete(true);
		// initialize the settings
		settings = ProjectSettings.getSettings();
		// initialize TestParameters
		testParameters = new ThreadLocal<ProjectTestParameters>(){
			protected ProjectTestParameters initialValue(){
				return new ProjectTestParameters();
			}
		};
		tpOverride = true;
	}

	/**
	 * Constructs the BaseTest within the hierarchy.<br>
	 * Setup will assign the ProjectTestParameters.
	 */
	public BaseTest(){
		//ctp = (ProjectTestParameters) tp;
		logger = Log.getInstance(Thread.currentThread().getStackTrace()[1].getClassName());
	}


	/**
	 * Main driver for test case execution. <br><br>
	 * The <code>runScript()</code> call (script step-wise execution) is wrapped into 
	 * this method, and prior to running, parameters, data and the driver 
	 * are all setup.
	 */
	protected void executeTest(){
		setDefaultTestParameters();
		//tp = getTestParameters();
		//initializeData(tp);

		// open driver 
		initializeWebDriver();
		// report and set values for start of test
		markTestStarted();
		// execution of test script content...
		runScript();
		// report and set values for end of test
		markTestEnded();
		//updateResults();
	}
	/**
	 * Convenience method for obtaining the TestParameters instance
	 * associated with the current test (managed by {@link #testParameters})
	 * @return instance of {@link TestParameters}, specific to this test. If the
	 * instance has not been created, it will be, however values are not
	 * assigned until {@link #setDefaultTestParameters()} has been called.
	 */
	public static final ProjectTestParameters getTestParameters(){
		return (ProjectTestParameters) testParameters.get();
	}
	/**
	 * Sets the parameters for this test case based on the 
	 * known settings from the global settings instance. 
	 */
	protected void setDefaultTestParameters(){
		// initialize test parameters
		tp = getTestParameters();
		ctp = (ProjectTestParameters) tp;
		// set class/test name
		String className = this.getClass().getSimpleName();
		ctp.setTestName(className);
		//tp.setTestShortName(getTestShortName(className));
		// Environment/Browser settings
		ctp.setEnvironment(settings.getProperty(Field.ENVIRONMENT));
		ctp.setPlatform(settings.getPlatform());
		ctp.setBrowser(settings.getBrowser());
		
	}
	
	@SuppressWarnings("unused")
	private void initializeData(TestParameters tp){
		testData = BaseTestSql.getRegistrationData(tp);
		tp.setTestData(testData);
	}
	
	
	/**
	 * Based on parameter settings, the driver instance will get created here.
	 * Window will also be maximized and implicit timeout set (if applicable).
	 */
	protected void initializeWebDriver(){
		try {
			ExecutionMode mode = settings.getExecutionMode();
			Browser browser = getTestParameters().getBrowser();
			switch(mode){
				case LOCAL:
					logger.info("Starting [local] WebDriver for: " + browser);
					driver = WebDriverFactory.getDriver(browser);
					logger.info("Started WebDriver for " + driver);
					logger.info("...maximizing...");
					driver.manage().window().maximize();
					logger.info("Setting implicit timeout to " + settings.getImplicitTimeOut() + "s");
					driver.manage().timeouts().implicitlyWait(settings.getImplicitTimeOut(), TimeUnit.SECONDS);
					break;
				case GRID:
					String remoteServerUrl = getHubUrl();
					logger.info("Starting remote WebDriver [hub: " + remoteServerUrl + "]");
					driver = WebDriverFactory.getDriver(browser, remoteServerUrl);
					logger.info("...maximizing...");
					driver.manage().window().maximize();
					logger.info("Setting implicit timeout to " + settings.getImplicitTimeOut() + "s");
					driver.manage().timeouts().implicitlyWait(settings.getImplicitTimeOut(), TimeUnit.SECONDS);
					logger.info("startWebDriverClient: Started remoteWebDriver for " + driver);
					break;
			}
		}catch (UnreachableBrowserException e) {
			logger.error(BROWSER_EXCEPTION_MSG);
			throw new UnreachableBrowserException(BROWSER_EXCEPTION_MSG);
		}catch (WebDriverException e){
			logger.error("Unable to start a new session. Possible causes are,\n\t1.Nodes with required browsers are not available.\n\t2.Path to the driver executables are not configured correctly.",e.fillInStackTrace());
			throw new WebDriverException(e.fillInStackTrace());
		}catch (IllegalStateException e){
			logger.error("Unable to start driver. Possible causes:\n\tPath to the driver executable is not configured correctly.", e.fillInStackTrace());
			throw new IllegalStateException(e.fillInStackTrace());
		}
		getTestParameters().setDriver(driver);
	}
	/**
	 * Conditional, depending on grid machine settings, or Sauce Labs (if applicable).
	 * Returns the URL for the hub machine
	 * @return URL for the hub machine
	 */
	private String getHubUrl(){
		boolean sauceRequested = settings.isSauceRequested();
		if(sauceRequested){
			return settings.getSauceUrl();
		}else{
			return "http://" + settings.getGridHostName()	+ ":" + settings.getGridPort() + "/wd/hub";
		}
	}
	
	
	/**
	 * Main executor for the script. Typically will contain the medium/high level
	 * steps for the test case, interacting with both logical and page modules.
	 */
	public abstract void runScript();
	

	/**
	 * 
	 */
	@AfterMethod
	@Deprecated
	public void afterMethod(ITestResult testResult) throws SQLException, TestLinkAPIException {

		String testlinkTestCaseID = settings.getTestLinkTestCaseIDPrefix() + "-" + tp.getTestName().substring(5, 6);
		//TestLinkConnection testLinkConnector = new TestLinkConnection();
		@SuppressWarnings("unused")
		String result = null;
		@SuppressWarnings("unused")
		String notes =null;
		System.out.println(testResult.getName());

		//if (testDataSource.equalsIgnoreCase("database")) 
		//{
		//	while (databaseConnection != null)
		//		databaseConnection.closeDatabaseConnectivity();
		//}
		if (testResult.getStatus() == 2) {
			try {
				takeScreenshot();
				result = TestLinkAPIResults.TEST_FAILED;				
				notes="Execution failed";
				//testLinkConnector.updateTestLinkResult(testlinkTestCaseID, notes, result);  
			} catch (Exception e) {
				logger.error("Unable to take screenshot for test: " + testResult.getName(),e.fillInStackTrace());
			}
		}
		if(testResult.getStatus() == 1){
			try {
				result = TestLinkAPIResults.TEST_PASSED;				
				notes="Executed successfully";				
				//testLinkConnector.updateTestLinkResult(testlinkTestCaseID, notes, result);
			} catch (Exception e) {
				logger.error("Unable to Update testLink Status " + testlinkTestCaseID,e.fillInStackTrace());
			}
		}
		if(driver != null && driver instanceof RemoteWebDriver){
			if(((RemoteWebDriver) driver).getSessionId() != null){
				driver.close();
				driver.quit();
			}
		}

		logger.info("Performing test cleanup, closing webdriver instance.\n");
		logger.info("-----------------------------------------------------------------------");

		String formattedTime = timeFormat.format(new Date()).toString();
		logger.info("End time:       "+formattedTime);
		logger.info("-----------------------------------------------------------------------\n");
	}

	
	

	


	
	/**
	 * Returns the settings default URL.
	 */
	public static String getDefaultURL() {
		return settings.getDefaultUrl();
	}
	
	/**
	 * Captures the screenshot and saves to the results directory.
	 */
	public void takeScreenshot() throws ScreenshotException{
		File screenshotFile = getScreenshot();

		if(screenshotFile != null){
			try{
				FileUtils.copyFile(screenshotFile, new File(settings.getApplicationDirectory() + "\\" + settings.getCurrentDateTime() + "\\" + tp.getTestName() + ".png"));
				logger.info("Screen shot has been taken");
			}catch (IOException e){
				logger.error("The filename, directory name, or volume label syntax of screenshot is incorrect");
				e.printStackTrace();
			}catch (ScreenshotException e1){
				throw new ScreenshotException("Unable to take screenshot");
			}
		}
	}
	
	/**
	 * Returns the value under the column (specified by key) from the plan registration
	 * table.<br> This is predicated on two conditions:<br>
	 * <ul>
	 * <li>The table exists
	 * <li>The table was loaded properly from {@link #initializeData(TestParameters)}
	 * </ul>
	 */
	public String getValue(String key){
		if(testData != null){
			return testData.get(key);
		}
		return "";
	}

	
	/**
	 * Sets the URL based on:<br>
	 * <ul>
	 * <li>the ApplicationUrl (from GlobalSettings.properties)
	 * </ul>
	 * @param 
	 */
	public void loadAppUrl(){
		String appUrl = settings.getApplicationUrl();
		if(appUrl == null || appUrl.equals("")){
			appUrl = settings.getDefaultUrl();
		}
		// TODO - add'l follow-up checks needed
		
		// copy url onto TestParameters environment variable
		getTestParameters().setEnvironment(appUrl);
		
		report(DONE, "Loading base URL " + appUrl + "]");

		if(!appUrl.trim().equals("")){  // use a matcher for validation and report
			try{
				driver.get(appUrl);
				report(PASS, "The base URL " + appUrl + " has been called to load");
				waitForPageLoad();
			}catch (UnreachableBrowserException e){
				report(FAIL, "The base URL " + appUrl + " is loaded succesfully");
				logger.error("Unable to load the Base URL: ", e.fillInStackTrace());
				throw new UnreachableBrowserException("Unable to load the Base URL: " + appUrl);
			}
		}else{
			report(FAIL, "Unable to load the Base URL: " + appUrl + ". Please provide a valid Base URL");
			logger.error("Unable to load the Base URL: " + appUrl + ". Please provide a valid Base URL");
			throw new UnreachableBrowserException("Unable to load the Base URL: " + appUrl + " Please provide a valid Base URL.");
		}
	}
	
	
	/**
	 * Verifies page load complete using a wait period and a check against
	 * the document ready state.<br>
	 * ** note - uses default timeout of 30s
	 * @return <b>true</b> if ready state confirmed, <b>false</b> if timed out and not confirmed
	 */
	public boolean waitForPageLoad(){
		boolean ready = false;
		final int interval = 100;
		final int timeout = 30 * 1000;
		int counter = 0;
		try{
			while(!ready && counter <= timeout){
				sleep(100L);
				counter += interval;
				ready = ((JavascriptExecutor)driver).
						executeScript("return document.readyState").equals("complete");
			}
		}catch(SeleniumException e){
			e.printStackTrace();
		}
		return ready;
	}
	
	/**
	 * Convenience method for implementing delays in execution.<br><br>
	 * This method should be used judiciously - for temporary measures, or to
	 * help debug certain timing issues.<br><br>
	 * 
	 * @param time in milliseconds of intended sleep
	 */
	public void sleep(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the corresponding variable value and reports "Testing started".
	 */
	protected void markTestStarted(){
		super.markTestStarted();
		report(PASS, "Testing started");
	}

	/**
	 * Sets the corresponding variable value, quits the driver (if not
	 * already) and reports "Testing ended".
	 */
	protected void markTestEnded(){
		if(driver != null)
			driver.quit();
		super.markTestEnded();
		report(PASS, "Testing ended");
	}
	
	/**
	 * Reports out a message to the Reporter.
	 * @param passFail
	 * @param msg
	 */
	protected void report(Status passFail, String msg){
		
		Reporting.getReporting().report(passFail, msg);
		//Reporter.log("PageComponent" + this.getClass().getSimpleName());
		//Reporter.log("PageMethod" + getMethodCalledFrom());
	    //Reporter.log(passFail + "_MESSAGE:-" + msg);
	}
	
	/**
	 * Reports out a message to the Reporter.
	 * @param passFail
	 * @param msg
	 */
	protected void report(String expected, Status status, String msg){
		Reporting.getReporting().reportVP(expected, status, msg);
	}
	
	/**
	 * <i>Initial version - not fully implemented.</i>
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getMethodCalledFrom(){
		StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		String className = "";
		String methodName = "";
		for(StackTraceElement e : elems){
			className = e.getClassName();
			if(className.equals("java.lang.Thread") || 
				className.endsWith("BasePage")){
				continue;
			}
			methodName = e.getMethodName();
			break;
		}
		return methodName;
	}
	
	/**
	 * Based on the presence of the underscore '_' to indicate
	 * a separation in the encoded name and the descriptive name, this will
	 * extract the first part (encoded name) and return. Should conditions
	 * not be met, the original given name is returned.
	 * @param testName
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String getTestShortName(String testName){
		if(testName.contains("_")){
			String [] split = testName.split("_");
			if(split.length > 1){
				return split[0];
			}
		}
		return testName;
	}
	
	/**
	 * Temporary measure for 2 & 3 letter compatibility - gets the first
	 * 2-3 letter acronym from the given short name, and matches
	 * to a worded functional category.
	 * @param testShortName
	 */
	@SuppressWarnings("unused")
	private static String getFunctionalCategoryFromShortName(String testShortName){
		final String SHORTNAME_MATCH = "([A-Za-z]{1,3})[0-9]{1,3}";
		String name = "";
		Pattern p = Pattern.compile(SHORTNAME_MATCH);
		Matcher m = p.matcher(testShortName);
		String fc = "";
		if(m.find()){
			fc = m.group(1);
			name = BaseTestSql.getFunctionalName(fc);
		}else{
			System.err.println("unable to validate test case name");
		}
		return name;
	}
	/**
	 * Sets the URL based on:<br>
	 * <ul>
	 * <li>the ApplicationUrl (from GlobalSettings.properties)
	 * </ul>
	 * @param 
	 */
	public void launchBaseURL(){
		String appUrl = settings.getApplicationUrl();
		if(appUrl == null || appUrl.equals("")){
			appUrl = settings.getDefaultUrl();
		}
		// TODO - add'l follow-up checks needed
		
		// copy url onto TestParameters environment variable
		getTestParameters().setEnvironment(appUrl);
		
		report(DONE, "Loading base URL [" + appUrl + "]");

		if(!appUrl.trim().equals("")){  // use a matcher for validation and report
			try{
				driver.get(appUrl);
				report(PASS, "The base URL " + appUrl + " has been called to load");
				waitForPageLoad();
			}catch (UnreachableBrowserException e){
				report(FAIL, "The base URL " + appUrl + " is loaded succesfully");
				logger.error("Unable to load the Base URL: ", e.fillInStackTrace());
				throw new UnreachableBrowserException("Unable to load the Base URL: " + appUrl);
			}
		}else{
			report(FAIL, "Unable to load the Base URL: " + appUrl + ". Please provide a valid Base URL");
			logger.error("Unable to load the Base URL: " + appUrl + ". Please provide a valid Base URL");
			throw new UnreachableBrowserException("Unable to load the Base URL: " + appUrl + " Please provide a valid Base URL.");
		}
	}
}
