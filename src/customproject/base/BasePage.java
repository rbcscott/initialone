
package customproject.base;

import static se.LocatorType.XPATH;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.Reporter;

import core.CoreBasePage;
import customproject.report.Reporting;
import customproject.settings.NavigationURL;
import customproject.settings.ProjectSettings;
import customproject.utilities.Log;
import se.objectlib.web.WebElem;

public abstract class BasePage extends CoreBasePage{
	
	protected static final ProjectSettings settings = ProjectSettings.getSettings();


	/**
	 * Creates BasePage, passing given driver instance to the super. Initializers
	 * for reporting and logging are executed here.
	 * @param driver instance of WebDriver to be used for page actions
	 */
	public BasePage(WebDriver driver){
		super(driver);
		initializeLogger();
		initializeReporting();
	}

	
	protected void initializeLogger(){
		logger = Log.getInstance(Thread.currentThread().getStackTrace()[1].getClassName());
	}
	
	protected void initializeReporting(){
		reporting = Reporting.getReporting();
	}

	public void launchUrl(NavigationURL url){
		launchUrl(url, null);
	}
	
	/**
	 * Loads the URL based on the specified navigational URL. The URL string
	 * is based on the settings value. For this implementation, if there is a
	 * replacement (e.g. MVP), it is passed in and replaced where applicable.
	 * @param navUrl
	 * @param replacement
	 */
	public void launchUrl(NavigationURL navUrl, String replacement){
		String targetUrl = "";
		switch(navUrl){
			case DEFAULT_URL:
				targetUrl = settings.getDefaultUrl();
				break;
			default:
				System.err.println("launchUrl(navUrl) - given navUrl [" + navUrl + "] is not handled");
				break;
		}
		if(!targetUrl.trim().equals("")){
			try{
				driver.get(targetUrl);
				reportVP(targetUrl, "NA", targetUrl + " loaded", PASS, "The Base URL: " + targetUrl + " is loaded succesfully.");
				logger.info(navUrl + ": " + targetUrl + " is loaded succesfully");
			}catch (UnreachableBrowserException e){
				reportVP(targetUrl, "NA", targetUrl + " loaded", FAIL, "Unable to load the " + navUrl + ".");
				logger.error("Unable to load the " + navUrl + ": ", e.fillInStackTrace());
				throw new UnreachableBrowserException("Unable to load the " + navUrl + ": " + targetUrl);
			}
		}else{
			reportVP(targetUrl, "NA", targetUrl + " loaded", FAIL, "Unable to load the Base URL");
			logger.error("Unable to load the " + navUrl + ": " + targetUrl + " Please provide a valid " + navUrl + ".");
			throw new RuntimeException("Unable to load the " + navUrl + ": " + targetUrl + ". URL string is empty. Please provide a valid " + navUrl + ".");
		}
	}
	
	

	
	
	/**
	 * Returns the title of the document obtained from teh 
	 * current driver instance.
	 */
	public String getTitle(){
		String title = driver.getTitle();
		logger.info("The Current Window title is " + title);
		return title;
	}

	/**
	 * Returns whether the given URL matches the current URL
	 * obtained from the driver instance.
	 * @param expectedURL
	 * @return <b>true</b> if matches current, <b>false</b> otherwise
	 */
	public boolean isUrlAsExpected(String expectedURL){
		String url = getCurrentURL();
		logger.info("The Current URL: " + url + "  Expected URL: " + expectedURL);
		return url.equals(expectedURL);
	}

	

	/**
	 * Checks whether the current window title matches expected.
	 * @param expectedTitle - Title expected in the Current Window
	 * @return <b>true</b> if the current title matches the
	 *          expectedTitle, otherwise <b>false</b>
	 */
	public boolean isTitleAsExpected(String expectedTitle){
		logger.info("The current window title is " + getPageTitle() + " whereas the expected is " + expectedTitle);
		return expectedTitle.equals(getPageTitle());
	}

	
	@SuppressWarnings("unused")
	private void reportTestNg(String input, String output, String expected, String passFail, String msg){
		String methodName = getMethodCalledFrom();
		Reporter.log("TestStepComponent" + methodName);
		Reporter.log("TestStepInput:-" + input);
		Reporter.log("TestStepOutput:-" + output);
		Reporter.log("TestStepExpectedResult:-" + expected);
		Reporter.log(passFail + "_MESSAGE:-" + msg);
	}
	
	@SuppressWarnings("unused")
	private void reportDb(String input, String output, String expected, String passFail, String msg){
		
	}
	
	/**
	 * Waits for the loading dialog to finish, if doesn't finish after some time
	 * it will fail the test.
	 */
	public void waitWhileLoading(){
		System.out.println("creating....");
		class LoadingDialog extends WebElem{
			public LoadingDialog(WebDriver driver){
				super(driver);
				setElement(XPATH, "//*[contains(text(),'Loading')]");
			}
		}
		LoadingDialog loadingDialog = new LoadingDialog(driver);
		System.out.println("existBefore");
		if(loadingDialog.existsBefore(2)){
			System.out.println("existAfter");
			if(loadingDialog.existsAfter(15)){
				System.out.println("FAIL TEST");
				report(FAIL, "waited 15s for loading message to disappear - however it still exists.");
			}
		}
	}
	
	/**
	 * Waits for the loading dialog to finish, if doesn't finish after some time
	 * it will fail the test.
	 */
	public void waitForLoadingModal() {
		try {
			class LoadingDialog extends WebElem{
				public LoadingDialog(WebDriver driver){
					super(driver);
					setElement(XPATH, "//*[contains(text(),'Loading')]");
				}
			}
			LoadingDialog loadingDialog = new LoadingDialog(driver);
			boolean flag = loadingDialog.waitForExistence(1);
			while (flag) {
				flag = loadingDialog.waitForExistence(1);
			}
		} catch (StaleElementReferenceException st) {

		}
	}
	
	/**
	 * Catch any alert and accept it.
	 * 
	 * @return TRUE if was possible to accept the alert and FALSE in any other
	 *         case.
	 */
	public Boolean isAlertExistAndAccepted(){
		try{
			Alert alert = driver.switchTo().alert();
			alert.accept();
			return true;
		}catch (NoAlertPresentException e){
			return false;
		}
	}
}
