/**
 *@BaseComponentName: BaseTest
 *@Description:  
 *@author: 
 *@CreatedDate:
 *@ModifiedBy:
 *@ModifiedDate:
 *@param: 
 *@return:
 */

package customproject.base;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.Se;
import core.report.Log;
import core.report.ReporterAdapter;
import core.report.Reporting.Status;
import core.test.CoreAPIBaseTest;
import core.test.TestParameters;
import customproject.report.Reporting;
import customproject.settings.ProjectSettings;
import customproject.settings.ProjectSettings.Field;
import db.RecordSet;

/**
 * Test class base. Contains some implementations and support methods,
 * as well as enforced method overrides. This is an intermediate abstract class
 * used to support and add customized project-specific implementations, 
 * overriding where necessary.
 */
public abstract class APIBaseTest extends CoreAPIBaseTest{

	
	public static Reporting reporting = Reporting.getReporting();
	public HashMap<String, String> mapDataSheet;
	private RecordSet testData = null;
	public String testBrowser;
	
	/**
	 * Local instance of TestParameters. Initialized during construction, and values
	 * assigned after settings (properties) have been established.
	 */
	ProjectTestParameters ctp = null;

	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	protected static ProjectSettings settings;

	static{
		Se.setReportingProvider(new ReporterAdapter());
		Se.setShowPassOnActionComplete(true);
		// initialize the settings
		settings = ProjectSettings.getSettings();
		// initialize TestParameters
		testParameters = new ThreadLocal<ProjectTestParameters>(){
			protected ProjectTestParameters initialValue(){
				return new ProjectTestParameters();
			}
		};
		tpOverride = true;
	}

	/**
	 * Constructs the BaseTest within the hierarchy.<br>
	 * Setup will assign the ProjectTestParameters.
	 */
	public APIBaseTest(){
		//ctp = (ProjectTestParameters) tp;
		logger = Log.getInstance(Thread.currentThread().getStackTrace()[1].getClassName());
	}


	/**
	 * Main driver for test case execution. <br><br>
	 * The <code>runScript()</code> call (script step-wise execution) is wrapped into 
	 * this method, and prior to running, parameters, data and the driver 
	 * are all setup.
	 */
	protected void executeTest(){
		setDefaultTestParameters();
		//tp = getTestParameters();
		//initializeData(tp);

		// open driver 
		initializeWebDriver();
		// report and set values for start of test
		markTestStarted();
		// execution of test script content...
		runScript();
		// report and set values for end of test
		markTestEnded();
		//updateResults();
	}
	/**
	 * Convenience method for obtaining the TestParameters instance
	 * associated with the current test (managed by {@link #testParameters})
	 * @return instance of {@link TestParameters}, specific to this test. If the
	 * instance has not been created, it will be, however values are not
	 * assigned until {@link #setDefaultTestParameters()} has been called.
	 */
	public static final ProjectTestParameters getTestParameters(){
		return (ProjectTestParameters) testParameters.get();
	}
	/**
	 * Sets the parameters for this test case based on the 
	 * known settings from the global settings instance. 
	 */
	protected void setDefaultTestParameters(){
		// initialize test parameters
		tp = getTestParameters();
		ctp = (ProjectTestParameters) tp;
		// set class/test name
		String className = this.getClass().getSimpleName();
		ctp.setTestName(className);
		//tp.setTestShortName(getTestShortName(className));
		// Environment/Browser settings
		ctp.setEnvironment(settings.getProperty(Field.ENVIRONMENT));
		ctp.setPlatform(settings.getPlatform());
		ctp.setBrowser(settings.getBrowser());
		
	}
	
	@SuppressWarnings("unused")
	private void initializeData(TestParameters tp){
		testData = BaseTestSql.getRegistrationData(tp);
		tp.setTestData(testData);
	}
	
	
	
	
	
	
	/**
	 * Main executor for the script. Typically will contain the medium/high level
	 * steps for the test case, interacting with both logical and page modules.
	 */
	public abstract void runScript();
	

	
	/**
	 * Returns the settings default URL.
	 */
	public static String getDefaultURL() {
		return settings.getDefaultUrl();
	}
	
	
	
	/**
	 * Returns the value under the column (specified by key) from the plan registration
	 * table.<br> This is predicated on two conditions:<br>
	 * <ul>
	 * <li>The table exists
	 * <li>The table was loaded properly from {@link #initializeData(TestParameters)}
	 * </ul>
	 */
	public String getValue(String key){
		if(testData != null){
			return testData.get(key);
		}
		return "";
	}

	
	/**
	 * Convenience method for implementing delays in execution.<br><br>
	 * This method should be used judiciously - for temporary measures, or to
	 * help debug certain timing issues.<br><br>
	 * 
	 * @param time in milliseconds of intended sleep
	 */
	public void sleep(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the corresponding variable value and reports "Testing started".
	 */
	protected void markTestStarted(){
		super.markTestStarted();
		report(PASS, "Testing started");
	}

	/**
	 * Sets the corresponding variable value, quits the driver (if not
	 * already) and reports "Testing ended".
	 */
	protected void markTestEnded(){
		super.markTestEnded();
		report(PASS, "Testing ended");
	}
	
	/**
	 * Reports out a message to the Reporter.
	 * @param passFail
	 * @param msg
	 */
	protected void report(Status passFail, String msg){
		
		Reporting.getReporting().report(passFail, msg);
		//Reporter.log("PageComponent" + this.getClass().getSimpleName());
		//Reporter.log("PageMethod" + getMethodCalledFrom());
	    //Reporter.log(passFail + "_MESSAGE:-" + msg);
	}
	
	/**
	 * <i>Initial version - not fully implemented.</i>
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getMethodCalledFrom(){
		StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		String className = "";
		String methodName = "";
		for(StackTraceElement e : elems){
			className = e.getClassName();
			if(className.equals("java.lang.Thread") || 
				className.endsWith("BasePage")){
				continue;
			}
			methodName = e.getMethodName();
			break;
		}
		return methodName;
	}
	
	/**
	 * Based on the presence of the underscore '_' to indicate
	 * a separation in the encoded name and the descriptive name, this will
	 * extract the first part (encoded name) and return. Should conditions
	 * not be met, the original given name is returned.
	 * @param testName
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String getTestShortName(String testName){
		if(testName.contains("_")){
			String [] split = testName.split("_");
			if(split.length > 1){
				return split[0];
			}
		}
		return testName;
	}
	
	/**
	 * Temporary measure for 2 & 3 letter compatibility - gets the first
	 * 2-3 letter acronym from the given short name, and matches
	 * to a worded functional category.
	 * @param testShortName
	 */
	@SuppressWarnings("unused")
	private static String getFunctionalCategoryFromShortName(String testShortName){
		final String SHORTNAME_MATCH = "([A-Za-z]{1,3})[0-9]{1,3}";
		String name = "";
		Pattern p = Pattern.compile(SHORTNAME_MATCH);
		Matcher m = p.matcher(testShortName);
		String fc = "";
		if(m.find()){
			fc = m.group(1);
			name = BaseTestSql.getFunctionalName(fc);
		}else{
			System.err.println("unable to validate test case name");
		}
		return name;
	}
	
}
