package customproject.base;

import core.test.TestParameters;
import db.AutoDB;
import db.JdbcOracle;
import db.Record;
import db.RecordSet;

public class BaseTestSql{

	private static RecordSet categoryMap = null;
	
	/**
	 * Using the test name, gathers the test case data from
	 * the corresponding table.<br><br>
	 * Currently, this is set for compatibility with the original
	 * table sets.
	 * @param tp test parameters instance (test name is utilized)
	 * @return
	 */
	public static RecordSet getRegistrationData(TestParameters tp){
		RecordSet rs = new RecordSet();
		String cat = tp.getFunctionalCategory().toUpperCase();
		String sql = 
				"SELECT * FROM DBSADV_" + cat + " " +
				"WHERE TEST_CASE_NAME = '" + tp.getTestName() + "'";
		JdbcOracle jdbc = new JdbcOracle();
		System.out.println("Executing SQL [" + sql + "]");
		rs = jdbc.executeSql(AutoDB.parameters(), sql);
		return rs;
	}
	
	/**
	 * Pulls the record from NAVIGATION_URL where COUNTRY_NAME
	 * is equal to the given country name.
	 * @param country
	 * @return RecordSet of URL records
	 */
	public static Record<String> getUrlBy(String country){
		RecordSet rs = new RecordSet();
		String sql = 
				"SELECT * FROM COUNTRY " +
				"WHERE COUNTRY_NAME = '" + country + "'";
		JdbcOracle jdbc = new JdbcOracle();
		System.out.println("Executing SQL [" + sql + "]");
		rs = jdbc.executeSql(AutoDB.parameters(), sql);
		return rs.getRecord(0);
	}
	
	/**
	 * Gets the mapped functional category from the given abbreviation/acronym.<br>
	 * This method will use a cached capture of the necessary table data after initial retrieval.
	 * @param fcAbbrev
	 * @return worded functional category
	 */
	public static String getFunctionalName(String fcAbbrev){
		if(categoryMap == null){
			String sql = 
					"SELECT * FROM CATEGORY_MAP ";
			JdbcOracle jdbc = new JdbcOracle();
			categoryMap = jdbc.executeSql(AutoDB.parameters(), sql);
		}
		Record<String> rec = categoryMap.getRecordWhere("FC_ABBREV", fcAbbrev);
		if(rec.isEmpty()){
			rec = categoryMap.getRecordWhere("FC_ABBREV2", fcAbbrev);
		}
		return rec.get("CATEGORY_DB_NAME");
	}
	
	/**
	 * Gets the mapped functional area from the given abbreviation/acronym.<br>
	 * This method will use a cached capture of the necessary table data after initial retrieval.
	 * @param fcAbbrev
	 * @return worded functional category
	 */
	public static String getFunctionalArea(String fcAbbrev){
		if(categoryMap == null){
			String sql = 
					"SELECT * FROM CATEGORY_MAP ";
			JdbcOracle jdbc = new JdbcOracle();
			categoryMap = jdbc.executeSql(AutoDB.parameters(), sql);
		}
		Record<String> rec = categoryMap.getRecordWhere("FC_ABBREV", fcAbbrev);
		if(rec.isEmpty()){
			rec = categoryMap.getRecordWhere("FC_ABBREV2", fcAbbrev);
		}
		return rec.get("AREA");
	}
	
}
