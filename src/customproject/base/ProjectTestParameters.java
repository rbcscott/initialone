package customproject.base;

import core.test.TestParameters;

public class ProjectTestParameters extends TestParameters{

	
	
	/**
	 * Output of the contained test parameters in a multi-line listing of "key: value".
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("TestCase:          " + testName + "\n");
		sb.append("TestCase (short):  " + testShortName + "\n");
		sb.append("Category:          " + functionalCategory + "\n");
		sb.append("Area:              " + functionalArea + "\n");
		sb.append("Platform:          " + platform + "\n");
		sb.append("Browser:           " + ((browser != null) ? browser.getDisplayValue() : "null") + "\n");
		sb.append("Browser Version:   " + browserVersion + "\n");
		sb.append("Environment:       " + environment + "\n");
		sb.append("WebDriver:         " + driver + "\n");
		sb.append("VM Name:           " + vmName + "\n");
		sb.append("Test Data:         \n" + testData + "\n");
		return sb.toString();
	}
	
}
