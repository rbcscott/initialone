package customproject.base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;

import core.Browser;
import core.PlatformOS;
import core.test.TestParameters;
import customproject.settings.ProjectSettings;

/**
 * Support class for creating the {@link WebDriver} instance.<br><br>
 * Methods:
 * <ul>
 * <li>{@link #getDriver(Browser)} - for Local use
 * <li>{@link #getDriver(Browser, String)} - [grid] browser with URL
 * <li>{@link #getDriver(Browser, String, Platform, String)} - [grid] same, using platform indicator
 * <li>{@link #getDriver(Browser, String, Platform, String, TestParameters)} - [grid] test parameter usage
 * </ul>
 * 
 */
public class WebDriverFactory{
	private static ProjectSettings properties = ProjectSettings.getSettings();
	private static final String SAUCE_INDICATOR = "saucelabs.com";

	/**
	 * Creates and returns a WebDriver instance of the specified browser.<br><br>
	 * Settings options are taken into account, and it creates a localized 
	 * driver instance (non-grid).
	 * @param browser {@link Browser}
	 * @return new WebDriver with the specified settings
	 */
	public static WebDriver getDriver(Browser browser){
		WebDriver driver = null;
		boolean proxyRequired = false;
		
		String relativePath = new File(System.getProperty("user.dir")).getAbsolutePath();
		DesiredCapabilities capabilities = getCapabilities(browser);

		switch(browser){
			case CHROME:
				if(PlatformOS.isWindows()){
					System.setProperty("webdriver.chrome.driver", properties.getChromeDriver());
				}else{
					// MAC ...
					System.setProperty("webdriver.chrome.driver", relativePath + "\\Driver\\" + properties.getProperty("ChromeDriverPath") + ".exe");
				}
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				driver = new ChromeDriver(options);
				driver.manage().deleteAllCookies();
				driver.quit();
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
				break;
			case INTERNET_EXPLORER:
				System.setProperty("webdriver.ie.driver", properties.getIEDriver32bit());
				driver = new InternetExplorerDriver();
				driver.manage().deleteAllCookies();
				driver.quit();
				driver = new InternetExplorerDriver(capabilities);
				driver.manage().window().maximize();
				break;
			case EDGE:
				driver = new EdgeDriver();
				driver.manage().deleteAllCookies();
				driver.quit();
				driver = new EdgeDriver(capabilities);
				driver.manage().window().maximize();
				break;
			case SAFARI:
				driver = new SafariDriver(capabilities);
				driver.manage().window().maximize();
				break;
			case FIREFOX:
				StringBuilder path = new StringBuilder();
				String userName = System.getProperty("user.name");

				path.append("C:\\Users\\");
				path.append(userName);
				path.append("\\Downloads\\");
				
				FirefoxProfile profile = null;
				
				if(properties != null){
					String profileLoc = properties.getProperty("FirefoxProfile");
					if(profileLoc != null && !profileLoc.equals("") && !profileLoc.equalsIgnoreCase("none")){
						ProfilesIni ini = new ProfilesIni();
						profile = ini.getProfile(profileLoc);
						profile.setAcceptUntrustedCertificates(true);
						profile.setPreference("browser.download.folderList", 2);
						profile.setPreference("browser.helperApps.alwaysAsk.force", false);
						profile.setPreference("browser.download.manager.showWhenStarting",false);
						profile.setPreference("browser.download.dir", path.toString());
						profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
						capabilities.setCapability(FirefoxDriver.PROFILE, profile);
					}
				}
				driver = new FirefoxDriver(capabilities);
				driver.manage().window().maximize();
				break;
			case HTML_UNIT:
				driver = new HtmlUnitDriver();
				driver.manage().deleteAllCookies();
				driver.quit();
				if(proxyRequired){
					driver = new HtmlUnitDriver(){
						@Override
						protected WebClient modifyWebClient(WebClient client){
							DefaultCredentialsProvider credentialsProvider = new DefaultCredentialsProvider();
							credentialsProvider.addNTLMCredentials(properties.getProperty("Username"),
									properties.getProperty("Password"), properties.getProperty("ProxyHost"),
									Integer.parseInt(properties.getProperty("ProxyPort")), "",
									properties.getProperty("Domain"));
							client.setCredentialsProvider(credentialsProvider);
							return client;
						}
					};

					((HtmlUnitDriver) driver).setProxy(properties.getProperty("ProxyHost"),
							Integer.parseInt(properties.getProperty("ProxyPort")));
				} else{
					driver = new HtmlUnitDriver();
				}
				break;
			default:
				System.err.println("Currently an unsupported browser [" + browser + "] for driver execution.");
		}
		return driver;
	}

	/**
	 * Creates and returns a WebDriver [RemoteWebDriver] using the given browser and 
	 * URL.<br><br>
	 * @param browser {@link Browser}
	 * @param remoteUrl URL of hub
	 * @return new WebDriver with the specified settings
	 */
	public static WebDriver getDriver(Browser browser, String remoteUrl){
		return getDriver(browser, null, null, remoteUrl);
	}

	/**
	 * Creates and returns a WebDriver [RemoteWebDriver] using the given browser, version
	 * platform and URL.<br><br>
	 * @param browser {@link Browser}
	 * @param browserVersion version of browser to specify
	 * @param platform {@link Platform}
	 * @param remoteUrl URL of hub
	 * @return new WebDriver with the specified settings
	 */
	public static WebDriver getDriver(Browser browser, String browserVersion, Platform platform, String remoteUrl){
		return getDriver(browser, browserVersion, platform, remoteUrl, null);
	}

	/**
	 * Creates and returns a WebDriver [RemoteWebDriver] using the given browser, version,
	 * platform, URL and test parameters.<br><br>
	 * <i>Test Parameters usage may not be fully implemented.</i>
	 * @param browser {@link Browser}
	 * @param browserVersion version of browser to specify
	 * @param platform {@link Platform}
	 * @param remoteUrl URL of hub
	 * @param testParams parameters (may contain information to relay to driver)
	 * @return new WebDriver with the specified settings
	 */
	public static WebDriver getDriver(Browser browser, String browserVersion, Platform platform, String remoteUrl, TestParameters testParams){

		//String buildId = "";
		//String currentTestCase = "";
		//String testCycleId = "";
		//String cycleId = "";
		
		if(testParams != null){
			//buildId = testParams.getbuildId();
			//currentTestCase = testParams.getCurrentTestcase();
		}
		
		ProjectSettings settings = ProjectSettings.getSettings();

		DesiredCapabilities desiredCapabilities = getCapabilities(browser);

		if(browserVersion != null){
			desiredCapabilities.setVersion(browserVersion);
		}
		
		if(platform == null){
			platform = settings.getPlatform();
		}
		
		
		desiredCapabilities.setPlatform(platform);
		desiredCapabilities.setJavascriptEnabled(true);

		// for sauce, apply settings/overrides
		if(remoteUrl.contains(SAUCE_INDICATOR)){
			applySauceLabsCapabilities(desiredCapabilities, browser);
		}


		URL url = null;
		try{
			url = new URL(remoteUrl);
		} catch(MalformedURLException e){
			e.printStackTrace();
			return null;
		}

		WebDriver driver = new RemoteWebDriver(url, desiredCapabilities);
		driver.manage().window().maximize();
		return driver;
	}

	@SuppressWarnings("unused")
	private static DesiredCapabilities getProxyCapabilities(){
		Proxy proxy = new Proxy();
		proxy.setProxyType(ProxyType.MANUAL);

		
		String proxyUrl = properties.getProperty("ProxyHost") + ":" + properties.getProperty("ProxyPort");

		proxy.setHttpProxy(proxyUrl);
		proxy.setFtpProxy(proxyUrl);
		proxy.setSslProxy(proxyUrl);
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);

		return desiredCapabilities;
	}

	private static DesiredCapabilities applySauceLabsCapabilities(DesiredCapabilities desiredCapabilities, Browser browser){
		switch(browser){
			case INTERNET_EXPLORER:
				// default to Win 7 for sauce setup - this will change to support others
				desiredCapabilities.setCapability("platform", "Windows 7");
				desiredCapabilities.setCapability("screen-resolution", "1440x900");
				break;
			case EDGE:
				// default to Win 7 for sauce setup - this will change to support others
				desiredCapabilities.setCapability("platform", "Windows 10");
				desiredCapabilities.setCapability("screen-resolution", "1440x900");
				break;
			case CHROME:
				// default to Win 7 for sauce setup - this will change to support others
				desiredCapabilities.setCapability("platform", "Windows 7");
				desiredCapabilities.setCapability("screen-resolution", "1440x900");
				break;
			case FIREFOX:
				// default to Win 7 for sauce setup - this will change to support others
				desiredCapabilities.setCapability("platform", "Windows 7");
				desiredCapabilities.setCapability("screen-resolution", "1440x900");
				break;
			case SAFARI:
				String macSpecificPlatform = ProjectSettings.getSettings().getMacPlatform();
				if(macSpecificPlatform == null || macSpecificPlatform.equals("")){
					String browserVersion = desiredCapabilities.getVersion();
					if(browserVersion == null || browserVersion.trim().equals("")){
						browserVersion = "8";// default - for now
					}
					macSpecificPlatform = getOsxVersionByBrowser(browserVersion);
				}
				desiredCapabilities.setCapability("platform", macSpecificPlatform);
				desiredCapabilities.setCapability("screen-resolution", "1024x768");
				break;
			case IPAD:
				desiredCapabilities = DesiredCapabilities.ipad();
				desiredCapabilities.setCapability("platform", "OS X 10.10");
				desiredCapabilities.setCapability("version", "8.2");
				desiredCapabilities.setCapability("deviceName", "iPad Simulator");
				desiredCapabilities.setCapability("deviceOrientation", "landscape");
				break;
			case ANDROID:
				desiredCapabilities = DesiredCapabilities.android();
				desiredCapabilities.setCapability("platform", "linux");
				desiredCapabilities.setCapability("version", "5.1");
				desiredCapabilities.setCapability("deviceName", "Android Emulator");
				desiredCapabilities.setCapability("deviceOrientation", "landscape");
				break;
			default:
				System.err.println("Unsupported browser type: " + browser);
				break;
		}
		if(desiredCapabilities != null){
			// tags (somewhat experimental until output is fully established)
			desiredCapabilities.setCapability("build", "");
			desiredCapabilities.setCapability("name", "");
			List<String> list = new ArrayList<String>();
			desiredCapabilities.setCapability("tags", list);
		}
		return desiredCapabilities;
	}
	
	private static DesiredCapabilities getCapabilities(Browser browser){
		DesiredCapabilities capabilities = null;
		switch(browser){
			case CHROME:
				capabilities = DesiredCapabilities.chrome();
				break;

			case FIREFOX:
				capabilities = DesiredCapabilities.firefox();

				StringBuilder path = new StringBuilder();
				String userName = System.getProperty("user.name");

				path.append("C:\\Users\\");
				path.append(userName);
				path.append("\\Downloads\\");
				
				FirefoxProfile profile = null;
				
				if(properties != null){
					String profileLoc = properties.getProperty("FirefoxProfile");
					if(profileLoc != null && !profileLoc.equals("") && !profileLoc.equalsIgnoreCase("none")){
						ProfilesIni ini = new ProfilesIni();
						profile = ini.getProfile(profileLoc);
						profile.setAcceptUntrustedCertificates(true);
						profile.setPreference("browser.download.folderList", 2);
						profile.setPreference("browser.helperApps.alwaysAsk.force", false);
						profile.setPreference("browser.download.manager.showWhenStarting",false);
						profile.setPreference("browser.download.dir", path.toString());
						profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
						capabilities.setCapability(FirefoxDriver.PROFILE, profile);
					}
				}
				break;

			case HTML_UNIT:
				capabilities = DesiredCapabilities.htmlUnit();
				// NTLM authentication for proxy supported
				break;

			case INTERNET_EXPLORER:
				capabilities = DesiredCapabilities.internetExplorer();
				// To manage cookies and settings
				//properties = Settings.getInstance();
				//ieCapabilities.setCapability(INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, false);
				//ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				//ieCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, false);
				try{
					//ieCapabilities = DesiredCapabilities.internetExplorer();
					capabilities.setCapability("browserName", "internet explorer");
					capabilities.setCapability("acceptSslCerts", "true");
					capabilities.setCapability("javascriptEnabled", "true");
					// Require Window Focus - turn on if requested
					if(properties.getProperty("IERequireWindowFocus", "").equalsIgnoreCase("true")){
						capabilities.setCapability("requireWindowFocus", true);
					}
					// Protected Mode Settings - only turn on if requested
					if(properties.getProperty("IEIgnoreProtectedModeSettings").equalsIgnoreCase("true")){
						capabilities.setCapability("ignoreProtectedModeSettings", true);
					}
					// Persistent Hover - Only turn off if requested
					if(properties.getProperty("IEEnablePersistentHover").equalsIgnoreCase("false")){
						capabilities.setCapability("enablePersistentHover", false);
					}
					// Native Events - only turn off if requested
					if(properties.getProperty("NativeEvents").equalsIgnoreCase("false")){
						capabilities.setCapability("nativeEvents", false);
					}
				}

				catch (IllegalStateException e){

				}
				break;
			case EDGE:
				capabilities = DesiredCapabilities.edge();
			case SAFARI:
				capabilities = DesiredCapabilities.safari();
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, false);
				break;
			default:
				System.err.println("Currently an unsupported browser [" + browser + "] for driver execution.");
		}
		return capabilities;
	}
	
	/**
	 * Not formalized, but gets the OSX version based on browser version (for
	 * supported SauceLabs configurations)
	 * @param browserVersion
	 * @return compatible OSX version for Sauce
	 */
	private static String getOsxVersionByBrowser(String browserVersion){
		StringBuilder osx = new StringBuilder("OS X ");
		if(browserVersion.startsWith("8")){
			osx.append("10.10");
		}else if(browserVersion.startsWith("7")){
			osx.append("10.9");
		}else{
			osx.append("10.8");
		}
		return osx.toString();
	}
}
