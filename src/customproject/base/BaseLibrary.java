package customproject.base;

import core.CoreLibrary;
import customproject.report.Reporting;

/**
 * Provides support for functional and page-related module
 * implementation.<br><br>
 * As an extension of the {@link CoreLibrary}, custom functionality
 * can be added here for use throughout the subclasses. The CoreLibrary
 * provides many of the standard reporting and other methods necessary, but 
 * project-specific needs should be added to this class.
 */
public abstract class BaseLibrary extends CoreLibrary{
	
	public BaseLibrary(){
		reporting = Reporting.getReporting();
	}

}
