package customproject.functional.account.sql;

import core.test.TestParameters;
import db.AutoDB;
import db.JdbcOracle;
import db.RecordSet;

public class AccountSql{

	/**
	 * Query and return account data for this test case.
	 * @param tp test parameters
	 * @return
	 */
	public static RecordSet getAccountData(TestParameters tp){
		RecordSet rs = new RecordSet();
		String sql = 
				"SELECT * FROM ACCOUNT " +
				"WHERE TESTCASE = '" + tp.getTestShortName() + "' ";
		JdbcOracle jdbc = new JdbcOracle();
		System.out.println("Executing SQL [" + sql + "]");
		rs = jdbc.executeSql(AutoDB.parameters(), sql);
		return rs;
	}
	
}
