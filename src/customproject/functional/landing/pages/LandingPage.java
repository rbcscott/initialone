package customproject.functional.landing.pages;

import org.openqa.selenium.WebDriver;

import customproject.functional.landing.pageobjects.LandingPageObjects;

/**
 * Object/UI interactions for the Landing page.<br><br>
 * Holds multiple methods for the landing page header navigation,
 * search, etc.
 */
public class LandingPage extends LandingPageObjects{

	/**
	 * Create new LandingPage object with the given driver.<br><br>
	 * Driver is passed through to the backing object class.
	 * @param driver active driver to use for this page
	 */
	public LandingPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Selects the 'MY ACCOUNT' option from the header navigation
	 * ribbon.<br><br>
	 * This is expected to display the list of options directly
	 * below (no page navigation).
	 */
	public void selectMyAccount(){
		if(TopNavigationRibbon.AccountNavItem.existsBefore(10)){
			TopNavigationRibbon.AccountNavItem.click();
		}else{
			reportVP("MY ACCOUNT is in existence", "MY ACCOUNT is not in existence.");
		}
	}
	
	/**
	 * Selects the 'SIGN IN' button from the list below
	 * the 'MY ACCOUNT' navigation option.<br><br>
	 * It is expected that this list is displayed, typically
	 * from a call to {@link #selectMyAccount()}
	 */
	public void selectSignIn(){
		if(TopNavigationRibbon.AccountNavItem.MenuContainer.existsBefore(5)){
			TopNavigationRibbon.AccountNavItem.MenuContainer.SignIn.navigate();
		}else{
			reportVP("Account menu list is displayed", FAIL, "Account menu list is not displayed");
		}
	}

	

	

}
