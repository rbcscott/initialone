package customproject.functional.landing;

import customproject.base.BaseLibrary;
import customproject.base.BaseTest;
import customproject.base.ProjectTestParameters;
import customproject.functional.landing.pages.LandingPage;

/**
 * Functional module for Landing page actions.
 */
public class Landing extends BaseLibrary{

	LandingPage landingPage = null;
	ProjectTestParameters ptp;
	
	/**
	 * Creates new instance of the Landing module.<br><br>
	 * Holds initializers for the test parameters and page
	 * classes that are used within.
	 */
	public Landing(){
		ptp = BaseTest.getTestParameters();
		landingPage = new LandingPage(ptp.getDriver());
	}
	
	
	/**
	 * Performs actions from the landing page to get to the login page.<br><br>
	 * <ul>
	 * <li>Selects MY ACCOUNT navigation button
	 * <li>Selects the SIGN IN option
	 * </ul>
	 */
	public void navigateToLogin(){
		landingPage.selectMyAccount();
		landingPage.selectSignIn();
		reportVP("Navigation to login", DONE, "Landing navigation complete");
	}

}
