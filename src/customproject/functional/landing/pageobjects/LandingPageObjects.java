package customproject.functional.landing.pageobjects;

import se.objectlib.web.*;
import static se.LocatorType.*;

import org.openqa.selenium.WebDriver;

import customproject.base.BasePage;
/**
 * Abstract superclass containing object definitions for LandingPageObjects (Selenium objects).
 * This class is intended for extension by a script or any class supporting object actions.<br><br>
 * <em>File contents are auto-generated - any changes made to this file will be overwritten
 * by a subsequent update to the corresponding object file via the Se Object View.</em>
 */
public abstract class LandingPageObjects extends BasePage{

	public LandingPageObjects(WebDriver driver){
		super(driver);
		objectInit();
	}

	private void objectInit(){
		TopNavigationRibbon = new TopNavigationRibbon(driver);
	}

	/**
	 * <b>Parent:</b> {@link  }<pre>
	 * <b>Entity:</b>  <em>Object</em>
	 * <b>Class:</b>   WebList
	 * <b>Name:</b>    TopNavigationRibbon<br>
	 * 	<font color="green">path [@className==stp--site-nav]</font>
	 * Number of child objects: 1</pre>
	 */
	protected TopNavigationRibbon TopNavigationRibbon;

	/**
	 * <b>Parent:</b> {@link  LandingPageObjects}<pre>
	 * <b>Entity:</b>  <em>Object</em>
	 * <b>Class:</b>   WebList
	 * <b>Name:</b>    TopNavigationRibbon<br>
	 * 	path [@className==stp--site-nav]
	 * <br>Number of child objects: 1</pre>
	 */
	public class TopNavigationRibbon extends WebList{

		public TopNavigationRibbon(WebDriver driver){
			super(driver);
			setElement(CLASS_NAME, "stp--site-nav");
		}
		/**
		 * <b>Parent:</b> {@link TopNavigationRibbon TopNavigationRibbon}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebListItem
		 * <b>Name:</b>    AccountNavItem<br>
		 * 	<font color="green">path [@className==account-nav-item]</font>
		 * Number of child objects: 1</pre>
		 */
		public AccountNavItem AccountNavItem = new AccountNavItem(driver);

		/**
		 * <b>Parent:</b> {@link TopNavigationRibbon TopNavigationRibbon}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebListItem
		 * <b>Name:</b>    AccountNavItem<br>
		 * 	path [@className==account-nav-item]
		 * <br>Number of child objects: 1</pre>
		 */
		public class AccountNavItem extends WebListItem{

			public AccountNavItem(WebDriver driver){
				super(driver);
				setElement(TopNavigationRibbon.this, CLASS_NAME, "account-nav-item");
			}
			/**
			 * <b>Parent:</b> {@link TopNavigationRibbon.AccountNavItem AccountNavItem}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebElem
			 * <b>Name:</b>    MenuContainer<br>
			 * 	<font color="green">path [@className==nav-item-menu-container]</font>
			 * Number of child objects: 1</pre>
			 */
			public MenuContainer MenuContainer = new MenuContainer(driver);

			/**
			 * <b>Parent:</b> {@link TopNavigationRibbon.AccountNavItem AccountNavItem}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebElem
			 * <b>Name:</b>    MenuContainer<br>
			 * 	path [@className==nav-item-menu-container]
			 * <br>Number of child objects: 1</pre>
			 */
			public class MenuContainer extends WebElem{

				public MenuContainer(WebDriver driver){
					super(driver);
					setElement(TopNavigationRibbon.AccountNavItem.this, CLASS_NAME, "nav-item-menu-container");
				}
				/**
				 * <b>Parent:</b> {@link TopNavigationRibbon.AccountNavItem.MenuContainer MenuContainer}<pre>
				 * <b>Entity:</b>  <em>Object</em>
				 * <b>Class:</b>   WebButton
				 * <b>Name:</b>    SignIn<br>
				 * 	<font color="green">path [@className==stp--btn-signIn]</font>
				 * Number of child objects: 0</pre>
				 */
				public SignIn SignIn = new SignIn(driver);

				/**
				 * <b>Parent:</b> {@link TopNavigationRibbon.AccountNavItem.MenuContainer MenuContainer}<pre>
				 * <b>Entity:</b>  <em>Object</em>
				 * <b>Class:</b>   WebButton
				 * <b>Name:</b>    SignIn<br>
				 * 	path [@className==stp--btn-signIn]
				 * <br>Number of child objects: 0</pre>
				 */
				public class SignIn extends WebButton{

					public SignIn(WebDriver driver){
						super(driver);
						setElement(TopNavigationRibbon.AccountNavItem.MenuContainer.this, CLASS_NAME, "stp--btn-signIn");
					}
				}
			}
		}
	}
}
