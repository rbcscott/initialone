package customproject.functional.login.scripts;

import org.testng.annotations.Test;

import core.test.TestScript;
import customproject.base.BaseTest;
import customproject.functional.landing.Landing;
import customproject.functional.login.Login;

/**
 * Login - Basic Login.<br>
 * <br>
 * Main steps:<br>
 * <ul>
 * <li>login</li>
 * </ul>
 * @version 1.0
 */
public class LGI001_BasicLogin extends BaseTest implements TestScript{


	@Override
	@Test(groups = { "Regression", "RegressionTest-LoginLogout" })
	public void runTest(){
		executeTest();
	}

	@Override
	public void runScript(){
		// initialize modules ...
		Login login = new Login();
		Landing landing = new Landing();

		// main steps ...
		launchBaseURL();
		landing.navigateToLogin();
		login.login();
	}

}
