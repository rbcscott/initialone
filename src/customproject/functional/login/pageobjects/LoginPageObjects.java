package customproject.functional.login.pageobjects;

import se.objectlib.web.*;
import static se.LocatorType.*;

import org.openqa.selenium.WebDriver;

import customproject.base.BasePage;
/**
 * Abstract superclass containing object definitions for LoginPageObjects (Selenium objects).
 * This class is intended for extension by a script or any class supporting object actions.<br><br>
 * <em>File contents are auto-generated - any changes made to this file will be overwritten
 * by a subsequent update to the corresponding object file via the Se Object View.</em>
 */
public abstract class LoginPageObjects extends BasePage{

	public LoginPageObjects(WebDriver driver){
		super(driver);
		objectInit();
	}

	private void objectInit(){
		LoginForm = new LoginForm(driver);
	}

	/**
	 * <b>Parent:</b> {@link  }<pre>
	 * <b>Entity:</b>  <em>Object</em>
	 * <b>Class:</b>   WebElem
	 * <b>Name:</b>    LoginForm<br>
	 * 	<font color="green">path [@id==stp-Login-Form]</font>
	 * Number of child objects: 4</pre>
	 */
	protected LoginForm LoginForm;

	/**
	 * <b>Parent:</b> {@link  LoginPageObjects}<pre>
	 * <b>Entity:</b>  <em>Object</em>
	 * <b>Class:</b>   WebElem
	 * <b>Name:</b>    LoginForm<br>
	 * 	path [@id==stp-Login-Form]
	 * <br>Number of child objects: 4</pre>
	 */
	public class LoginForm extends WebElem{

		public LoginForm(WebDriver driver){
			super(driver);
			setElement(ID, "stp-Login-Form");
		}
		/**
		 * <b>Parent:</b> {@link LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebTextField
		 * <b>Name:</b>    UserName<br>
		 * 	<font color="green">path [@id==username]</font>
		 * Number of child objects: 0</pre>
		 */
		public UserName UserName = new UserName(driver);

		/**
		 * <b>Parent:</b> {@link LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebTextField
		 * <b>Name:</b>    Password<br>
		 * 	<font color="green">path [@id==password]</font>
		 * Number of child objects: 0</pre>
		 */
		public Password Password = new Password(driver);

		/**
		 * <b>Parent:</b> {@link LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebButton
		 * <b>Name:</b>    SignIn<br>
		 * 	<font color="green">path [@id==sign-in-button]</font>
		 * Number of child objects: 0</pre>
		 */
		public SignIn SignIn = new SignIn(driver);

		/**
		 * <b>Parent:</b> {@link LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebElem
		 * <b>Name:</b>    AssistLinks<br>
		 * 	<font color="green">path [@className==doublemargined]</font>
		 * Number of child objects: 2</pre>
		 */
		public AssistLinks AssistLinks = new AssistLinks(driver);

		/**
		 * <b>Parent:</b> {@link .LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebTextField
		 * <b>Name:</b>    UserName<br>
		 * 	path [@id==username]
		 * <br>Number of child objects: 0</pre>
		 */
		public class UserName extends WebTextField{

			public UserName(WebDriver driver){
				super(driver);
				setElement(LoginForm.this, ID, "username");
			}
		}
		/**
		 * <b>Parent:</b> {@link LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebTextField
		 * <b>Name:</b>    Password<br>
		 * 	path [@id==password]
		 * <br>Number of child objects: 0</pre>
		 */
		public class Password extends WebTextField{

			public Password(WebDriver driver){
				super(driver);
				setElement(LoginForm.this, ID, "password");
			}
		}
		/**
		 * <b>Parent:</b> {@link .LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebButton
		 * <b>Name:</b>    SignIn<br>
		 * 	path [@id==sign-in-button]
		 * <br>Number of child objects: 0</pre>
		 */
		public class SignIn extends WebButton{

			public SignIn(WebDriver driver){
				super(driver);
				setElement(LoginForm.this, ID, "sign-in-button");
			}
		}
		/**
		 * <b>Parent:</b> {@link .LoginForm LoginForm}<pre>
		 * <b>Entity:</b>  <em>Object</em>
		 * <b>Class:</b>   WebElem
		 * <b>Name:</b>    AssistLinks<br>
		 * 	path [@className==doublemargined]
		 * <br>Number of child objects: 2</pre>
		 */
		public class AssistLinks extends WebElem{

			public AssistLinks(WebDriver driver){
				super(driver);
				setElement(LoginForm.this, CLASS_NAME, "doublemargined");
			}
			/**
			 * <b>Parent:</b> {@link LoginForm.AssistLinks AssistLinks}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebLink
			 * <b>Name:</b>    ForgotUsername<br>
			 * 	<font color="green">path [@cssSelector==a[href&#42;=loginuser]]</font>
			 * Number of child objects: 0</pre>
			 */
			public ForgotUsername ForgotUsername = new ForgotUsername(driver);

			/**
			 * <b>Parent:</b> {@link LoginForm.AssistLinks AssistLinks}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebLink
			 * <b>Name:</b>    ForgotPassword<br>
			 * 	<font color="green">path [@cssSelector==a[href&#42;=loginassist]]</font>
			 * Number of child objects: 0</pre>
			 */
			public ForgotPassword ForgotPassword = new ForgotPassword(driver);

			/**
			 * <b>Parent:</b> {@link LoginForm.AssistLinks AssistLinks}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebLink
			 * <b>Name:</b>    ForgotUsername<br>
			 * 	path [@cssSelector==a[href&#42;=loginuser]]
			 * <br>Number of child objects: 0</pre>
			 */
			public class ForgotUsername extends WebLink{

				public ForgotUsername(WebDriver driver){
					super(driver);
					setElement(LoginForm.AssistLinks.this, CSS, "a[href*=loginuser]");
				}
			}
			/**
			 * <b>Parent:</b> {@link LoginForm.AssistLinks AssistLinks}<pre>
			 * <b>Entity:</b>  <em>Object</em>
			 * <b>Class:</b>   WebLink
			 * <b>Name:</b>    ForgotPassword<br>
			 * 	path [@cssSelector==a[href&#42;=loginassist]]
			 * <br>Number of child objects: 0</pre>
			 */
			public class ForgotPassword extends WebLink{

				public ForgotPassword(WebDriver driver){
					super(driver);
					setElement(LoginForm.AssistLinks.this, CSS, "a[href*=loginassist]");
				}
			}
		}
	}
}
