package customproject.functional.login.pages;

import org.openqa.selenium.WebDriver;

import customproject.functional.login.pageobjects.LoginPageObjects;
import se.baselib.web.SeWebElement;

/**
 * Object/UI interactions for the Login page.<br><br>
 * Holds multiple methods for getting and setting
 * values on the page (login form fields, button(s)
 * and text links).
 */
public class LoginPage extends LoginPageObjects{

	/**
	 * Create new LoginPage object with the given driver.<br><br>
	 * Driver is passed through to the backing object class.
	 * @param driver active driver to use for this page
	 */
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Applies the given userID to the username field.<br><br>
	 * If userID is <b>null</b>, no action is taken.
	 * @param userID user to set into the text field
	 */
	public void setUserName(String userID){
		if(userID != null){
			if(LoginForm.UserName.existsBefore(10)){
				LoginForm.UserName.setText(userID);
			}
		}
	}

	/**
	 * Applies the given password to the password field.<br><br>
	 * If password is <b>null</b>, no action is taken.
	 * @param password to set into the password field
	 */
	public void setPassword(String password){
		if(password != null){
			if(LoginForm.Password.existsBefore(2)){
				LoginForm.Password.setText(password);
			}
		}
	}

	/**
	 * Selects the "SIGN IN" button from the login form.<br><br>
	 * Action is made via {@link SeWebElement#navigate()} which will
	 * click and wait for appropriate page loads and completion.
	 */
	public void selectSignIn(){
		LoginForm.SignIn.waitForClickable(5);
		LoginForm.SignIn.navigate();
	}

	/**
	 * Selects the 'password' link from the "Forgot 
	 * <span style='color:blue;'>username</span> or 
	 * <span style='color:blue;'>password</span>" options at 
	 * bottom of form.<br><br>
	 * Link is located by CSS (using a language-independent capture
	 * of the href content) and is clicked via a navigate() call. 
	 */
	public void selectForgotPassword(){
		LoginForm.AssistLinks.ForgotPassword.waitForClickable(5);
		LoginForm.AssistLinks.ForgotPassword.navigate();
	}
	
	

	

}
