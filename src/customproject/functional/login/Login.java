package customproject.functional.login;

import customproject.base.BaseLibrary;
import customproject.base.BaseTest;
import customproject.base.ProjectTestParameters;
import customproject.functional.login.pages.LoginPage;
import customproject.functional.login.sql.LoginSql;
import db.RecordSet;
import static customproject.functional.login.sql.tables.APP_LOGIN.*;

/**
 * Functional module for Login actions. Provides steps
 * for actions related to login, e.g.
 * <ul>
 * <li>login() - basic login
 * <li>loginForgotPassword() - handle forgot password actions
 * </ul>
 * 
 */
public class Login extends BaseLibrary{

	LoginPage loginPage = null;
	ProjectTestParameters ptp;
	
	/**
	 * Creates new instance of the Login module.<br><br>
	 * Holds initializers for the test parameters and page
	 * classes that are used within.
	 */
	public Login(){
		ptp = BaseTest.getTestParameters();
		loginPage = new LoginPage(ptp.getDriver());
	}
	
	
	/**
	 * Based on the current executing test, data is pulled and used to perform
	 * the login procedure (customer/user/password entry followed by selecting
	 * the login button).
	 */
	public void login(){
		// get test data
		RecordSet loginData = LoginSql.getLoginData(ptp);
		if(loginData.size() > 0){
			String userID = loginData.get(USERNAME);
			String userPassword = loginData.get(PASSWORD);
			loginPage.setUserName(userID);
			loginPage.setPassword(userPassword);
			loginPage.selectSignIn();
		}
	}
	
	/**
	 * Login with forgotten password.<br><br>
	 * Using the test case login data, this will enter the 
	 * user, and then select the 'password' link from the "Forgot 
	 * <span style='color:blue;'>username</span> or 
	 * <span style='color:blue;'>password</span>" options at 
	 * bottom of form.<br><br>
	 * <i>Test method - partially implemented.</i>
	 */
	public void loginForgotPassword(){
		RecordSet loginData = LoginSql.getLoginData(ptp);
		if(loginData.size() > 0){
			String userID = loginData.get(USERNAME);
			loginPage.setUserName(userID);
			loginPage.selectForgotPassword();
			// ...
			// remainder is unimplemented
			// ...
		}
	}

}
