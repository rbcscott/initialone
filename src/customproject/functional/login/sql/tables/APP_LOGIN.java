package customproject.functional.login.sql.tables;

import java.util.*;
import db.tables.*;

/** Defines the columns that exist for the COMLIB table <b>APP_LOGIN</b>.*/ 
public class APP_LOGIN implements Table{

	private APP_LOGIN(){
		super();
	}

	public static APP_LOGIN getTable(){
		return new APP_LOGIN();
	}

	private static final String tableName ="APP_LOGIN";

	/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>ID<br><b>Column Type: </b>NUMERIC </pre>*/
	public static final String ID = Field.ID.fieldName;
	/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>USERNAME<br><b>Column Type: </b>VARCHAR </pre>*/
	public static final String USERNAME = Field.USERNAME.fieldName;
	/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>PASSWORD<br><b>Column Type: </b>VARCHAR </pre>*/
	public static final String PASSWORD = Field.PASSWORD.fieldName;
	/**
	 * Representation of the fields available in this table. The enumeration
	 * backs the front-facing set for this table class, and provides additional
	 * attributes where necessary.
	 */
	public enum Field implements TableColumn{
		/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>ID<br><b>Column Type: </b>NUMERIC </pre>*/
		ID("ID", "NUMERIC"),
		/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>USERNAME<br><b>Column Type: </b>VARCHAR </pre>*/
		USERNAME("USERNAME", "VARCHAR"),
		/** <pre>      <b>Table: </b>APP_LOGIN<br>     <b>Column: </b>PASSWORD<br><b>Column Type: </b>VARCHAR </pre>*/
		PASSWORD("PASSWORD", "VARCHAR");

		private String fieldName = "";

		private String type = null;

		private Field(String dbName){
			this.fieldName = dbName;
		}

		private Field(String dbName, String type){
			this.fieldName = dbName;
			this.type = type;
		}

		/**
		 * The assigned field/column name. Usually the same as the name of the 
		 * enumerated type (what can be derived from <code>toString()</code>), 
		 * but does not have to be.
		 */
		public String getDbName(){
			return this.fieldName;
		}
		/**
		 * The assigned field/column name. Usually the same as the name of the 
		 * enumerated type (what can be derived from <code>toString()</code>), 
		 * but does not have to be.
		 */
		public String getName(){
			return getDbName();
		}
		/**
		 * The assigned field type (String value)		 */
		public String getType(){
			return this.type;
		}
		
		/**
		 * Finds and returns the matching enumeration type for the given field name.
		 * If a name given does not find a match, <code><b>null</b></code> is returned.
		 * @param fieldName name of the field to match
		 * @return matching Field
		 */
		public static Field getFieldByName(String fieldName){
			if(fieldName != null){
				for(Field f : Field.values()){
					if(f.fieldName.equals(fieldName)){
						return f;
					}
				}
			}
			return null;
		}
		/**
		 * Returns a String list of the field names from this table. They are in order
		 * of the declared underlying enumeration (although this is not always guaranteed).
		 * @return list of the field names from this table
		 */
		public List<String> listFields(){
			List<String> fieldSet = new ArrayList<String>();
			for(Field f : Field.values()){
				fieldSet.add(f.fieldName);
			}
			return fieldSet;
		}
	}

	@Override
	public String getTableName(){
		return tableName;
	}
	@Override
	public String getType(String name){
		String type = null;
		Field f = Field.getFieldByName(name);
		if(f != null){
			type = f.getType();
		}
		return type;
	}
	
	public TableColumn getField(String fieldName){
		return Field.getFieldByName(fieldName);
	}

}