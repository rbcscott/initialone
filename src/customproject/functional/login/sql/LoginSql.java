package customproject.functional.login.sql;

import core.test.TestParameters;
import db.AutoDB;
import db.JdbcOracle;
import db.RecordSet;

public class LoginSql{

	/**
	 * Query and return login data for this test case.<br><br>
	 * This is based on a relationship between the actual account (user)
	 * data and the registered test case. For example, if using the following user data:
	 * <pre>
	 * ID  USERNAME      PASSWORD        
	 * --  ------------  ------------
	 * 1   AUTODEMO_001  PASSWORD_001
	 * </pre>
	 * One would reference this via the registration table, LOGIN_ID field:
	 * <pre>
	 * TESTCASE  LOGIN_ID  ITERATION  INTERNAL_ITERATION    
	 * --------  --------  ---------  ------------------    
	 * LGI001    1         1          1                     
	 * </pre>
	 * thus leaving one instance of the account record, and the ability for
	 * multiple tests to reference it when needed.<br><br>
	 * 
	 * @param tp test parameters
	 * @return RecordSet of selected data
	 */
	public static RecordSet getLoginData(TestParameters tp){
		RecordSet rs = new RecordSet();
		String sql = 
				"SELECT * FROM APP_LOGIN, APP_LOGIN_REGISTRATION " +
				"WHERE APP_LOGIN_REGISTRATION.TESTCASE = '" + tp.getTestShortName() + "' ";
		JdbcOracle jdbc = new JdbcOracle();
		System.out.println("Executing SQL [" + sql + "]");
		rs = jdbc.executeSql(AutoDB.parameters(), sql);
		return rs;
	}
	
}
