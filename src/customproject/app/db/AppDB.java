package customproject.app.db;

import java.util.Properties;


/**
 * Represents the application database used by automation, with its main purpose to act as
 * a builder for the connection string to the database. Full usage described below.<br><br>
 * Clients should specify the db instance and the schema using this class 
 * as entry point:<br><br>
 * Instance:<br>
 * {@link #setInstance(Properties, String)}<br>
 * or<br>
 * {@link #setInstance(Instance)}<br><br>
 * Schema:<br>
 * {@link #setSchema(Schema, String)}<br><br>
 * <b>Get a connect string</b>:<br>
 * <code>String connectString = AppDB.QA.user();</code><br>
 * <code>String connectString = AppDB.STAGE1.user();</code><br><br>
 * <b>Given a sql:</b><br>
 * <code>String sql = <font color='blue'>"SELECT ITEM_NO, ITEM_DESC FROM ITEMS_TABLE"</font>;</code><br><br>
 * <b>then...</b><br>
 * <code>JdbcOracle oracleDb = <b>new</b> JdbcOracle();</code><br>
 * <code>RecordSet rs = oracleDb.executeSql(connectString, sql);</code><br><br>
 * will return the records queried using the given connection and sql, into a {@link RecordSet} object.
 */
public class AppDB{
	
	private static final String DEFAULT_PORT = "1521";
	//private static String PARAMS = "";
	//private static String HIST = "";
	//private static String DW = "";
	private static Instance instance = null;
	/**
	 * Represents the QA instance of the application database. There are currently:<br>
	 * <ul>
	 * <li>QA - QA
	 * </ul>
	 * This is used in line with a call to get the schema and build a 
	 * connection string, e.g.:<br><br>
	 * <code>String connectString = AppDB.QA.user();</code><br>
	 */
	public static Instance QA = Instance.QA;
	
	
	/**
	 * Create a working instance of this class with the given properties file and key field
	 * that holds the instance to be set. This can be any String set in the properties file,
	 * e.g., in bold below:<br><br>
	 * <code><b>AppDBInstance</b>=<font color='blue'>QA</font></code><br>
	 * <br>
	 * and will be used to set the instance. Subsequently, whenever a direct call to obtain the 
	 * schema is made, the set instance will used:<br><br>
	 * <code>AppDB.user();</code><br>
	 * @param properties properties instance with a field set to valid value using...
	 * @param instanceKeyField the key field used to read the instance value
	 */
	public static void setInstance(Properties properties, String instancePropertyKey){
		String propVal = properties.getProperty(instancePropertyKey);
		Instance i = Instance.getInstance(propVal);
		if(i == null){
			System.out.println("Unable to set automation database instance "
					+ "[given " + instancePropertyKey + "=" + propVal + "]");
		}else{
			instance = i;
		}
	}
	
	/**
	 * Set this class with the given instance value<br><br>
	 * Subsequently, whenever a direct call to obtain the 
	 * schema is made, the set instance will used, e.g.:<br><br>
	 * <code>AppDB.user();</code><br>
	 * <code>AppDB.parameters();</code><br><br>
	 * @param dbInstance a value from {@link Instance}
	 */
	public static void setInstance(Instance dbInstance){
		if(dbInstance == null){
			System.out.println("Unable to set automation database instance [null]");
		}else{
			instance = dbInstance;
		}
	}
	
	/**
	 * Sets the password for the given schema. E.g.:<br><br>
	 * <code>setSchema(_USER, <span style='color:blue;'>"mypw"</span>);</code><br><br>
	 * will set the password for the schema to <code style='color:blue;'>"mypw"</code>.
	 * @param schema Schema type to set
	 * @param pw schema password
	 */
	public static void setSchema(Schema schema, String pw){
		if(schema != null)
			schema.setSchema(pw);
	}
	
	/**
	 * Represents a particular instance of the application database. There are currently:<br>
	 * <ul>
	 * <li>QA - QA
	 * </ul>
	 * This is used in line with a call to get the schema and build a 
	 * connection string, e.g.:<br><br>
	 * <code>String connectString = AppDB.QA.user();</code><br>
	 */
	public static enum Instance{
		QA("QA", "<servername>.staples.com");
		
		
		private String instance = "";
		private String server = "";
		
		private Instance(String instance, String server){
			this.instance = instance;
			this.server = server;
		}
		
		/**
		 * Gets the Oracle connection string representing a connection to this instance
		 * and to the _USER. This string is ready for delivery to
		 * an execution call against the {@link JdbcOracle} class (or other direct call to a 
		 * {@link java.sql.DriverManager}).
		 * @return connection string to the _USER schema
		 */
		public String appUser(){
			return buildConnectString(Schema._USER);
		}
		
		private String buildConnectString(Schema schema){
			return AppDB.buildConnectString(this, schema);
		}
		
		/**
		 * Attempts to get a match on the given instance String. E.g., if given
		 * <font color='blue'>"qa"</font>, a case-insensitive comparison is made 
		 * and will return Instance.QA. Likewise for <font color='blue'>"stage1"</font>
		 *  and Instance.STAGE1<br>If no match, <b><code>null</code></b> is returned.<br>
		 * If <b><code>null</code></b> is sent, <b><code>null</code></b> is returned.<br><br>
		 * There are currently:<br>
		 * <ul>
		 * <li>QA - QA
		 * </ul>
		 * @param instance String to match the instance value requested
		 * @return matching Instance
		 */
		public static Instance getInstance(String instance){
			if(instance != null){
				for(Instance _instance : Instance.values()){
					if(_instance.toString().equalsIgnoreCase(instance)){
						return _instance;
					}
				}
			}
			return null;
		}
	}
	/**
	 * Schemas available to this database. They are represented here as:<br>
	 * <ul>
	 * <li>_USER
	 * </ul>
	 * Schemas should be set by using the AppDB calls to setSchema or setAllSchemas:<br>
	 * {@link #setSchema(Schema, String, String)}<br>
	 * {@link #setAllSchemas(String, String)}<br>
	 */
	public static enum Schema{
		/**
		 * _USER - user schema
		 */
		_USER("_USER");
		
		private String user = "";
		private String pw = "";
		
		private Schema(String user){
			this.user = user;
		}
		
		private void setSchema(String pw){
			if(pw != null)
				this.pw = pw;
		}
		
		/**
		 * Finds and returns the matching enumeration type for the given schema name.
		 * If a name given does not find a match, <code><b>null</b></code> is returned.
		 * @param schemaName name of the field to match
		 * @return matching Schema
		 */
		public static Schema getSchema(String schemaName){
			if(schemaName != null){
				for(Schema schema : Schema.values()){
					if(schema.user.equals(schemaName)){
						return schema;
					}
				}
			}
			return null;
		}
	}
		
	/**
	 * Gets the Oracle connection string representing a connection to this instance
	 * and to the user schema [_USER]. This string is ready for delivery to
	 * an execution call against the {@link JdbcOracle} class (or other direct call to a 
	 * {@link java.sql.DriverManager}).<br><br>
	 * <em>Note that the instance must be set (prior to using this directly) 
	 * via {@link #setInstance(Properties, String)}</em>
	 * @return connection string to the _USER schema
	 */
	public static String appUser(){
		return instance == null ? "" : buildConnectString(instance, Schema._USER);
	}
	
	
	
	private static String buildConnectString(Instance instance, Schema schema){
		StringBuilder str = new StringBuilder();
		str.append("jdbc:oracle:thin:");
		str.append(schema.user + "/" + schema.pw + "@//");
		str.append(instance.server + ":" + DEFAULT_PORT + "/");
		str.append(instance.instance);
		return str.toString();
	}
}

