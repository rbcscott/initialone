/**
	 *@UtilityComponentName: Log
	 *@Description:  
	 *@author: 
	 *@CreatedDate:
	 *@ModifiedBy:
	 *@ModifiedDate:
     *@param: 
     *@return:
     */
package customproject.utilities;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import se.Se;

/**
 *
 */
public class Log {
	public static Logger getInstance(String className){
	    DOMConfigurator.configure("log4j.xml");
	    Logger logger = Logger.getLogger(className);
	    Se.addLoggingProvider(new LoggerAdapter(logger));
		return logger;
	}

}
