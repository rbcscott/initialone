package customproject.utilities;

import org.apache.log4j.Logger;

import se.SeObjectLogger;

public class LoggerAdapter implements SeObjectLogger{

	private Logger logger;

	public LoggerAdapter(Logger logger){
		this.logger = logger;
	}
	
	public void debug(Object message){
		logger.debug(message);
	}

	public void error(Object message){
		logger.error(message);
	}

	public void fatal(Object message){
		logger.fatal(message);
	}

	public void info(Object message){
		logger.info(message);
	}

	public void trace(Object message){
		logger.trace(message);
	}

	public void warn(Object message){
		logger.warn(message);
	}

	public String getName(){
		
		return logger.getName();
	}

}
