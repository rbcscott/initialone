package customproject.utilities;

import java.util.Map;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import core.util.CoreSuiteListener;
import core.util.FileEdit;
import customproject.settings.ProjectSettings;

public class SuiteListener extends CoreSuiteListener{

	@Override
	public void onStart(ISuite suite){
		// get all settings from the suite XML...
				String filePath = suite.getXmlSuite().getFileName();
				String xmlContent = FileEdit.getFileContentAsString(filePath);
				nu.xom.Document doc = null;
				Map<String, String> settingsMap = getSettingsMap(doc, xmlContent);
				// apply to current settings instance...
				ProjectSettings settings = ProjectSettings.getSettings();
				settings.applyRunplanOverrides(settingsMap);

	}

	@Override
	public void onFinish(ISuite suite){


	}



}
