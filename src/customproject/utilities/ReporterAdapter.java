package customproject.utilities;

import org.testng.Reporter;

import se.ReportableObjectAction;
import se.SeObjectReporter;

/**
 * Adapts a TestNG Reporter into one that can be used in 
 * the Se class.
 */
public class ReporterAdapter implements SeObjectReporter{

	/**
	 * Creates this class, which adapts a TestNG Reporter 
	 * into one that can be used in the Se class set.
	 */
	public ReporterAdapter(){
		
	}

	public void log(String message){
		Reporter.log(message);
	}
	
	public void logObjectAction(String objectName, String objectMethod, String status){
		logObjectAction(objectName, objectMethod, null, status);
	}
	
	public void logObjectAction(String objectName, String objectMethod, String inputValue, String status){
		Reporter.log("PageComponent" + objectName);
		Reporter.log("PageMethod" + objectMethod);
		Reporter.log("TestStepComponent" + "n/a");
		Reporter.log("TestStepInput:- " + (inputValue != null ? inputValue : "n/a"));
		Reporter.log("TestStepOutput:- " + "n/a");
		Reporter.log("TestStepExpectedResult:-" + "completed");
		Reporter.log("INFO_MESSAGE:-" + "completed");
	}



	public void logObjectAction(ReportableObjectAction objectAction){
		String inputValue = objectAction.getInputValue();
		Reporter.log("PageComponent" + objectAction.getCalledFromClassName());
		Reporter.log("PageMethod" + objectAction.getCalledFromMethodName());
		Reporter.log("TestStepComponent" + objectAction.getObjectName() + "." + objectAction.getActionName());
		Reporter.log("TestStepInput:- " + (inputValue != null ? inputValue : "n/a"));
		Reporter.log("TestStepOutput:- " + "n/a");
		Reporter.log("TestStepExpectedResult:-" + objectAction.getExpected());
		switch(objectAction.getStatus()){
		case COMPLETED:
			Reporter.log("INFO_MESSAGE:-" + "completed");
			break;
		case FAIL:
			Reporter.log("FAIL_MESSAGE:-" + objectAction.getActual());
			break;
		case OK:
		case ONF:
		case PASS:
			Reporter.log("PASS_MESSAGE:-" + "completed");
			break;
		default:
			Reporter.log("INFO_MESSAGE:-" + "invalid status reported [" + objectAction.getStatus() + "]");
			break;
		
		}
		
	}
	
}
