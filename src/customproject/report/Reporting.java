package customproject.report;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.testng.Reporter;




import core.test.TestParameters;
import customproject.base.BaseTest;
import customproject.settings.ProjectSettings;
import db.AutoDB;
import db.Record;
import db.RecordSet;

public class Reporting implements core.report.Reporting{

	private Reporting(){}
	
	private static Reporting reporting = null;
	private boolean reportingToDB = false;
	
	/**
	 * Obtains the reporting instance. 
	 * @return the reporting instance
	 */
	public static Reporting getReporting(){
		if(reporting == null){
			reporting = new Reporting();
		}
		return reporting;
	}
	
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param status
	 * @param msg
	 */
	public void report(Status status, String msg){
		//String className = getClassName();
		//String methodCall = getMethodCall();
		TestParameters tp = BaseTest.getTestParameters();
		Record<String> r = createVerificationRecord(tp, msg, msg, status);
		RecordSet vpData = BaseTest.getTestParameters().getVPData();
		vpData.addRecord(r);
		Reporter.log("PageComponent" + getClassName());
		Reporter.log("PageMethod" + getMethodCall());
	    Reporter.log(status + "_MESSAGE:-" + msg);
	}
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param expected
	 * @param passFail
	 * @param msg
	 */
	public void reportVP(String expected, Status passFail, String msg){
		reportVP("NA", "NA", expected, passFail, msg);
	}
	/**
	 * Reports out based on the given verification point parameters. Elements
	 * are processed to be formatted with the Report sequence.
	 * @param input
	 * @param output
	 * @param expected
	 * @param status
	 * @param msg
	 */
	public void reportVP(String input, String output, String expected, Status status, String msg){
		TestParameters tp = BaseTest.getTestParameters();
		
		Record<String> r = createVerificationRecord(tp, expected, msg, status);
		RecordSet vpData = BaseTest.getTestParameters().getVPData();
		vpData.addRecord(r);
		
		if(/*reportTestNg*/true){
			reportTestNg(input, output, expected, status, msg);
		}
		if(/*reportDb */true){
			reportDb(r);
		}
		
		
	}
	
	private void reportTestNg(String input, String output, String expected, Status status, String msg){
		String methodName = getMethodCall();
		Reporter.log("TestStepComponent" + methodName);
		Reporter.log("TestStepInput:-" + input);
		Reporter.log("TestStepOutput:-" + output);
		Reporter.log("TestStepExpectedResult:-" + expected);
		Reporter.log(status + "_MESSAGE:-" + msg);
	}
	
	private void reportDb(Record<String> r){
		r.setDbConnection(AutoDB.history());
		r.writeToDatabase(VP_RESULTS.getTable());
	}
	
	private String getMethodCall(){
		StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		String className = "";
		String methodName = "";
		for(StackTraceElement e : elems){
			className = e.getClassName();
			if(className.equals("java.lang.Thread") || 
				className.endsWith("BasePage") ||
				className.endsWith("Reporting")){
				continue;
			}
			methodName = e.getMethodName();
			break;
		}
		return methodName;
	}
	
	private String getClassName(){
		return this.getClass().getSimpleName();
	}
	private Record<String> createVerificationRecord(TestParameters tp, String expected, String msg, Object status){
		Record<String> r = new Record<>();
		ProjectSettings advSettings = ProjectSettings.getSettings();
		r.addElement("TESTCASE", tp.getTestShortName());
		r.addElement("ITERATION", Integer.toString(tp.getIteration()));
		r.addElement("INTERNAL_ITERATION", Integer.toString(tp.getInternalIteration()));
		r.addElement("ENVIRONMENT", tp.getEnvironment());
		r.addElement("DESCRIPTION", msg);
		r.addElement("EXPECTED", expected);
		r.addElement("ACTUAL", msg);
		r.addElement("STATUS", status.toString());
		r.addElement("DATE_TIME", getDateTime());
		r.addElement("TIMEZONE", getTimeZone());
		r.addElement("USER_NAME", System.getProperty("user.name"));
		r.addElement("MACHINE_NAME", getMachineName());
		r.addElement("MACHINE_IP", getMachineIP());
		r.addElement("RUN_NAME", advSettings.getRunPlanName());
		r.addElement("RUN_ID", advSettings.getRunID());
		return r;
	}
	
	private String getDateTime(){
		Calendar currentdate = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		//TimeZone timeZone = Calendar.getInstance().getTimeZone();
		//formatter.setTimeZone(timeZone);
		return formatter.format(currentdate.getTime());
	}
	
	private String getTimeZone(){
		TimeZone timeZone = Calendar.getInstance().getTimeZone();
		return timeZone.getDisplayName(true, TimeZone.SHORT);
	}
	
	private String getMachineName(){
		String returnVal = "";
		try{
			returnVal = InetAddress.getLocalHost().getHostName();
		}catch (UnknownHostException e){
			e.printStackTrace();
		}
		return returnVal;
	}
	
	private String getMachineIP(){
		String returnVal = "";
		try{
			returnVal = InetAddress.getLocalHost().getHostAddress();
		}catch (UnknownHostException e){
			e.printStackTrace();
		}
		return returnVal;
	}

	/**
	 * Returns whether or not this report is set to send out results to the 
	 * connected database (the connection is made via the RecordSet or
	 * Record object, used in the condition this flag is true).
	 * @return <b>true</b> if flagged to report to connected DB
	 */
	public boolean isReportingToDB(){
		return reportingToDB;
	}

	/**
	 * Set this report to send results to DB. This flag is used in the reporting
	 * implementation to determine whether to generate and post a record of 
	 * results to the storing database (aqedev.VP_RESULTS or other)).
	 * @param reportingToDB set <b>true</b> to send results to connected database
	 */
	public void setReportingToDB(boolean reportingToDB){
		this.reportingToDB = reportingToDB;
	}

}
