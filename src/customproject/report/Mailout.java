package customproject.report;

import java.util.ArrayList;
import java.util.List;

import core.report.Mail;
import customproject.settings.ProjectSettings;
import db.RecordSet;

public class Mailout{

	public void sendMail(RecordSet reportData){
		
		ProjectSettings settings = ProjectSettings.getSettings();
		
		boolean sendMail = Boolean.parseBoolean(settings.getProperty("SendMail")); 

		if (sendMail) {
			List<String> recipients = new ArrayList<String>();
			String propRecipients = settings.getProperty("Recipients");
			if (propRecipients != null) {
				String[] arrRecipients = propRecipients.split(",");
				for (String emailAddr : arrRecipients) {
					recipients.add(emailAddr);
				}
			}

			StringBuilder emailLog = new StringBuilder();
			// pass fail status formatting and updating count
			emailLog.append("<b><font color=blue>Result Path: " + "<a href=>Summary" + "</a></font><br><br>");
			
			int passCount = 2;
			int failCount = 0;
			int noRunCount = 0;
			
			emailLog.append("<b><font color=green>Passed: " + passCount + "</font><br>");
			emailLog.append("<font color=red>Failed: &nbsp;&nbsp;" + failCount + "</font></b><br>");
			emailLog.append("<font color=black>No Run: &nbsp;&nbsp;" + noRunCount + "</font></b><br>");
			//emailLog.append(formatTable());
			emailLog.append("</p></body></html>");
			System.out.println("Send email:Started");

			//boolean attachmentOK = false;
			//String attachmentFlag = properties.getProperty("AttachmentAllowed", "false");
			//if(Boolean.getBoolean(attachmentFlag)){
			//	attachmentOK = true;
			//}
			
			int total = passCount + failCount + noRunCount;
			double passPct = (double)passCount/(double)total;
			int roundedPassPct = (int) (passPct * 100);
			
			// based on no failed tests (100% pass determination)...
			String passFail = (failCount < 1 ? "PASS" : "FAIL") + "  [" + roundedPassPct + "%]";
			
			Mail.sendMail(recipients, "Execution complete [" + "env" + "] - " + passFail + " - [" + settings.getMachineName() + "]", emailLog.toString(), "");
			
			System.out.println("Send email:Done");

		}
	}
	
}
